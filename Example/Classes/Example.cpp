#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "d3d10.lib")
#pragma comment(lib, "d3dx10.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "DirectXTex.lib")
#pragma comment(lib, "fmodex_vc.lib")

#define EXAMPLE_TYPE_STRUCTURE			1
#define EXAMPLE_TYPE_MATH				2
#define EXAMPLE_TYPE_DIRECT_3D			3

#define EXAMPLE_TYPE			EXAMPLE_TYPE_DIRECT_3D

#if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE

#elif EXAMPLE_TYPE == EXAMPLE_TYPE_MATH

#elif EXAMPLE_TYPE == EXAMPLE_TYPE_DIRECT_3D
#include "Global/Utility/Manager/CSceneManager.h"
#include "Scene/CTestScene.h"
#endif			// #if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE

/*
자료구조란?
:
- 데이터를 저장하고 표현하는 방법을 자료구조라고 한다. (즉, 데이터를
관리함에 있어서 효율적인 구조를 지니는 정형화되어있는 데이터 구조를
의미한다.)
*/
//! 메인 함수
int WINAPI _tWinMain(HINSTANCE a_hInstance,
	HINSTANCE a_hPrevInstance, 
	TCHAR *a_pszCommand, 
	int a_nShowOptions)
{
#if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE || EXAMPLE_TYPE == EXAMPLE_TYPE_MATH
	if (AllocConsole()) {
		freopen("CONIN$", "rb", stdin);
		freopen("CONOUT$", "wb", stdout);
		freopen("CONOUT$", "wb", stderr);
	}

#if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE
	//EXAMPLE_1::Example_1();
	//EXAMPLE_2::Example_2();
	//EXAMPLE_3::Example_3();
	EXAMPLE_4::Example_4();
#else
	EXAMPLE_5::Example_5();
#endif			// #if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE

	std::system("pause");
	FreeConsole();

	return 0;
#elif EXAMPLE_TYPE == EXAMPLE_TYPE_DIRECT_3D
	SIZE stWindowSize = {
		1440, 900
	};

	//GET_SCENE_MANAGER()->init(a_hInstance, a_nShowOptions, stWindowSize);

	//return 0;
	//GET_SCENE_MANAGER()->changeScene(CSceneManager::ESCENETYPE::TAG_TEST_SCENE);

	//return GET_SCENE_MANAGER()->getCurrentScene()->run();

	CTestScene oApplication(a_hInstance, a_nShowOptions, stWindowSize);

	return oApplication.run();
#endif			// #if EXAMPLE_TYPE == EXAMPLE_TYPE_STRUCTURE || EXAMPLE_TYPE == EXAMPLE_TYPE_MATH
}
