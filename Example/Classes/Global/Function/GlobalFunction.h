#pragma once

#include "../Define/KGlobalDefine.h"

class CMesh;

//! 입력 레이아웃 -> 정점 선언으로 변환한다
std::vector<D3DVERTEXELEMENT9> InputLayoutToDeclaration(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 정점 선언 -> 입력 레이아웃으로 변환한다
std::vector<D3D11_INPUT_ELEMENT_DESC> DeclarationToInputLayout(const std::vector<D3DVERTEXELEMENT9> &a_rElementList);

//! 기본 경로를 반환한다
std::string GetBasePath(const std::string &a_rFilepath);

//! 대체 문자열을 반환한다
std::string GetReplaceString(const std::string &a_rString,
	const std::string &a_rFindString,
	const std::string &a_rReplaceString);

//! 형식 문자열을 반환한다
std::string GetFormatString(const char *a_pszFormat, ...);

//! 재질을 반환한다
STMaterial GetMaterial(const std::string &a_rFilepath);

//! 웨이브 사운드를 반환한다
STWaveSound GetWaveSound(const std::string &a_rFilepath);

//! 경계 상자를 반환한다
STBoundingBox GetBoundingBox(CMesh *a_pMesh);

//! 경계 구를 반환한다
STBoundingSphere GetBoundingSphere(CMesh *a_pMesh);


//! 사본 메시를 반환한다
LPD3DXMESH GetCloneMesh(LPD3DXMESH a_pOriginMesh,
	LPD3DXBUFFER a_pAdjacency, const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 삼각형 피킹 여부를 반환한다
bool IntersectTriangle(const D3DXVECTOR3& orig, const D3DXVECTOR3& dir,
	D3DXVECTOR3& v0, D3DXVECTOR3& v1, D3DXVECTOR3& v2,
	FLOAT* t, FLOAT* u, FLOAT* v);

//! 경계 상자의 충돌 여부를 검사한다
bool IsIntersectBox(const STBoundingBox &a_rstLhs,
	const STBoundingBox &a_rstRhs);

//! 객체 상자의 충돌 여부를 검사한다
bool IsIntersectObjectBox(const STObjectBox &a_rstLhs,
	const STObjectBox &a_rstRhs);

//! 경계 구의 충돌 여부를 검사한다
bool IsIntersectSphere(const STBoundingSphere &a_rstLhs,
	const STBoundingSphere &a_rstRhs);

//! 경계 상자와 광선 간의 충돌 여부를 검사한다
bool IsIntersectBoxRay(const STBoundingBox &a_rstLhs,
	const STRay &a_rstRay);

//! 경계 구와 광선 간의 충돌 여부를 검사한다
bool IsIntersectSphereRay(const STBoundingSphere &a_rstLhs,
	const STRay &a_rstRay);

//! 시간 간격을 계산한다
float CalculateDeltaTime(const std::chrono::system_clock::time_point &a_rStartTimePoint,
	const std::chrono::system_clock::time_point &a_rEndTimePoint);

//! 정수 랜덤 값을 생성한다
int CreateIntRandomValue(int a_nMin, int a_nMax);

//! 실수 랜덤 값을 생성한다
float CreateFloatRandomValue(float a_fMin, float a_fMax);

//! D3DXVECTOR3를 문자열로 반환
std::string ToString(const D3DXVECTOR3& stValue);

//! 광선을 생성한다
STRay CreateRay(const POINT &a_rstPoint);


//! 쉐이더를 컴파일한다
ID3D10Blob * CompileShader(const std::string &a_rFilepath,
	const std::string &a_rFunctionName,
	const std::string &a_rShaderVersion);

//! 상자 메시를 생성한다
CMesh * CreateBoxMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList,
	float a_fWidth = 1.0f, float a_fHeight = 1.0f, float a_fDepth = 1.0f);

//! 구 메시를 생성한다
CMesh * CreateSphereMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList,
	float a_fRadius = 0.5f);

//! 원기둥 메시를 생성한다
CMesh * CreateCylinderMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 주전자 메시를 생성한다
CMesh * CreateTeapotMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 도넛 메시를 생성한다
CMesh * CreateTorusMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 평면 메시를 생성한다
CMesh * CreatePlaneMesh(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! X 메시를 생성한다
CMesh * CreateXMesh(const std::string &a_rFilepath,
	const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList);

//! 버퍼를 생성한다
ID3D11Buffer * CreateBuffer(UINT a_nSize,
	UINT a_nBindFlags, 
	UINT a_nCPUAccessFlags = D3D11_CPU_ACCESS_WRITE, 
	D3D11_USAGE a_eUsage = D3D11_USAGE_DYNAMIC);

//! 정점 쉐이더를 생성한다
ID3D11VertexShader * CreateVertexShader(const std::string &a_rFilepath,
	ID3D10Blob **a_pOutVertexShaderCode,
	const std::string &a_rFunctionName = "main",
	const std::string &a_rShaderVersion = "vs_4_0");

//! 픽셀 쉐이더를 생성한다
ID3D11PixelShader * CreatePixelShader(const std::string &a_rFilepath,
	ID3D10Blob **a_pOutPixelShaderCode,
	const std::string &a_rFunctionName = "main",
	const std::string &a_rShaderVersion = "ps_4_0");

//! 입력 레이아웃을 생성한다
ID3D11InputLayout * CreateInputLayout(const std::vector<D3D11_INPUT_ELEMENT_DESC> &a_rElementDescList,
	ID3D10Blob *a_pVertexShaderCode);

//! 샘플러 상태를 생성한다
ID3D11SamplerState * CreateSamplerState(D3D11_FILTER a_eFilter,
	D3D11_TEXTURE_ADDRESS_MODE a_eAddressU = D3D11_TEXTURE_ADDRESS_WRAP,
	D3D11_TEXTURE_ADDRESS_MODE a_eAddressV = D3D11_TEXTURE_ADDRESS_WRAP);

//! 래스터라이저 상태를 생성한다
ID3D11RasterizerState * CreateRasterizerState(D3D11_FILL_MODE a_eFillMode,
	D3D11_CULL_MODE a_eCullMode);

//! 블렌드 상태를 생성한다
ID3D11BlendState * CreateBlendState(bool a_bIsBlendEnable,
	D3D11_BLEND a_eSourceBlend = D3D11_BLEND_SRC_ALPHA,
	D3D11_BLEND a_eDestinationBlend = D3D11_BLEND_INV_SRC_ALPHA,
	D3D11_BLEND_OP a_eBlendOperation = D3D11_BLEND_OP_ADD);

//! 쉐이더 리소스 뷰를 생성한다
ID3D11ShaderResourceView * CreateShaderResourceView(const std::string &a_rFilepath);

template <typename T>
T ClampValue(T a_tValue, T a_tMinValue, T a_tMaxValue);

/*
C++ 스마트 포인터 종류
:
- shared_ptr (레퍼런스 카운팅 방식으로 동작)
- unique_ptr (유일한 소유권을 지니는 방식으로 동작)
- weak_ptr (shared_ptr 객체를 보조하는 스마트 포인터)
- auto_ptr (유일한 소유권을 지니는 방식으로 동작 -> 쓰이지 않음)
*/
//! 버퍼 포인터를 반환한다
template <typename T>
std::unique_ptr<T, std::function<void(T *)>> GetBufferPointer(ID3D11Resource *a_pResource);

//! 인스턴스를 생성한다
template <typename T, typename... ARGS>
T * Create(ARGS&&... args);

#include "GlobalFunction.inl"
