#include "CObject.h"

CObject::CObject(void)
:
m_eObjectType(EObjectType::OBJECT),

m_stPosition(0.0f, 0.0f, 0.0f),
m_stScale(1.0f, 1.0f, 1.0f),
m_stRotation(0.0f, 0.0f, 0.0f),

m_stRightDirection(WORLD_RIGHT_DIRECTION),
m_stUpDirection(WORLD_UP_DIRECTION),
m_stForwardDirection(WORLD_FORWARD_DIRECTION)
{
	D3DXMatrixIdentity(&m_stOffsetMatrix);
	D3DXMatrixIdentity(&m_stWorldMatrix);
	D3DXMatrixIdentity(&m_stFinalWorldMatrix);
}

CObject::~CObject(void)
{
	for (auto pChildObject : m_oChildObjectList) {
		SAFE_DELETE(pChildObject);
	}
}

void CObject::update(void)
{
	this->doUpdate();
	m_bIsUpdateEnable = false;

	// 자식 객체 상태를 갱신한다
	for (auto pChildObject : m_oChildObjectList) {
		pChildObject->update();
	}
}

EObjectType CObject::getObjectType(void)
{
	return m_eObjectType;
}

D3DXVECTOR3 & CObject::getPosition(void)
{
	return m_stPosition;
}

D3DXVECTOR3 & CObject::getScale(void)
{
	return m_stScale;
}

D3DXVECTOR3 & CObject::getRotation(void)
{
	return m_stRotation;
}

D3DXVECTOR3 & CObject::getRightDirection(void)
{
	return m_stRightDirection;
}

D3DXVECTOR3 & CObject::getUpDirection(void)
{
	return m_stUpDirection;
}

D3DXVECTOR3 & CObject::getForwardDirection(void)
{
	return m_stForwardDirection;
}

D3DXMATRIXA16 & CObject::getWorldMatrix(void)
{
	return m_stWorldMatrix;
}

D3DXMATRIXA16 & CObject::getFinalWorldMatrix(void)
{
	// 부모 객체가 없을 경우
	if (m_pParentObject == nullptr) {
		return m_stWorldMatrix;
	}

	/*
	행렬을 통한 계층 구조 형성 방법
	:
	- Matrix(F) = Matrix(L) * Matrix(P)
	*/
	auto stParentMatrix = m_pParentObject->getFinalWorldMatrix();
	m_stFinalWorldMatrix = m_stWorldMatrix * stParentMatrix;

	return m_stFinalWorldMatrix;
}

CObject * CObject::getParentObject(void)
{
	return m_pParentObject;
}

void CObject::setOffset(const D3DXVECTOR3 & a_rstOffset)
{
	m_bIsUpdateEnable = true;

	D3DXMatrixTranslation(&m_stOffsetMatrix,
		a_rstOffset.x, a_rstOffset.y, a_rstOffset.z);
}

void CObject::setPosition(const D3DXVECTOR3 & a_rstPosition)
{
	m_bIsUpdateEnable = true;
	m_stPosition = a_rstPosition;
}

void CObject::setScale(const D3DXVECTOR3 & a_rstScale)
{
	m_bIsUpdateEnable = true;
	m_stScale = a_rstScale;
}

void CObject::setRotation(const D3DXVECTOR3 & a_rstRotation)
{
	m_stRotation = a_rstRotation;

	// 방향 벡터를 설정한다
	// {
	D3DXMATRIXA16 stRotate;

	D3DXMatrixRotationYawPitchRoll(&stRotate,
		D3DXToRadian(a_rstRotation.y),
		D3DXToRadian(a_rstRotation.x),
		D3DXToRadian(a_rstRotation.z));

	CopyMemory(&m_stRightDirection, 
		&stRotate(0, 0), sizeof(m_stRightDirection));

	CopyMemory(&m_stUpDirection,
		&stRotate(1, 0), sizeof(m_stUpDirection));

	CopyMemory(&m_stForwardDirection,
		&stRotate(2, 0), sizeof(m_stForwardDirection));
	// }

	// 방향 벡터를 정규화한다
	this->normalizeDirections();
}

void CObject::setRightDirection(const D3DXVECTOR3 & a_rstRightDirection)
{
	m_stUpDirection = WORLD_UP_DIRECTION;
	m_stForwardDirection = WORLD_FORWARD_DIRECTION;

	m_stRightDirection = a_rstRightDirection;
	D3DXVec3Normalize(&m_stRightDirection, &m_stRightDirection);

	D3DXVec3Cross(&m_stUpDirection, 
		&m_stForwardDirection, &a_rstRightDirection);

	D3DXVec3Cross(&m_stForwardDirection,
		&a_rstRightDirection, &m_stUpDirection);

	// 방향 벡터를 정규화한다
	this->normalizeDirections();
}

void CObject::setUpDirection(const D3DXVECTOR3 & a_rstUpDirection)
{
	m_stRightDirection = WORLD_RIGHT_DIRECTION;
	m_stForwardDirection = WORLD_FORWARD_DIRECTION;

	m_stUpDirection = a_rstUpDirection;
	D3DXVec3Normalize(&m_stUpDirection, &m_stUpDirection);

	D3DXVec3Cross(&m_stRightDirection,
		&a_rstUpDirection, &m_stForwardDirection);

	D3DXVec3Cross(&m_stForwardDirection,
		&m_stRightDirection, &a_rstUpDirection);

	// 방향 벡터를 정규화한다
	this->normalizeDirections();
}

void CObject::setForwardDirection(const D3DXVECTOR3 & a_rstForwardDirection)
{
	m_stRightDirection = WORLD_RIGHT_DIRECTION;
	m_stUpDirection = WORLD_UP_DIRECTION;

	m_stForwardDirection = a_rstForwardDirection;
	D3DXVec3Normalize(&m_stForwardDirection, &m_stForwardDirection);

	D3DXVec3Cross(&m_stRightDirection,
		&m_stUpDirection, &a_rstForwardDirection);

	D3DXVec3Cross(&m_stUpDirection,
		&a_rstForwardDirection, &m_stRightDirection);

	// 방향 벡터를 정규화한다
	this->normalizeDirections();
}

void CObject::setWorldMatrix(const D3DXMATRIXA16 & a_rstWorldMatrix)
{
	m_stWorldMatrix = a_rstWorldMatrix;
}

void CObject::setParentObject(CObject * a_pParentObject)
{
	assert(a_pParentObject == nullptr || m_pParentObject == nullptr);
	m_pParentObject = a_pParentObject;
}

void CObject::resetDirections(void)
{
	D3DXVec3Cross(&m_stRightDirection, 
		&m_stUpDirection, &m_stForwardDirection);

	D3DXVec3Cross(&m_stUpDirection,
		&m_stForwardDirection, &m_stRightDirection);

	D3DXVec3Cross(&m_stForwardDirection,
		&m_stRightDirection, &m_stUpDirection);

	// 방향 벡터를 정규화한다
	this->normalizeDirections();
}

void CObject::normalizeDirections(void)
{
	m_bIsUpdateEnable = true;

	D3DXVec3Normalize(&m_stRightDirection, &m_stRightDirection);
	D3DXVec3Normalize(&m_stUpDirection, &m_stUpDirection);
	D3DXVec3Normalize(&m_stForwardDirection, &m_stForwardDirection);
}

void CObject::moveByRightDirection(float a_fOffset, bool a_bIsLocal)
{
	if (a_bIsLocal) {
		this->moveByDirection(m_stRightDirection * a_fOffset);
	}
	else {
		this->moveByDirection(WORLD_RIGHT_DIRECTION * a_fOffset);
	}
}

void CObject::moveByUpDirection(float a_fOffset, bool a_bIsLocal)
{
	if (a_bIsLocal) {
		this->moveByDirection(m_stUpDirection * a_fOffset);
	}
	else {
		this->moveByDirection(WORLD_UP_DIRECTION * a_fOffset);
	}
}

void CObject::moveByForwardDirection(float a_fOffset, bool a_bIsLocal)
{
	if (a_bIsLocal) {
		this->moveByDirection(m_stForwardDirection * a_fOffset);
	}
	else {
		this->moveByDirection(WORLD_FORWARD_DIRECTION * a_fOffset);
	}
}

void CObject::moveByDirection(const D3DXVECTOR3 & a_rstDirection)
{
	m_stPosition += a_rstDirection;
	m_bIsUpdateEnable = true;
}

void CObject::rotateByRightDirection(float a_fAngle, bool a_bIsLocal)
{
	D3DXMATRIXA16 stRotate;

	/*
	D3DXMatrixRotationAxis 함수는 특정 축을 기준으로 회전을 수행 할 수 있는
	회전 행렬을 계산하는 역할을 한다.
	*/
	if (a_bIsLocal) {
		D3DXMatrixRotationAxis(&stRotate,
			&m_stRightDirection, D3DXToRadian(a_fAngle));
	}
	else {
		D3DXMatrixRotationAxis(&stRotate,
			&WORLD_RIGHT_DIRECTION, D3DXToRadian(a_fAngle));
	}

	this->rotateByMatrix(stRotate);
}

void CObject::rotateByUpDirection(float a_fAngle, bool a_bIsLocal)
{
	D3DXMATRIXA16 stRotate;

	if(a_bIsLocal) {
		D3DXMatrixRotationAxis(&stRotate,
			&m_stUpDirection, D3DXToRadian(a_fAngle));
	}
	else {
		D3DXMatrixRotationAxis(&stRotate,
			&WORLD_UP_DIRECTION, D3DXToRadian(a_fAngle));
	}

	this->rotateByMatrix(stRotate);
}

void CObject::rotateByForwardDirection(float a_fAngle, bool a_bIsLocal)
{
	D3DXMATRIXA16 stRotate;

	if (a_bIsLocal) {
		D3DXMatrixRotationAxis(&stRotate,
			&m_stForwardDirection, D3DXToRadian(a_fAngle));
	}
	else {
		D3DXMatrixRotationAxis(&stRotate,
			&WORLD_FORWARD_DIRECTION, D3DXToRadian(a_fAngle));
	}

	this->rotateByMatrix(stRotate);
}

void CObject::rotateByPosition(const D3DXVECTOR3 & a_rstPosition, bool a_bIsShpere)
{
	D3DXVECTOR3 stDirection = a_rstPosition - m_stPosition;

	/*
	a_bIsShpere 가 참이라고 한다면 해당 방향을 주시하기 위해서 X, Y, Z 축에
	대한 회전이 발생한다. (즉, 전체 빌보드를 만드는 효과가 있다.)

	반면, a_bIsSphere 가 거짓이라면 Y 축에 대한 회전만을 수행하기 때문에
	이는 Y 축 빌보드를 만드는 효과를 나타낸다.
	*/
	if (!a_bIsShpere) {
		stDirection.y = 0.0f;
	}

	this->setForwardDirection(stDirection);
}

void CObject::rotateByMatrix(const D3DXMATRIXA16 & a_rstMatrix)
{
	/*
	D3DXVec3TransformNormal 함수는 벡터의 w 요소를 0 으로 가정하고
	행렬과의 변환을 수행하는 역할을 한다. (즉, w 요소가 0 이기 때문에
	이동 변환은 해당 벡터에 아무런 영향을 미치지 않는다.)
	*/
	D3DXVec3TransformNormal(&m_stRightDirection,
		&m_stRightDirection, &a_rstMatrix);

	D3DXVec3TransformNormal(&m_stUpDirection,
		&m_stUpDirection, &a_rstMatrix);

	D3DXVec3TransformNormal(&m_stForwardDirection,
		&m_stForwardDirection, &a_rstMatrix);

	// 방향 벡터를 초기화한다
	this->resetDirections();
}

void CObject::addChildObject(CObject * a_pChildObject)
{
	assert(a_pChildObject->getParentObject() == nullptr);

	a_pChildObject->setParentObject(this);
	m_oChildObjectList.push_back(a_pChildObject);
}

void CObject::removeChildObject(CObject * a_pChildObject, bool a_bIsRemove)
{
	auto oIterator = std::find(m_oChildObjectList.begin(),
		m_oChildObjectList.end(), a_pChildObject);

	if (oIterator != m_oChildObjectList.end()) {
		m_oChildObjectList.erase(oIterator);
		a_pChildObject->setParentObject(nullptr);

		if (a_bIsRemove) {
			SAFE_DELETE(a_pChildObject);
		}
	}
}

void CObject::doUpdate(void)
{
	if (m_bIsUpdateEnable) {
		// 이동 행렬을 설정한다
		// {
		D3DXMATRIXA16 stTranslate;

		D3DXMatrixTranslation(&stTranslate,
			m_stPosition.x, m_stPosition.y, m_stPosition.z);
		// }

		// 척도 행렬을 설정한다
		// {
		D3DXMATRIXA16 stScale;

		D3DXMatrixScaling(&stScale,
			m_stScale.x, m_stScale.y, m_stScale.z);
		// }

		/*
		회전 행렬 요소
		:
		1행: Rx, Ry, Rz,  0		<- X 축 방향 벡터
		2행: Ux, Uy, Uz,  0		<- Y 축 방향 벡터
		3행: Fx, Fy, Fz,  0		<- Z 축 방향 벡터
		4행:  0,  0,  0,  1
		*/
		// 회전 행렬을 설정한다
		// {
		D3DXMATRIXA16 stRotate;
		D3DXMatrixIdentity(&stRotate);

		CopyMemory(&stRotate(0, 0),
			&m_stRightDirection, sizeof(m_stRightDirection));

		CopyMemory(&stRotate(1, 0),
			&m_stUpDirection, sizeof(m_stUpDirection));

		CopyMemory(&stRotate(2, 0),
			&m_stForwardDirection, sizeof(m_stForwardDirection));
		// }

		m_stWorldMatrix = m_stOffsetMatrix * stScale * stRotate * stTranslate;
	}
}
