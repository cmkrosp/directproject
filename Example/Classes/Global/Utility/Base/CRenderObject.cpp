#include "CRenderObject.h"
#include "../../Function/GlobalFunction.h"
#include "../Base/CDirect3DApplication.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Debug/CDebugRenderer.h"
#include "../Render/CShader.h"
#include "../Object/CCamera.h"

CRenderObject::CRenderObject(void)
:
CRenderObject(STParameters{
	0, DXGI_FORMAT_UNKNOWN, "", "", "", "", "", ""
})
{
	/*
	C++ 객체를 ZeroMemory 함수로 초기화 할 경우 내부적으로 클래스에
	대한 정보가 소멸되기 때문에 C++ 객체는 절대 ZeroMemory 함수로 
	초기화 시키면 안된다.
	*/
	//ZeroMemory(&m_stParameters, sizeof(m_stParameters));
}

CRenderObject::CRenderObject(const STParameters & a_rstParameters)
:
m_stParameters(a_rstParameters),
m_stColor(1.0f, 1.0f, 1.0f, 1.0f)
{
	m_eObjectType = EObjectType::RENDER_OBJECT;
}

CRenderObject::~CRenderObject(void)
{
	SAFE_DELETE(m_pDebugRenderer);
}

std::vector<STBoundingBox>& CRenderObject::getBoundingBoxList(void)
{
	return m_oBoundingBoxList;
}

std::vector<STBoundingSphere>& CRenderObject::getBoundingSphereList(void)
{
	return m_oBoundingSphereList;
}

std::vector<STObjectBox>& CRenderObject::getFinalObjectBoxList(void)
{
	auto &rBoundingBoxList = this->getFinalBoundingBoxList();
	m_oFinalObjectBoxList.clear();

	for (int i = 0; i < rBoundingBoxList.size(); ++i) {
		auto &rstBoundingBox = rBoundingBoxList[i];

		STObjectBox stObjectBox;
		ZeroMemory(&stObjectBox, sizeof(stObjectBox));

		stObjectBox.m_astDirections[0] = m_stRightDirection;
		stObjectBox.m_astDirections[1] = m_stUpDirection;
		stObjectBox.m_astDirections[2] = m_stForwardDirection;

		for (int j = 0; j < NUM_DIRECTIONS; ++j) {
			auto stDirection = stObjectBox.m_astDirections[j];
			auto stHalfDirection = (rstBoundingBox.m_stMax - rstBoundingBox.m_stMin) / 2.0f;

			stObjectBox.m_afHalfLengths[j] = D3DXVec3Dot(&stDirection, &stHalfDirection);
		}

		stObjectBox.m_stCenter = (rstBoundingBox.m_stMin + rstBoundingBox.m_stMax) / 2.0f;
		m_oFinalObjectBoxList.push_back(stObjectBox);
	}

	return m_oFinalObjectBoxList;
}

std::vector<STBoundingBox>& CRenderObject::getFinalBoundingBoxList(void)
{
	auto &rstWorldMatrix = this->getFinalWorldMatrix();
	m_oFinalBoundingBoxList.clear();

	for (int i = 0; i < m_oBoundingBoxList.size(); ++i) {
		auto stBoundingBox = m_oBoundingBoxList[i];

		D3DXVec3TransformCoord(&stBoundingBox.m_stMin,
			&stBoundingBox.m_stMin, &rstWorldMatrix);

		D3DXVec3TransformCoord(&stBoundingBox.m_stMax,
			&stBoundingBox.m_stMax, &rstWorldMatrix);

		m_oFinalBoundingBoxList.push_back(stBoundingBox);
	}

	return m_oFinalBoundingBoxList;
}

std::vector<STBoundingSphere>& CRenderObject::getFinalBoundingSphereList(void)
{
	auto &rstWorldMatrix = this->getFinalWorldMatrix();
	m_oFinalBoundingSphereList.clear();

	for (int i = 0; i < m_oBoundingSphereList.size(); ++i) {
		auto stBoundingSphere = m_oBoundingSphereList[i];
		stBoundingSphere.m_fRadius *= max(max(m_stScale.x, m_stScale.y), m_stScale.z);

		D3DXVec3TransformCoord(&stBoundingSphere.m_stCenter,
			&stBoundingSphere.m_stCenter, &rstWorldMatrix);

		m_oFinalBoundingSphereList.push_back(stBoundingSphere);
	}

	return m_oFinalBoundingSphereList;
}

void CRenderObject::setDebugEnable(bool a_bIsDebugEnable, EDebugRenderType a_eDebugRenderType)
{
	if (m_bIsDebugEnable != a_bIsDebugEnable) {
		m_bIsDebugEnable = a_bIsDebugEnable;

		if (!a_bIsDebugEnable) {
			SAFE_DELETE(m_pDebugRenderer);
		}
		else {
			m_pDebugRenderer = Create<CDebugRenderer>(this, 
				a_eDebugRenderType);
		}
	}
}

void CRenderObject::setRenderEnable(bool a_bIsRenderEnable)
{
	m_bIsRenderEnable = a_bIsRenderEnable;
}

void CRenderObject::setAlphaBlendEnable(bool a_bIsAlphaBlendEnable)
{
	m_bIsAlphaBlendEnable = a_bIsAlphaBlendEnable;
}

void CRenderObject::setColor(const D3DXCOLOR & a_rstColor)
{
	m_stColor = a_rstColor;
}

void CRenderObject::setBoundingBoxList(const std::vector<STBoundingBox>& a_rBoundingBoxList)
{
	m_oBoundingBoxList = a_rBoundingBoxList;
}

void CRenderObject::setBoundingSphereList(const std::vector<STBoundingSphere>& a_rBoundingSphereList)
{
	m_oBoundingSphereList = a_rBoundingSphereList;
}

void CRenderObject::render(void)
{
	if (m_bIsRenderEnable) {
		this->preRender();
		this->doRender();
		this->postRender();

		if (m_bIsDebugEnable) {
			m_pDebugRenderer->render();
		}

		for (auto pChildObject : m_oChildObjectList) {
			if (pChildObject->getObjectType() == EObjectType::RENDER_OBJECT) {
				static_cast<CRenderObject *>(pChildObject)->render();
			}
		}
	}
}

void CRenderObject::addBoundingBox(const STBoundingBox & a_rstBoundingBox)
{
	m_oBoundingBoxList.push_back(a_rstBoundingBox);
}

void CRenderObject::addBoundingSphere(const STBoundingSphere & a_rstBoundingSphere)
{
	m_oBoundingSphereList.push_back(a_rstBoundingSphere);
}

void CRenderObject::preRender(void)
{
	// 정점 버퍼를 설정한다
	// {
	UINT nStride = m_stParameters.m_nVertexSize;
	UINT nOffset = 0;

	auto pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);
	GET_CONTEXT()->IASetVertexBuffers(0, 1, &pVertexBuffer, &nStride, &nOffset);
	// }

	// 인덱스 버퍼를 설정한다
	auto pIndexBuffer = GET_BUFFER(m_stParameters.m_oIndexBufferKey);
	GET_CONTEXT()->IASetIndexBuffer(pIndexBuffer, m_stParameters.m_eIndexFormat, 0);

	// 변환 버퍼를 설정한다
	auto pTransformBuffer = GET_BUFFER(m_stParameters.m_oTransformBufferKey);
	GET_CONTEXT()->VSSetConstantBuffers(0, 1, &pTransformBuffer);

	// 렌더 버퍼를 설정한다
	auto pRenderBuffer = GET_BUFFER(m_stParameters.m_oRenderBufferKey);
	GET_CONTEXT()->VSSetConstantBuffers(1, 1, &pRenderBuffer);

	// 정점 쉐이더, 픽셀 쉐이더를 설정한다
	// {
	auto pVertexShader = GET_VERTEX_SHADER(m_stParameters.m_oShaderKey);
	auto pPixelShader = GET_PIXEL_SHADER(m_stParameters.m_oShaderKey);

	GET_CONTEXT()->VSSetShader(pVertexShader, NULL, 0);
	GET_CONTEXT()->PSSetShader(pPixelShader, NULL, 0);
	// }

	// 입력 레이아웃을 설정한다
	auto pInputLayout = GET_INPUT_LAYOUT(m_stParameters.m_oInputLayoutKey);
	GET_CONTEXT()->IASetInputLayout(pInputLayout);

	// 블렌드 상태를 설정한다
	// {
	std::string oBlendStateKey = m_bIsAlphaBlendEnable ? KEY_ALPHA_BLEND_ENABLE_STATE 
		: KEY_ALPHA_BLEND_DISABLE_STATE;

	auto pBlendState = GET_BLEND_STATE(oBlendStateKey);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);
	// }

	// 정점 집합을 설정한다
	GET_CONTEXT()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void CRenderObject::doRender(void)
{
	auto pTransformBuffer = GET_BUFFER(m_stParameters.m_oTransformBufferKey);

	if (pTransformBuffer != nullptr) {
		auto oTransformBufferPtr = GetBufferPointer<STTransform>(pTransformBuffer);
		auto pstTransform = oTransformBufferPtr.get();
		
		pstTransform->m_stWorldMatrix = this->getFinalWorldMatrix();
		pstTransform->m_stViewMatrix = GET_VIEW_MATRIX();
		pstTransform->m_stProjectionMatrix = GET_PROJECTION_MATRIX();
	}
}

void CRenderObject::postRender(void)
{
	auto pBlendState = GET_BLEND_STATE(KEY_ALPHA_BLEND_DISABLE_STATE);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);
}
