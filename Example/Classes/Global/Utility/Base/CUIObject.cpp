#include "CUIObject.h"
#include "../../../Global/Function/GlobalFunction.h"
#include "../../../Global/Utility/Manager/CDeviceManager.h"
#include "../../../Global/Utility/Manager/CResourceManager.h"
#include "../../../Global/Utility/Render/CShader.h"
#include "../../../Global/Utility/Manager/CWindowManager.h"

CUIObject::CUIObject(void)
	: m_eObjectType(EObjectType::UI_OBJECT)
	, m_stLocalPosition(0.0f, 0.0f)
	, m_stWorldPosition(0.0f, 0.0f)
	, m_stScale(1.0f, 1.0f)
	, m_pParent(nullptr)
	, m_nTag(0)
	, m_isHide(false)
{
}

CUIObject::~CUIObject(void)
{
	for (auto pChildObject : m_vecChildren) {
		SAFE_DELETE(pChildObject);
	}
}

void CUIObject::update(void)
{
	m_stWorldPosition = m_stLocalPosition;

	if (m_pParent)
	{
		m_stWorldPosition += m_pParent->m_stWorldPosition;
	}

	for (auto pChildObject : m_vecChildren) {
		pChildObject->update();
	}
}

void CUIObject::render(void)
{
	if (m_isHide) return;

	for each (auto pChild in m_vecChildren)
	{
		pChild->render();
	}
}

void CUIObject::addChildObject(CUIObject * a_pChildObject)
{
	if (a_pChildObject)
	{
		a_pChildObject->setParent(this);
		m_vecChildren.push_back(a_pChildObject);
	}
}

CUIObject * CUIObject::findChildObjectbyTag(int a_nTag)
{
	if (m_nTag == a_nTag)
		return this;

	for each (auto pChild in m_vecChildren)
	{
		CUIObject* pObj = pChild->findChildObjectbyTag(a_nTag);
		if (pObj)
		{
			return pObj;
		}
	}
	return nullptr;
}

void CUIObject::removeChildObject(CUIObject * a_pChildObject)
{
	for (int i = 0; i < m_vecChildren.size(); ++i)
	{
		if (m_vecChildren[i] == a_pChildObject)
		{
			m_vecChildren.erase(m_vecChildren.begin() + i);

			break;
		}
	}
}

void CUIObject::removeChildObjectbyTag(int a_nTag)
{
	for (int i = 0; i < m_vecChildren.size(); ++i)
	{
		CUIObject* pObj = m_vecChildren[i]->findChildObjectbyTag(a_nTag);
		if (pObj)
		{
			m_vecChildren.erase(m_vecChildren.begin() + i);

			break;
		}
	}
}
