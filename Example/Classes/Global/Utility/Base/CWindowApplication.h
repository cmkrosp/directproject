#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IWindowMessageHandler.h"

//! 윈도우 어플리케이션
class CWindowApplication : public IWindowMessageHandler
{
public:			// 인터페이스 구현

	//! 윈도우 메세지를 처리한다
	virtual LRESULT handleWindowMessage(HWND a_hWindow,
		UINT a_nMessage, WPARAM a_wParam, LPARAM a_lParam) override;

public:			// getter, setter

	//! 윈도우 어플리케이션을 반환한다
	static CWindowApplication * getInstance(void);

	//! 윈도우 크기를 변경한다
	virtual void setWindowSize(const SIZE &a_rstWindowSize);

public:			// public 함수

	//! 어플리케이션을 구동한다
	int run(void);

protected:			// protected 함수

	//! 초기화
	virtual void init(void);

	//! 메세지 루프를 구동한다
	virtual int runMessageLoop(void);

	//! 제거 메세지를 처리한다
	virtual void handleDestroyMessage(WPARAM a_wParam, LPARAM a_lParam);

protected:			// 생성자, 소멸자

	//! 생성자
	CWindowApplication(HINSTANCE a_hInstance,
		int a_nShowOptions, const SIZE &a_rstWindowSize);

	/*
	순수 가상 함수는 구현부가 없이 함수의 원형 (인터페이스) 만 존재하는
	함수를 의미한다. (즉, 순수 가상 함수가 1 개 이상 선언되어 있으면
	해당 클래스는 객체화 시킬 수 없는 추상 클래스가 된다.)

	또한, 해당 클래스를 객체화 시키기 위해서는 해당 클래스를 상속한
	자식 클래스에서 반드시 순수 가상 함수를 구현해주어야한다.

	만약, 자식 클래스가 순수 가상 함수를 구현하지 않았을 경우 자식 클래스
	또한 추상 클래스가 되기 때문에 객체화 시키는 것이 불가능하다.
	*/
	//! 소멸자
	virtual ~CWindowApplication(void) = 0;

protected:			// protected 변수

	SIZE m_stWindowSize;
	
	int m_nShowOptions = 0;
	HINSTANCE m_hInstance = nullptr;
};
