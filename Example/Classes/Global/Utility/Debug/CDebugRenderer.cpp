#include "CDebugRenderer.h"
#include "../Base/CDirect3DApplication.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Object/CCamera.h"
#include "../Render/CMesh.h"

CDebugRenderer::CDebugRenderer(CRenderObject * a_pTargetObject, EDebugRenderType a_eDebugRenderType)
:
CRenderObject(CRenderObject::STParameters{
	0,
	DXGI_FORMAT_UNKNOWN,

	KEY_COLOR_SHADER,
	"",
	"",
	KEY_TRANSFORM_BUFFER,
	KEY_COLOR_RENDER_BUFFER,
	KEY_COLOR_INPUT_LAYOUT
}),

m_pTargetObject(a_pTargetObject),
m_eDebugRenderType(a_eDebugRenderType)
{
	if (a_eDebugRenderType == EDebugRenderType::BOX) {
		auto &rBoundingBoxList = m_pTargetObject->getBoundingBoxList();

		for (int i = 0; i < rBoundingBoxList.size(); ++i) {
			auto stBoundingBox = rBoundingBoxList[i];
			
			std::string oMeshKey = GetFormatString("BoundingBoxMeshMin%f,%f,%fMax%f,%f,%f",
				stBoundingBox.m_stMin.x, stBoundingBox.m_stMin.y, stBoundingBox.m_stMin.z,
				stBoundingBox.m_stMax.x, stBoundingBox.m_stMax.y, stBoundingBox.m_stMax.z);

			auto pMesh = GET_MESH(oMeshKey);

			if (pMesh == nullptr) {
				pMesh = CreateBoxMesh(INPUT_COLOR_ELEMENT_DESCS,
					fabsf(stBoundingBox.m_stMin.x - stBoundingBox.m_stMax.x),
					fabsf(stBoundingBox.m_stMin.y - stBoundingBox.m_stMax.y),
					fabsf(stBoundingBox.m_stMin.z - stBoundingBox.m_stMax.z));

				GET_RESOURCE_MANAGER()->addMesh(oMeshKey, pMesh);
			}

			m_oMeshList.push_back(pMesh);
		}
	}
	else {
		auto &rBoundingSphereList = m_pTargetObject->getBoundingSphereList();

		for (int i = 0; i < rBoundingSphereList.size(); ++i) {
			auto stBoundingSphere = rBoundingSphereList[i];

			std::string oMeshKey = GetFormatString("BoundingSphereMeshRadius%fCenter%f,%f,%f",
				stBoundingSphere.m_fRadius,
				stBoundingSphere.m_stCenter.x, stBoundingSphere.m_stCenter.y, stBoundingSphere.m_stCenter.z);

			auto pMesh = GET_MESH(oMeshKey);

			if (pMesh == nullptr) {
				pMesh = CreateSphereMesh(INPUT_COLOR_ELEMENT_DESCS,
					stBoundingSphere.m_fRadius);

				GET_RESOURCE_MANAGER()->addMesh(oMeshKey, pMesh);
			}

			m_oMeshList.push_back(pMesh);
		}
	}
}

void CDebugRenderer::preRender(void)
{
	CRenderObject::preRender();

	auto pRasterizerState = GET_RASTERIZER_STATE(KEY_WIREFRAME_RASTERIZER_STATE);
	GET_CONTEXT()->RSSetState(pRasterizerState);
}

void CDebugRenderer::doRender(void)
{
	CRenderObject::doRender();

	if (m_eDebugRenderType == EDebugRenderType::BOX) {
		this->renderBoundingBox();
	}
	else {
		this->renderBoundingSphere();
	}
}

void CDebugRenderer::postRender(void)
{
	CRenderObject::postRender();

	auto pRasterizerState = GET_RASTERIZER_STATE(KEY_SOLID_RASTERIZER_STATE);
	GET_CONTEXT()->RSSetState(pRasterizerState);
}

void CDebugRenderer::renderBoundingBox(void)
{
	auto pTransformBuffer = GET_BUFFER(m_stParameters.m_oTransformBufferKey);

	if (pTransformBuffer != nullptr) {
		auto &rBoundingBoxList = m_pTargetObject->getBoundingBoxList();

		for (int i = 0; i < rBoundingBoxList.size(); ++i) {
			auto oTransformBufferPtr = GetBufferPointer<STTransform>(pTransformBuffer);
			auto pstTransform = oTransformBufferPtr.get();

			// 간격 행렬을 설정한다
			// {
			D3DXMATRIXA16 stOffsetMatrix;
			
			D3DXMatrixTranslation(&stOffsetMatrix,
				(rBoundingBoxList[i].m_stMin.x + rBoundingBoxList[i].m_stMax.x) / 2.0f,
				(rBoundingBoxList[i].m_stMin.y + rBoundingBoxList[i].m_stMax.y) / 2.0f,
				(rBoundingBoxList[i].m_stMin.z + rBoundingBoxList[i].m_stMax.z) / 2.0f);
			// }

			pstTransform->m_stWorldMatrix = stOffsetMatrix * m_pTargetObject->getFinalWorldMatrix();
			pstTransform->m_stViewMatrix = GET_VIEW_MATRIX();
			pstTransform->m_stProjectionMatrix = GET_PROJECTION_MATRIX();

			// 버퍼를 설정한다
			m_oMeshList[i]->setupBuffers();

			// 물체를 그린다
			// {
			DWORD nNumAttributes = m_oMeshList[i]->getNumAttributes();

			for (int j = 0; j < nNumAttributes; ++j) {
				auto stAttribute = m_oMeshList[i]->getAttribute(j);
				GET_CONTEXT()->DrawIndexed(stAttribute.FaceCount * 3, stAttribute.FaceStart * 3, 0);
			}
			// }
		}
	}
}

void CDebugRenderer::renderBoundingSphere(void)
{
	auto pTransformBuffer = GET_BUFFER(m_stParameters.m_oTransformBufferKey);

	if (pTransformBuffer != nullptr) {
		auto &rBoundingSphereList = m_pTargetObject->getBoundingSphereList();

		for (int i = 0; i < rBoundingSphereList.size(); ++i) {
			auto oTransformBufferPtr = GetBufferPointer<STTransform>(pTransformBuffer);
			auto pstTransform = oTransformBufferPtr.get();

			// 간격 행렬을 설정한다
			// {
			D3DXMATRIXA16 stOffsetMatrix;

			D3DXMatrixTranslation(&stOffsetMatrix,
				rBoundingSphereList[i].m_stCenter.x,
				rBoundingSphereList[i].m_stCenter.y,
				rBoundingSphereList[i].m_stCenter.z);
			// }

			pstTransform->m_stWorldMatrix = stOffsetMatrix * m_pTargetObject->getFinalWorldMatrix();
			pstTransform->m_stViewMatrix = GET_VIEW_MATRIX();
			pstTransform->m_stProjectionMatrix = GET_PROJECTION_MATRIX();

			// 버퍼를 설정한다
			m_oMeshList[i]->setupBuffers();

			// 물체를 그린다
			// {
			DWORD nNumAttributes = m_oMeshList[i]->getNumAttributes();

			for (int j = 0; j < nNumAttributes; ++j) {
				auto stAttribute = m_oMeshList[i]->getAttribute(j);
				GET_CONTEXT()->DrawIndexed(stAttribute.FaceCount * 3, stAttribute.FaceStart * 3, 0);
			}
			// }
		}
	}
}
