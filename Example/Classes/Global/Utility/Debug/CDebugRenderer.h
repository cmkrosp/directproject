#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

class CMesh;

//! 디버그 렌더러
class CDebugRenderer : public CRenderObject
{
public:			// 생성자

	//! 생성자
	CDebugRenderer(CRenderObject *a_pTargetObject,
		EDebugRenderType a_eDebugRenderType);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;

	//! 물체를 그린 후
	virtual void postRender(void) override;

private:			// private 함수

	//! 경계 상자를 그린다
	void renderBoundingBox(void);

	//! 경계 구를 그린다
	void renderBoundingSphere(void);

private:			// private 변수

	std::vector<CMesh *> m_oMeshList;

	CRenderObject *m_pTargetObject = nullptr;
	EDebugRenderType m_eDebugRenderType = EDebugRenderType::NONE;
};
