#include "CGizmo.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"

CGizmo::CGizmo(int a_nLength)
:
CRenderObject(CRenderObject::STParameters{
	sizeof(STColorVertex),
	DXGI_FORMAT_UNKNOWN,

	KEY_COLOR_SHADER,
	GetFormatString("GizmoVertexBuffer%d", a_nLength),
	"",
	KEY_TRANSFORM_BUFFER,
	KEY_COLOR_RENDER_BUFFER,
	KEY_COLOR_INPUT_LAYOUT
}),

m_nLength(a_nLength)
{
	m_pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);

	if (m_pVertexBuffer == nullptr) {
		m_pVertexBuffer = this->createVertexBuffer();
		
		GET_RESOURCE_MANAGER()->addBuffer(m_stParameters.m_oVertexBufferKey, 
			m_pVertexBuffer);
	}
}

void CGizmo::preRender(void)
{
	CRenderObject::preRender();
	GET_CONTEXT()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	// 렌더 정보를 설정한다
	// {
	auto pRenderBuffer = GET_BUFFER(m_stParameters.m_oRenderBufferKey);

	if (pRenderBuffer != nullptr) {
		auto oRenderBufferPtr = GetBufferPointer<STColorRender>(pRenderBuffer);
		auto pstRender = oRenderBufferPtr.get();

		pstRender->m_stColor = m_stColor;
	}
	// }
}

void CGizmo::doRender(void)
{
	CRenderObject::doRender();
	GET_CONTEXT()->Draw(18, 0);
}

ID3D11Buffer * CGizmo::createVertexBuffer(void)
{
	// 정점 버퍼를 생성한다
	auto pVertexBuffer = CreateBuffer(sizeof(STColorVertex) * 18,
		D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	// 정점 정보를 설정한다
	// {
	auto oVertexBufferPtr = GetBufferPointer<STColorVertex>(pVertexBuffer);
	auto pstVertices = oVertexBufferPtr.get();

	// X 축
	// {
	pstVertices[0].m_stPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	pstVertices[0].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	pstVertices[1].m_stPosition = D3DXVECTOR3(m_nLength, 0.0f, 0.0f);
	pstVertices[1].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	pstVertices[2].m_stPosition = D3DXVECTOR3(m_nLength - 1.0f, 0.0f, 1.0f);
	pstVertices[2].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	pstVertices[3].m_stPosition = pstVertices[1].m_stPosition;
	pstVertices[3].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	pstVertices[4].m_stPosition = D3DXVECTOR3(m_nLength - 1.0f, 0.0f, -1.0f);
	pstVertices[4].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);

	pstVertices[5].m_stPosition = pstVertices[1].m_stPosition;
	pstVertices[5].m_stColor = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
	// }

	// Y 축
	// {
	pstVertices[6].m_stPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	pstVertices[6].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);

	pstVertices[7].m_stPosition = D3DXVECTOR3(0.0f, m_nLength, 0.0f);
	pstVertices[7].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);

	pstVertices[8].m_stPosition = D3DXVECTOR3(-1.0f, m_nLength - 1.0f, 0.0f);
	pstVertices[8].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);

	pstVertices[9].m_stPosition = pstVertices[7].m_stPosition;
	pstVertices[9].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);

	pstVertices[10].m_stPosition = D3DXVECTOR3(1.0f, m_nLength - 1.0f, 0.0f);
	pstVertices[10].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);

	pstVertices[11].m_stPosition = pstVertices[7].m_stPosition;
	pstVertices[11].m_stColor = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);
	// }

	// Z 축
	// {
	pstVertices[12].m_stPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	pstVertices[12].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	pstVertices[13].m_stPosition = D3DXVECTOR3(0.0f, 0.0f, m_nLength);
	pstVertices[13].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	pstVertices[14].m_stPosition = D3DXVECTOR3(-1.0f, 0.0f, m_nLength - 1.0f);
	pstVertices[14].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	pstVertices[15].m_stPosition = pstVertices[13].m_stPosition;
	pstVertices[15].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	pstVertices[16].m_stPosition = D3DXVECTOR3(1.0f, 0.0f, m_nLength - 1.0f);
	pstVertices[16].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	pstVertices[17].m_stPosition = pstVertices[13].m_stPosition;
	pstVertices[17].m_stColor = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);
	// }
	// }

	return pVertexBuffer;
}
