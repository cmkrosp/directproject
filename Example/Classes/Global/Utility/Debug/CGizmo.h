#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

//! 기즈모
class CGizmo : public CRenderObject
{
public:			// 생성자

	//! 생성자
	CGizmo(int a_nLength = 5);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;

private:			// private 함수

	//! 정점 버퍼를 생성한다
	ID3D11Buffer * createVertexBuffer(void);

private:			// private 변수

	int m_nLength = 0;
	ID3D11Buffer *m_pVertexBuffer = nullptr;
};
