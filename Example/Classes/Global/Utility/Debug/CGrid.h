#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

//! 그리드
class CGrid : public CRenderObject
{
public:			// 생성자

	//! 생성자
	CGrid(int a_nSize = 5);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;
	
private:			// private 함수

	//! 정점 버퍼를 생성한다
	ID3D11Buffer * createVertexBuffer(void);

private:			// private 변수

	int m_nSize = 0;
	ID3D11Buffer *m_pVertexBuffer = nullptr;
};
