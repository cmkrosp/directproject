#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

class CLine : public CRenderObject
{
public:
	CLine(const D3DXVECTOR3& stP1, const D3DXVECTOR3& stP2);
	~CLine();

	virtual void update(const D3DXVECTOR3& stP1, const D3DXVECTOR3& stP2);

	virtual void preRender(void) override;
	virtual void doRender(void) override;

private:
	ID3D11Buffer * createVertexBuffer(const D3DXVECTOR3& stP1, const D3DXVECTOR3& stP2);

private:
	ID3D11Buffer *m_pVertexBuffer = nullptr;
};

