#include "CBattleManager.h"
#include "../../../Monster/Monster.h"
#include "../../Function/GlobalFunction.h"
#include "../../Utility/Object/CStaticMesh.h"
#include "../../Utility/Manager/CInputManager.h"
#include "..//..//Utility/Manager/CTimeManager.h"
#include "..//Object/CSkinnedMesh.h"
#include "..//Object/CParticleSystem.h"

void CBattleManager::init(Player* a_pPlayerMesh, Monster* a_pMonsterMesh)
{
	srand(time(NULL));
	m_pPlayer = a_pPlayerMesh;
	m_pMonster = a_pMonsterMesh;

	m_pPlayer->setDamage(5.0f);
	this->createMesh();
}

void CBattleManager::update()
{
	this->updatePosition();
	this->meshUpdate();
	this->setDebugMode();

	m_pPParticle->update();
	m_pMParticle->update();
	m_pMParticle->setPosition(m_pMHeadMesh->getPosition() / 2);
	m_pMParticle->startParticleEmit(0.0f);

	if(m_pPlayer->getState() == Player::EPlayerState::FLASH)
		this->stun();

	if (m_bIsFlashBomb)
	{
		ftime += GET_DELTA_TIME();

		if (ftime >= 2.0f)
		{
			m_pMonster->stopAnimation();
			m_pMonster->setAnimationRun(false);
			m_pMonster->setState(Monster::EState::STUN);
			ftime = 0.0f;
			m_bIsFlashBomb = false;
		}
	}

	if (m_pMonster->getState() != Monster::EState::DIE && m_pMonster->getState() != Monster::EState::APPEAR && m_pMonster->getState() != Monster::EState::WING && m_pMonster->getState() != Monster::EState::START_IDLE)
	{
		this->player_MonsterAttack();
		if (m_pMonster->getState() != Monster::EState::STUN)
		{
			this->monster_PlayerAttack();
			this->pmBoudningBoxCollision();
		}
	}
	if (!m_pMonster->getAnimationRun() && m_pMonster->getHP() <= 0)
	{
		m_pMonster->setState(Monster::EState::DIE);
	}
}

void CBattleManager::render()
{
	if (m_bIsDebug)
	{
		this->meshRender();
	}


	if(m_pMonster->getHP() <= 100)
	{
		m_pMParticle->render();
	}
	m_pPParticle->render();
}

bool CBattleManager::getCollision()
{
	return m_bIsCollision;
}

void CBattleManager::createMesh()
{
	m_pPAtkMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	m_pPAtkMesh->setScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	m_pPSAtkMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pPSAtkMesh->setScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	m_pPHitMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pPHitMesh->setScale(D3DXVECTOR3(1.0f, 2.5f, 1.5f));
	m_pPHitMesh->setColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_pMHeadMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMHeadMesh->setScale(D3DXVECTOR3(3.0f, 3.0f, 3.0f));

	m_pMTailMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMTailMesh->setScale(D3DXVECTOR3(3.0f, 3.0f, 3.0f));

	m_pMBodyMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMBodyMesh->setScale(D3DXVECTOR3(7.0f, 7.0f, 7.0f));

	m_pMForwardMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMForwardMesh->setScale(D3DXVECTOR3(10.0f, 3.0f, 10.0f));

	m_pMBackMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMBackMesh->setScale(D3DXVECTOR3(15.0f, 3.0f, 15.0f));

	m_pMRemoteMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pMRemoteMesh->setScale(D3DXVECTOR3(6.0f, 3.0f, 6.0f));

	m_pPParticle = Create<CParticleSystem>(CParticleSystem::STParameters{
	"Resources/Textures/particle_bl.png",					//이미지파서
	0.2f, 0.3f, 5.0f,										//나온 텍스처 효과시간, 시작 최대 스케일, 끝 최대 스케일
	D3DXCOLOR(0.5f, 0.0f, 0.0f, 0.5f),						//시작 텍스처 색상
	D3DXCOLOR(0.5f, 0.0f, 0.0f, 0.25f),						//끝 텍스처 색상
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),							//FRONT, RIGHT, UP 이동 최대치
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),							//BACK, LEFT, DOWN 이동 최대치
	0.0f, 10, 10											//이동 속도, 최대 파티클, 생성 파티클
		});
	m_pPParticle->setAlphaBlendEnable(true);

	m_pMParticle = Create<CParticleSystem>(CParticleSystem::STParameters{
	"Resources/Textures/particle_fire.png",					//이미지파서
	0.5f, 1.0f, 7.0f,										//나온 텍스처 효과시간, 시작 최대 스케일, 끝 최대 스케일
	D3DXCOLOR(0.7f, 0.3f, 0.3f, 1.0f),						//시작 텍스처 색상
	D3DXCOLOR(1.0f, 0.0f, 0.0f, 0.5f),						//끝 텍스처 색상
	D3DXVECTOR3(0.0f, 0.0f, 0.1f),							//BACK, LEFT, DOWN 이동 최대치
	D3DXVECTOR3(0.0f, 0.0f, 0.1f),							//FRONT, RIGHT, UP 이동 최대치
	0.2f, 150, 50											//이동 속도, 최대 파티클, 생성 파티클
		});
	m_pMParticle->setAlphaBlendEnable(true);
}

void CBattleManager::meshUpdate()
{
	m_pMTailMesh->update();
	m_pMHeadMesh->update();
	m_pMBodyMesh->update();
	m_pMBackMesh->update();
	m_pMForwardMesh->update();
	m_pMRemoteMesh->update();
	m_pPAtkMesh->update();
	m_pPSAtkMesh->update();
	m_pPHitMesh->update();
}

void CBattleManager::meshRender()
{
	//몬스터 박스
	m_pMHeadMesh->render();
	m_pMTailMesh->render();
	m_pMBodyMesh->render();
	m_pMForwardMesh->render();
	m_pMBackMesh->render();
	m_pMRemoteMesh->render();

	m_pPAtkMesh->render();
	m_pPSAtkMesh->render();
	m_pPHitMesh->render();
}

void CBattleManager::setDebugMode()
{
	if (IS_KEY_PRESSED(DIK_F1))
	{
		if (m_bIsDebug)
		{
			m_bIsDebug = false;
		}
		else
		{
			m_bIsDebug = true;
		}
	}
}

void CBattleManager::updatePosition()
{
	// 몬스터 바디 위치
	auto pBodyPos = m_pMonster->getBodyObjectBox().m_stCenter;
	m_pMHeadMesh->setPosition(pBodyPos);

	// 몬스터 꼬리 위치
	auto pTailPos = m_pMonster->getTailObjectBox().m_stCenter;
	m_pMTailMesh->setPosition(pTailPos);

	// 몬스터 힙 위치
	auto pHipPos = m_pMonster->getHipObjectBox().m_stCenter;
	m_pMBodyMesh->setPosition(pHipPos);

	// 몬스터 front 박스 위치
	auto pForward = m_pMonster->getFrontObjectBox().m_stCenter;
	m_pMForwardMesh->setPosition(pForward);

	// 몬스터 back 박스 위치
	auto pBack = m_pMonster->getBackObjectBox().m_stCenter;
	m_pMBackMesh->setPosition(pBack);

	// 몬스터 원거리 박스 위치
	auto pRemote = m_pMonster->getRemoteObjectBox().m_stCenter;
	m_pMRemoteMesh->setPosition(pRemote);

	// 플레이어 검 위치
	auto pAttackPos = m_pPlayer->getAtkObjectBox().m_stCenter;
	m_pPAtkMesh->setPosition(pAttackPos);

	// 플레이어 방패 위치
	auto pShieldPos = m_pPlayer->getSAtkObjectBox().m_stCenter;
	m_pPSAtkMesh->setPosition(pShieldPos);

	// 플레이어 히트 박스 위치
	auto pHitPos = m_pPlayer->getHitObjectBox().m_stCenter;
	m_pPHitMesh->setPosition(pHitPos);
}

bool CBattleManager::IntersectPMObjectBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh)
{
	if (IsIntersectObjectBox(a_pPMesh->getFinalObjectBoxList()[0], a_pMMesh->getFinalObjectBoxList()[0]))
	{
		return true;
	}

	return false;
}

bool CBattleManager::IntersectPMBoundingBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh)
{
	if (IsIntersectBox(a_pPMesh->getFinalBoundingBoxList()[0], a_pMMesh->getFinalBoundingBoxList()[0]))
	{
		return true;
	}

	return false;
}

void CBattleManager::monster_PlayerAttack()
{
	if (IntersectPMObjectBox(m_pPHitMesh, m_pMForwardMesh) && m_pMonster->getState() != Monster::EState::WALK)
	{
		if (!m_pMonster->getAnimationRun())
		{
			if (m_pMonster->getPrevState() == Monster::EState::BITE)
			{
				m_pMonster->setState(Monster::EState::BOUNDING_IDLE);
			}
			else
			{
				m_pMonster->setState(Monster::EState::BITE);
			}
		}
	
		if (IntersectPMObjectBox(m_pPHitMesh, m_pMHeadMesh) &&
			m_pPlayer->getStar() != true && 
			m_pPlayer->getState() != Player::EPlayerState::BLOCK)
		{
			if (m_pMonster->getBiteDblPercent() > 0.3f)
			{
				m_pPParticle->setPosition(D3DXVECTOR3(m_pPHitMesh->getPosition().x, m_pPHitMesh->getPosition().y, m_pPHitMesh->getPosition().z));
				m_pPParticle->startParticleEmit(0.2f);
				m_pPlayer->setState(Player::EPlayerState::SHIT);
				m_pPlayer->setHp(m_pPlayer->getHp() - m_pMonster->getDmg());
			}
			else if (m_pMonster->getState() == Monster::EState::RUN_BITE)
			{
				m_pPParticle->setPosition(m_pPHitMesh->getPosition());
				m_pPParticle->startParticleEmit(0.2f);
				m_pPlayer->setState(Player::EPlayerState::LHIT);
				m_pPlayer->setHp(m_pPlayer->getHp() - m_pMonster->getDmg());
			}
		}

	}
	else if(IntersectPMObjectBox(m_pPHitMesh, m_pMBackMesh))
	{
		if (!m_pMonster->getAnimationRun())
		{
			auto a_iChoice = rand() % 2;

			if (a_iChoice == 0)
			{
				m_pMonster->setState(Monster::EState::TAIL_ATTACK);
			}
			else
			{
				if (m_pMonster->getTurnTarget() == NULL)
					m_pMonster->setPlayerPosition(m_pPlayer);
				D3DXVECTOR3 v1 = m_pPlayer->m_pBaseMesh->getPosition() - m_pMonster->getMonsterPosition();
				D3DXVECTOR3 v2 = m_pMonster->getMonsterRightDirection();
				D3DXVec3Normalize(&v1, &v1);

				D3DXVECTOR3 v3;
				D3DXVec3Cross(&v3, &v1, &v2);

				if (v3.y < 0)
				{
					if (!m_pMonster->getAnimationRun())
						m_pMonster->setState(Monster::EState::RIGHT_TURN);
				}
				else
				{
					if (!m_pMonster->getAnimationRun())
						m_pMonster->setState(Monster::EState::LEFT_TURN);
				}
			}
		}
	
		if (IntersectPMObjectBox(m_pPHitMesh, m_pMTailMesh) &&
			!m_pPlayer->getStar() && m_pPlayer->getState() != Player::EPlayerState::BLOCK)
		{
			m_pPlayer->setPoison(true);
			m_pPParticle->setPosition(m_pPHitMesh->getPosition());
			m_pPParticle->startParticleEmit(0.2f);
			m_pPlayer->setState(Player::EPlayerState::LHIT);
			m_pPlayer->setHp(m_pPlayer->getHp() - m_pMonster->getDmg());
		}

	}
	else if (IntersectPMObjectBox(m_pPHitMesh, m_pMRemoteMesh))
	{
		if (!m_pMonster->getAnimationRun())
		{
			auto a_iChoice = rand() % 3;
	
			if (a_iChoice == 0)
			{
				m_pMonster->setState(Monster::EState::FIRE_BALL);
			}
			else
			{
				m_pMonster->setState(Monster::EState::RUN_BITE);
			}
		}
	
		if (IntersectPMObjectBox(m_pPHitMesh, m_pMHeadMesh) &&
			!m_pPlayer->getStar() && m_pPlayer->getState() != Player::EPlayerState::BLOCK)
		{
			m_pPParticle->setPosition(m_pPHitMesh->getPosition());
			m_pPParticle->startParticleEmit(0.2f);
			m_pPlayer->setState(Player::EPlayerState::SHIT);
			m_pPlayer->setHp(m_pPlayer->getHp() - m_pMonster->getDmg());
		}
	}
	else
	{

		if (m_pMonster->getTurnTarget() == NULL)
			m_pMonster->setPlayerPosition(m_pPlayer);
		D3DXVECTOR3 v1 = m_pPlayer->m_pBaseMesh->getPosition() - m_pMonster->getMonsterPosition();
		D3DXVECTOR3 v2 = m_pMonster->getMonsterRightDirection();
		D3DXVec3Normalize(&v1, &v1);

		D3DXVECTOR3 v3;
		D3DXVec3Cross(&v3, &v1, &v2);

		if (v3.y < -0.01f)
		{
			if (!m_pMonster->getAnimationRun())
				m_pMonster->setState(Monster::EState::RIGHT_TURN);
		}
		else if (v3.y > 0.01f)
		{
			if (!m_pMonster->getAnimationRun())
				m_pMonster->setState(Monster::EState::LEFT_TURN);
		}
		else
		{
			if (!m_pMonster->getAnimationRun())
			{
				auto length = D3DXVec3Length(&(m_pMonster->getMonsterPosition() - m_pPlayer->m_pBaseMesh->getPosition()));
				if (length > 20.0f)
					m_pMonster->setState(Monster::EState::WALK);
				else
					m_pMonster->setState(Monster::EState::BITE);
			}
		}
	}
	if(m_pMonster->getState() == Monster::EState::FIRE_BALL)
	{
		if(m_pMonster->getBallStaticMesh() != NULL)
		{
			if(IntersectPMObjectBox(m_pPHitMesh, m_pMonster->getBallStaticMesh()) && !m_pPlayer->getStar() && m_pPlayer->getState() != Player::EPlayerState::BLOCK)
			{
				m_pPParticle->setPosition(m_pPHitMesh->getPosition());
				m_pPParticle->startParticleEmit(0.2f);
				m_pPlayer->setState(Player::EPlayerState::LHIT);
				m_pPlayer->setHp(m_pPlayer->getHp() - m_pMonster->getDmg());
			}

			if(IntersectPMObjectBox(m_pPHitMesh, m_pMonster->getBallStaticMesh()) && m_pPlayer->getState() == Player::EPlayerState::BLOCK)
			{
				m_pPlayer->setState(Player::EPlayerState::SHHIT);
			}
		}
	}
}

void CBattleManager::player_MonsterAttack()
{
	if (m_pPlayer->getState() == Player::EPlayerState::ATTACK)
	{
		auto pAType = m_pPlayer->getAttack();
		if (m_ePrevAtkType != pAType)
		{
			if (this->IntersectPMObjectBox(m_pPAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMObjectBox(m_pPAtkMesh, m_pMBodyMesh) ||
				this->IntersectPMObjectBox(m_pPAtkMesh, m_pMTailMesh) ||
				this->IntersectPMObjectBox(m_pPSAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMObjectBox(m_pPSAtkMesh, m_pMHeadMesh) ||
				this->IntersectPMObjectBox(m_pPSAtkMesh, m_pMHeadMesh))
			{
				m_pPParticle->setPosition(m_pPAtkMesh->getPosition() / 2);
				m_pPParticle->startParticleEmit(0.2f);
				m_ePrevAtkType = pAType;
				//printf("%.2f\n", m_pMonster->getHP());
				//1.2에 플레이어 공격력
				m_pMonster->setHP(m_pMonster->getHP() - m_pPlayer->getDamage());
				if (this->IntersectPMObjectBox(m_pPAtkMesh, m_pMTailMesh))
				{
					m_pMonster->setTailHP(m_pMonster->getTailHP() - m_pPlayer->getDamage());
				}
			}
		}
	}
	else
	{
		m_ePrevAtkType = Player::EAttackType::NONE;
	}
}

void CBattleManager::pmBoudningBoxCollision()
{
	//auto stCurrPos = m_pPlayer->getPosition();
	if(this->IntersectPMObjectBox(m_pPHitMesh, m_pMBodyMesh) || this->IntersectPMObjectBox(m_pPHitMesh, m_pMTailMesh) || this->IntersectPMObjectBox(m_pPHitMesh, m_pMHeadMesh))
	{
		m_bIsCollision = true;
		m_pPlayer->setMoveSpd(0.0f);
		m_pPlayer->setJumpSpd(0.0f);
		//auto gapPos = m_stPrevPos - stCurrPos;
		if (m_oPrevPos.size() != 0)
		{
			m_pPlayer->setPosition(m_oPrevPos[m_oPrevPos.size() - 1]);
			m_pPlayer->setPlayerControl(Player::EPlayerControl::FORCE);
			m_oPrevPos.pop_back();
		}
	}
	else
	{
		m_bIsCollision = false;
		if(m_pPlayer->getState() == Player::EPlayerState::MOVE || m_pPlayer->getState() == Player::EPlayerState::ATTACK)
		{
			m_oPrevPos.push_back(m_pPlayer->getPosition());
			if(m_oPrevPos.size() == 501)
			{
				m_oPrevPos.erase(m_oPrevPos.begin());		// 추후 최적화 해야할것
			}
		}
		m_pPlayer->setPlayerControl(Player::EPlayerControl::NONE);
		m_pPlayer->setMoveSpd(10.0f);
		m_pPlayer->setJumpSpd(10.0f);
	}
}

void CBattleManager::stun()
{
	m_bIsFlashBomb = true;
}
