#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../../../Player/Player.h"

class Player;
class Monster;
class CStaticMesh;
class CParticleSystem;

class CBattleManager
{
public:

	virtual void init(Player* a_pMesh1, Monster* a_pMesh2);
	virtual void update();
	virtual void render();

public:
	bool getCollision();

private:
	void createMesh();
	void meshUpdate();
	void updatePosition();
	bool IntersectPMObjectBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh);
	bool IntersectPMBoundingBox(CStaticMesh* a_pPMesh, CStaticMesh* a_pMMesh);

	void meshRender();

	void setDebugMode();
	void monster_PlayerAttack();
	void player_MonsterAttack();
	void pmBoudningBoxCollision();

	void stun();
	
private:

	Player* m_pPlayer;
	Monster* m_pMonster;

	bool m_bIsDebug = false;
	bool m_bIsCollision = false;
	bool m_bIsFlashBomb = false;
	
	float ftime = 0.0f;
	
	CStaticMesh * m_pPAtkMesh = nullptr;
	CStaticMesh * m_pPSAtkMesh = nullptr;
	CStaticMesh * m_pPHitMesh = nullptr;

	CStaticMesh * m_pMHeadMesh = nullptr;
	CStaticMesh * m_pMTailMesh = nullptr;
	CStaticMesh * m_pMBodyMesh = nullptr;
	CStaticMesh * m_pMForwardMesh = nullptr;
	CStaticMesh * m_pMBackMesh = nullptr;
	CStaticMesh* m_pMRemoteMesh = nullptr;

	Player::EAttackType m_ePrevAtkType = Player::EAttackType::NONE;
	D3DXVECTOR3 m_stPrevPos;
	std::vector<D3DXVECTOR3> m_oPrevPos;
	CParticleSystem* m_pPParticle;
	CParticleSystem* m_pMParticle;
};