#include "CDeviceManager.h"
#include "CWindowManager.h"

CDeviceManager::CDeviceManager(void)
{
	ZeroMemory(&m_stBufferDesc, sizeof(m_stBufferDesc));
	ZeroMemory(&m_stSampleDesc, sizeof(m_stSampleDesc));
}

CDeviceManager::~CDeviceManager(void)
{
	SAFE_RELEASE(m_p9VersionDevice);
	SAFE_RELEASE(m_pDirect3D);

	SAFE_RELEASE(m_pRenderTargetView);
	SAFE_RELEASE(m_pDepthStencilView);

	SAFE_RELEASE(m_pDevice);
	SAFE_RELEASE(m_pContext);

	SAFE_RELEASE(m_pSwapChain);
	SAFE_RELEASE(m_pFactory);
}

void CDeviceManager::init(void)
{
	m_pDirect3D = this->createDirect3D();
	m_p9VersionDevice = this->create9VersionDevice();

	m_pFactory = this->createFactory();
	m_pDevice = this->createDevice(&m_pContext);
	m_pSwapChain = this->createSwapChain();

	this->resizeSwapChainBuffer();
}

LPDIRECT3D9 CDeviceManager::getDirect3D(void)
{
	return m_pDirect3D;
}

LPDIRECT3DDEVICE9 CDeviceManager::get9VersionDevice(void)
{
	return m_p9VersionDevice;
}

IDXGIFactory * CDeviceManager::getFactory(void)
{
	return m_pFactory;
}

IDXGISwapChain * CDeviceManager::getSwapChain(void)
{
	return m_pSwapChain;
}

ID3D11Device * CDeviceManager::getDevice(void)
{
	return m_pDevice;
}

ID3D11DeviceContext * CDeviceManager::getContext(void)
{
	return m_pContext;
}

ID3D11RenderTargetView * CDeviceManager::getRenderTargetView(void)
{
	return m_pRenderTargetView;
}

ID3D11DepthStencilView * CDeviceManager::getDepthStencilView(void)
{
	return m_pDepthStencilView;
}

void CDeviceManager::resizeSwapChainBuffer(void)
{
	SAFE_RELEASE(m_pRenderTargetView);
	SAFE_RELEASE(m_pDepthStencilView);

	m_stBufferDesc.Width = GET_WINDOW_SIZE().cx;
	m_stBufferDesc.Height = GET_WINDOW_SIZE().cy;

	m_stBufferDesc.RefreshRate.Numerator = 0;
	m_stBufferDesc.RefreshRate.Denominator = 1;

	// 갱신 주기를 설정한다
	// {
	IDXGIAdapter *pAdapter = nullptr;
	m_pFactory->EnumAdapters(0, &pAdapter);

	IDXGIOutput *pOutput = nullptr;
	pAdapter->EnumOutputs(0, &pOutput);

	UINT nNumModes = 0;

	/*
	GetDisplayModeList 함수는 모니터의 해상도 정보 리스트를 반환하는
	역할을 한다.

	해당 함수는 2 가지 방식으로 동작하는데 마지막 매개 변수인 버퍼의
	포인터를 NULL 설정하면 현재 모니터가 지원하는 해상도의 개수를
	조회 할 수 있으며, 해당 정보를 기반으로 버퍼를 생성 후 전달하면
	모니터의 해상도 정보를 가져 올 수 있다.
	*/
	pOutput->GetDisplayModeList(m_stBufferDesc.Format,
		DXGI_ENUM_MODES_INTERLACED, &nNumModes, NULL);

	auto pstModeDescs = (DXGI_MODE_DESC *)malloc(sizeof(DXGI_MODE_DESC) * nNumModes);

	pOutput->GetDisplayModeList(m_stBufferDesc.Format,
		DXGI_ENUM_MODES_INTERLACED, &nNumModes, pstModeDescs);

	for (int i = 0; i < nNumModes; ++i) {
		if (pstModeDescs[i].Width == GET_WINDOW_SIZE().cx &&
			pstModeDescs[i].Height == GET_WINDOW_SIZE().cy)
		{
			m_stBufferDesc.RefreshRate.Numerator = pstModeDescs[i].RefreshRate.Numerator;
			m_stBufferDesc.RefreshRate.Denominator = pstModeDescs[i].RefreshRate.Denominator;
		}
	}

	SAFE_FREE(pstModeDescs);

	SAFE_RELEASE(pOutput);
	SAFE_RELEASE(pAdapter);
	// }

	/*
	일반적으로 대부분의 DirectX 관련 함수는 결과 값으로 에러 코드에
	해당하는 HRESULT 형 데이터를 반환한다.

	이때, 해당 결과가 올바른 것인지 유무를 검사하기 위해서 DirectX 는
	2 가지의 매크로 함수를 제공한다. (SUCCEEDED, FAILED)

	ResizeTarget 함수는 물체를 그릴 윈도우 크기와 갱신 주기를 변경하는
	역할을 하며, ResizeBuffers 함수는 스왑 체인이 지니고 있는 백 버퍼의
	크기를 변경하는 역할을 한다.
	*/
	// 스완 체인 버퍼 크기를 변경한다
	if (SUCCEEDED(m_pSwapChain->ResizeTarget(&m_stBufferDesc))) {
		if (SUCCEEDED(m_pSwapChain->ResizeBuffers(1,
			m_stBufferDesc.Width,
			m_stBufferDesc.Height,
			m_stBufferDesc.Format,
			DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH)))
		{
			// 뷰포트를 설정한다
			// {
			D3D11_VIEWPORT stViewport;
			ZeroMemory(&stViewport, sizeof(stViewport));

			stViewport.TopLeftX = 0.0f;
			stViewport.TopLeftY = 0.0f;
			stViewport.Width = GET_WINDOW_SIZE().cx;
			stViewport.Height = GET_WINDOW_SIZE().cy;
			stViewport.MinDepth = 0.0f;
			stViewport.MaxDepth = 1.0f;

			m_pContext->RSSetViewports(1, &stViewport);
			// }

			// 렌더 타겟 뷰, 깊이 스텐실 뷰를 설정한다
			// {
			m_pRenderTargetView = this->createRenderTargetView();
			m_pDepthStencilView = this->createDepthStencilView();

			m_pContext->OMSetRenderTargets(1,
				&m_pRenderTargetView, m_pDepthStencilView);
			// }
		}
	}
}

LPDIRECT3D9 CDeviceManager::createDirect3D(void)
{
	return Direct3DCreate9(D3D_SDK_VERSION);
}

LPDIRECT3DDEVICE9 CDeviceManager::create9VersionDevice(void)
{
	D3DCAPS9 stDeviceCaps;
	ZeroMemory(&stDeviceCaps, sizeof(stDeviceCaps));

	UINT nVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	m_pDirect3D->GetDeviceCaps(0, D3DDEVTYPE_NULLREF, &stDeviceCaps);

	// 하드웨어 연산을 지원 할 경우
	if (stDeviceCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
		nVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	}

	D3DPRESENT_PARAMETERS stParameters;
	ZeroMemory(&stParameters, sizeof(stParameters));

	stParameters.BackBufferCount = 1;
	stParameters.BackBufferWidth = GET_WINDOW_SIZE().cx;
	stParameters.BackBufferHeight = GET_WINDOW_SIZE().cy;
	stParameters.BackBufferFormat = D3DFMT_A8R8G8B8;
	stParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	stParameters.EnableAutoDepthStencil = TRUE;
	stParameters.AutoDepthStencilFormat = D3DFMT_D24S8;
	stParameters.Windowed = TRUE;
	stParameters.hDeviceWindow = GET_WINDOW_HANDLE();
	stParameters.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	stParameters.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	stParameters.MultiSampleType = D3DMULTISAMPLE_NONE;
	stParameters.MultiSampleQuality = 0;
	stParameters.Flags = 0;

	// 디바이스를 생성한다
	// {
	LPDIRECT3DDEVICE9 p9VersionDevice = nullptr;

	m_pDirect3D->CreateDevice(0,
		D3DDEVTYPE_NULLREF,
		GET_WINDOW_HANDLE(),
		nVertexProcessing,
		&stParameters,
		&p9VersionDevice);
	// }

	
	return p9VersionDevice;
}

IDXGIFactory * CDeviceManager::createFactory(void)
{
	/*
	IDXGIFactory 객체는 다른 DXGI 관련 객체를 생성하기 위한 역할을
	한다.
	
	__uuidof 키워드는 COM (Component Object Modeling) 객체의 인터페이스의 
	UUID (Universal Unique Identifier) 를 가져오는 역할을 한다.
	*/
	// 팩토리를 생성한다
	IDXGIFactory *pFactory = nullptr;
	CreateDXGIFactory(__uuidof(IDXGIFactory), (void **)&pFactory);
	
	return pFactory;
}

IDXGISwapChain * CDeviceManager::createSwapChain(void)
{
	// 스왑 체인을 생성한다
	// {
	DXGI_SWAP_CHAIN_DESC stSwapChainDesc;
	ZeroMemory(&stSwapChainDesc, sizeof(stSwapChainDesc));

	/*
	버퍼 설정 옵션
	:
	- BufferCount (백 버퍼의 개수)
	- BufferUsage (백 버퍼의 사용 용도)
	- BufferDesc.Width (백 버퍼의 너비)
	- BufferDesc.Height (백 버퍼의 높이)
	- BufferDesc.Format (백 버퍼의 데이터 포맷 == 픽셀 포맷)
	- BufferDesc.Scaling (전체 화면 전환 시 해상도 비율 조절)
	- BufferDesc.ScanlineOrdering (래스터라이저 연산 시 픽셀 결정 알고리즘 지정)
	- BufferDesc.RefreshRate.Numerator (화면 갱신 주기에 사용되는 분자)
	- BufferDesc.RefreshRate.Denominator (화면 갱신 주기에 사용되는 분모)
	*/
	// 버퍼 설정
	stSwapChainDesc.BufferCount = 1;
	stSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	stSwapChainDesc.BufferDesc.Width = GET_WINDOW_SIZE().cx;
	stSwapChainDesc.BufferDesc.Height = GET_WINDOW_SIZE().cy;
	stSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	stSwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	stSwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	stSwapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	stSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	/*
	샘플링 설정 옵션
	:
	- SampleDesc.Count (특정 픽셀을 그리기 위해서 필요한 샘플링 횟수)
	- SampleDesc.Qaulity (멀티 샘플링 품질 설정)
	*/
	// 샘플링 설정
	stSwapChainDesc.SampleDesc.Count = 1;
	stSwapChainDesc.SampleDesc.Quality = 0;

	/*
	기타 설정 옵션
	:
	- Windowed (창 모드 실행 여부)
	- OutputWindow (그림을 그릴 윈도우)
	- SwapEffect (페이지 플리핑 효과)
	- Flags (기타 옵션 지정 - 일반적으로 DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH 설정 지정 -)
	*/
	// 기타 설정
	stSwapChainDesc.Windowed = TRUE;
	stSwapChainDesc.OutputWindow = GET_WINDOW_HANDLE();
	stSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	stSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain *pSwapChain = nullptr;
	m_pFactory->CreateSwapChain(m_pDevice, &stSwapChainDesc, &pSwapChain);
	// }

	CopyMemory(&m_stBufferDesc, 
		&stSwapChainDesc.BufferDesc, sizeof(stSwapChainDesc.BufferDesc));

	CopyMemory(&m_stSampleDesc,
		&stSwapChainDesc.SampleDesc, sizeof(stSwapChainDesc.SampleDesc));

	return pSwapChain;
}

ID3D11Device * CDeviceManager::createDevice(ID3D11DeviceContext ** a_pOutContext)
{
	// 디바이스를 생성한다
	// {
	D3D_FEATURE_LEVEL aeFeatureLevels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	ID3D11Device *pDevice = nullptr;

	/*
	기존의 다이렉트 3D 는 디바이스 객체를 통해서 리소스 생성과 물체를
	렌더링하는 작업을 처리했지만 11 버전에서 리소스를 생성하는 역할은
	디바이스, 물체를 렌더링하는 작업은 컨텍스트가 담당한다.
	*/
	D3D11CreateDevice(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		0,
		aeFeatureLevels,
		sizeof(aeFeatureLevels) / sizeof(aeFeatureLevels[0]),
		D3D11_SDK_VERSION,
		&pDevice,
		NULL,
		a_pOutContext);
	// }


	return pDevice;
}

ID3D11RenderTargetView * CDeviceManager::createRenderTargetView(void)
{
	// 렌더 타겟 뷰를 생성한다
	// {
	/*
	다이렉트 3D 11 버전은 ID3D11Buffer 객체를 통해서 1 차원 버퍼,
	ID3D11Texture2D 객체를 통해서 2 차원 버퍼를 제어하는 것이
	가능하다.
	*/
	ID3D11Texture2D *pBackBuffer = nullptr;
	m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&pBackBuffer);

	ID3D11RenderTargetView *pRenderTargetView = nullptr;

	/*
	ID3D11RenderTargetView 객체는 물체를 그리기 위한 렌더 타겟 버퍼를
	가리키는 역할을 한다.
	*/
	m_pDevice->CreateRenderTargetView(pBackBuffer, 
		NULL,
		&pRenderTargetView);
	// }

	SAFE_RELEASE(pBackBuffer);
	return pRenderTargetView;
}

ID3D11DepthStencilView * CDeviceManager::createDepthStencilView(void)
{
	/*
	ID3D11DepthStencilView 객체는 화면 픽셀의 깊이 값과 스텐실 값을 보관하는
	버퍼를 가리키는 역할을 한다.

	깊이 값은 물체 렌더링 될 때 특정 물체의 값과 비교를 물체의 앞뒤를
	판정하는데 사용되며, 스텐실 값은 스텐실 연산을 통해서 해당 위치에
	그림을 그릴지 여부를 판정하는데 사용된다.
	*/
	// 깊이 스텐실 뷰를 생성한다
	// {
	// 깊이 스텐실 버퍼 설정
	D3D11_TEXTURE2D_DESC stDepthStencilBufferDesc;
	ZeroMemory(&stDepthStencilBufferDesc, sizeof(stDepthStencilBufferDesc));

	stDepthStencilBufferDesc.ArraySize = 1;
	stDepthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	stDepthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	stDepthStencilBufferDesc.Width = GET_WINDOW_SIZE().cx;
	stDepthStencilBufferDesc.Height = GET_WINDOW_SIZE().cy;
	stDepthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	stDepthStencilBufferDesc.CPUAccessFlags = 0;
	stDepthStencilBufferDesc.MipLevels = 1;
	stDepthStencilBufferDesc.MiscFlags = 0;
	stDepthStencilBufferDesc.SampleDesc.Count = 1;
	stDepthStencilBufferDesc.SampleDesc.Quality = 0;

	ID3D11Texture2D *pDepthStencilBuffer = nullptr;
	
	m_pDevice->CreateTexture2D(&stDepthStencilBufferDesc, 
		NULL, 
		&pDepthStencilBuffer);

	// 깊이 스텐실 뷰 설정
	D3D11_DEPTH_STENCIL_VIEW_DESC stDepthStencilViewDesc;
	ZeroMemory(&stDepthStencilViewDesc, sizeof(stDepthStencilViewDesc));

	stDepthStencilViewDesc.Format = stDepthStencilBufferDesc.Format;
	stDepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	stDepthStencilViewDesc.Texture2D.MipSlice = 0;
	stDepthStencilViewDesc.Flags = 0;

	ID3D11DepthStencilView *pDepthStencilView = nullptr;

	m_pDevice->CreateDepthStencilView(pDepthStencilBuffer,
		&stDepthStencilViewDesc,
		&pDepthStencilView);
	// }

	SAFE_RELEASE(pDepthStencilBuffer);
	return pDepthStencilView;
}
