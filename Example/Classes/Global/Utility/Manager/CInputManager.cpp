#include "CInputManager.h"
#include "CWindowManager.h"

CInputManager::CInputManager(void)
{
	ZeroMemory(m_anKeyboardState, sizeof(m_anKeyboardState));
	ZeroMemory(m_anPrevKeyboardState, sizeof(m_anPrevKeyboardState));

	ZeroMemory(&m_stMouseState, sizeof(m_stMouseState));
	ZeroMemory(&m_stPrevMouseState, sizeof(m_stPrevMouseState));
}

CInputManager::~CInputManager(void)
{
	/*
	다이렉트 인풋의 디바이스 객체는 각 장치를 제어하기 위한 권한을 획득한 후
	장치 사용을 완료했으면 해당 장치에 대한 권한을 반납해야한다.

	만약, 장치의 권한을 반납하지 않은 상태에서 프로그램이 종료 될 경우
	다른 프로그램이 해당 장치를 사용 할 수 없는 상황이 발생 할 수 있다.
	*/
	SAFE_UNACQUIRE(m_pKeyboardDevice);
	SAFE_UNACQUIRE(m_pMouseDevice);

	/*
	DirectX 프레임워크에 속해있는 객체들은 대부분은 COM 객체이기 때문에
	해당 객체를 제거하기 위해서는 명시적으로 delete 를 호출하는 것이 아니라
	Release 함수를 통해서 레퍼런스 카운트를 감소시켜야한다.

	(즉, COM 객체는 레퍼런스 카운팅 방식으로 메모리를 관리하며
	내부적으로 레퍼런스 카운트가 0 이 되면 스스로 메모리를 제거한다.)
	*/
	SAFE_RELEASE(m_pDirectInput);
}

void CInputManager::init(void)
{
	m_pDirectInput = this->createDirectInput();
	m_pKeyboardDevice = this->createKeyboardDevice();
	m_pMouseDevice = this->createMouseDevice();
}

void CInputManager::update(void)
{
	CopyMemory(m_anPrevKeyboardState, m_anKeyboardState, sizeof(m_anKeyboardState));
	CopyMemory(&m_stPrevMouseState, &m_stMouseState, sizeof(m_stMouseState));

	/*
	GetDeviceState 함수는 특정 장치의 현재 상태를 가져오는 역할을 한다.
	(즉, 입력 데이터를 폴링한다.)
	*/
	m_pKeyboardDevice->GetDeviceState(sizeof(m_anKeyboardState), m_anKeyboardState);
	m_pMouseDevice->GetDeviceState(sizeof(m_stMouseState), &m_stMouseState);
}

float CInputManager::getMouseWheel(void)
{
	return (float)m_stMouseState.lZ;
}

POINT CInputManager::getMousePosition(void)
{
	POINT stMousePosition;
	ZeroMemory(&stMousePosition, sizeof(stMousePosition));

	/*
	ScreenToClient 함수는 스크린 위치를 기준으로 설정되어있는 위치를
	특정 윈도우의 클라이언트 영역을 기준으로 한 위치로 변경하는 역할을
	한다.
	*/
	GetCursorPos(&stMousePosition);
	ScreenToClient(GET_WINDOW_HANDLE(), &stMousePosition);

	return stMousePosition;
}

LPDIRECTINPUT8 CInputManager::getDirectInput(void)
{
	return m_pDirectInput;
}

LPDIRECTINPUTDEVICE8 CInputManager::getKeyboardDevice(void)
{
	return m_pKeyboardDevice;
}

LPDIRECTINPUTDEVICE8 CInputManager::getMouseDevice(void)
{
	return m_pMouseDevice;
}

bool CInputManager::isKeyDown(BYTE a_nKeyCode)
{
	return m_anKeyboardState[a_nKeyCode] & 0x80;
}

bool CInputManager::isKeyPressed(BYTE a_nKeyCode)
{
	return (m_anKeyboardState[a_nKeyCode] & 0x80) &&
		!(m_anPrevKeyboardState[a_nKeyCode] & 0x80);
}

bool CInputManager::isKeyReleased(BYTE a_nKeyCode)
{
	return !(m_anKeyboardState[a_nKeyCode] & 0x80) &&
		(m_anPrevKeyboardState[a_nKeyCode] & 0x80);
}

bool CInputManager::isMouseButtonDown(EMouseButton a_eMouseButton)
{
	return m_stMouseState.rgbButtons[(int)a_eMouseButton] & 0x80;
}

bool CInputManager::isMouseButtonPressed(EMouseButton a_eMouseButton)
{
	return (m_stMouseState.rgbButtons[(int)a_eMouseButton] & 0x80) &&
		!(m_stPrevMouseState.rgbButtons[(int)a_eMouseButton] & 0x80);
}

bool CInputManager::isMouseButtonReleased(EMouseButton a_eMouseButton)
{
	return !(m_stMouseState.rgbButtons[(int)a_eMouseButton] & 0x80) &&
		(m_stPrevMouseState.rgbButtons[(int)a_eMouseButton] & 0x80);
}

LPDIRECTINPUT8 CInputManager::createDirectInput(void)
{
	// 다이렉트 인풋을 생성한다
	// {
	LPDIRECTINPUT8 pDirectInput = nullptr;

	DirectInput8Create(GET_INSTANCE_HANDLE(),
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void **)&pDirectInput,
		NULL);
	// }

	return pDirectInput;
}

LPDIRECTINPUTDEVICE8 CInputManager::createKeyboardDevice(void)
{
	// 키보드 디바이스를 생성한다
	// {
	LPDIRECTINPUTDEVICE8 pKeyboardDevice = nullptr;

	m_pDirectInput->CreateDevice(GUID_SysKeyboard,
		&pKeyboardDevice,
		NULL);
	// }

	pKeyboardDevice->SetDataFormat(&c_dfDIKeyboard);
	pKeyboardDevice->SetCooperativeLevel(GET_WINDOW_HANDLE(), DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
	pKeyboardDevice->Acquire();

	return pKeyboardDevice;
}

LPDIRECTINPUTDEVICE8 CInputManager::createMouseDevice(void)
{
	// 마우스 디바이스를 생성한다
	// {
	LPDIRECTINPUTDEVICE8 pMouseDevice = nullptr;

	m_pDirectInput->CreateDevice(GUID_SysMouse,
		&pMouseDevice,
		NULL);
	// }

	pMouseDevice->SetDataFormat(&c_dfDIMouse);
	pMouseDevice->SetCooperativeLevel(GET_WINDOW_HANDLE(), DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
	pMouseDevice->Acquire();

	return pMouseDevice;
}
