#pragma once

#include "../../Define/KGlobalDefine.h"

class CMesh;
class CShader;

//! 리소스 관리자
class CResourceManager
{
public:			// getter

	//! 재질을 반환한다
	STMaterial getMaterial(const std::string &a_rKey, bool a_bIsAutoCreate = true);

	//! 웨이브 사운드를 반환한다
	STWaveSound getWaveSound(const std::string &a_rKey, bool a_bIsAutoCreate = true);

	//! 메시를 반환한다
	CMesh * getMesh(const std::string &a_rKey);

	//! 쉐이더를 반환한다
	CShader * getShader(const std::string &a_rKey);

	//! 버퍼를 반환한다
	ID3D11Buffer * getBuffer(const std::string &a_rKey);

	//! 입력 레이아웃을 반환한다
	ID3D11InputLayout * getInputLayout(const std::string &a_rKey);

	//! 래스터라이저 상태를 반환한다
	ID3D11RasterizerState * getRasterizerState(const std::string &a_rKey);

	//! 블렌드 상태를 반환한다
	ID3D11BlendState * getBlendState(const std::string &a_rKey);

	//! 쉐이더 리소스 뷰를 반환한다
	ID3D11ShaderResourceView * getShaderResourceView(const std::string &a_rKey,
		bool a_bIsAutoCreate = true);

public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CResourceManager);

	//! 초기화
	virtual void init(void);

	//! 재질을 추가한다
	void addMaterial(const std::string &a_rKey,
		const STMaterial &a_rstMaterial);

	//! 웨이브 사운드를 추가한다
	void addWaveSound(const std::string &a_rKey,
		const STWaveSound &a_rstWaveSound);

	//! 메시를 추가한다
	void addMesh(const std::string &a_rKey, CMesh *a_pMesh);

	//! 쉐이더를 추가한다
	void addShader(const std::string &a_rKey, CShader *a_pShader);

	//! 버퍼를 추가한다
	void addBuffer(const std::string &a_rKey,
		ID3D11Buffer *a_pBuffer);

	//! 입력 레이아웃을 추가한다
	void addInputLayout(const std::string &a_rKey,
		ID3D11InputLayout *a_pInputLayout);

	//! 래스터라이저 상태를 추가한다
	void addRasterizerState(const std::string &a_rKey,
		ID3D11RasterizerState *a_pRasterizerStae);

	//! 블렌드 상태를 추가한다
	void addBlendState(const std::string &a_rKey,
		ID3D11BlendState *a_pBlendState);

	//! 쉐이더 리소스 뷰를 추가한다
	void addShaderResourceView(const std::string &a_rKey,
		ID3D11ShaderResourceView *a_pShaderResourceView);
	
private:			// private 변수

	std::unordered_map<std::string, STMaterial> m_oMaterialList;
	std::unordered_map<std::string, STWaveSound> m_oWaveSoundList;

	std::unordered_map<std::string, CMesh *> m_oMeshList;
	std::unordered_map<std::string, CShader *> m_oShaderList;
	std::unordered_map<std::string, ID3D11Buffer *> m_oBufferList;
	std::unordered_map<std::string, ID3D11InputLayout *> m_oInputLayoutList;
	std::unordered_map<std::string, ID3D11RasterizerState *> m_oRasterizerStateList;
	std::unordered_map<std::string, ID3D11BlendState *> m_oBlendStateList;
	std::unordered_map<std::string, ID3D11ShaderResourceView *> m_oShaderResourceViewList;
};
