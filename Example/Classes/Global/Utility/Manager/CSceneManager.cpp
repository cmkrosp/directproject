#include "CSceneManager.h"
#include "../../../Scene/CTestScene.h"

CSceneManager::CSceneManager(void)
{
}

CSceneManager::~CSceneManager(void)
{
}

void CSceneManager::init(HINSTANCE a_hInstance, int a_nShowOptions, const SIZE& a_rstWindowSize)
{
	m_oSceneList.clear();

	CTestScene oTestScene(a_hInstance, a_nShowOptions, a_rstWindowSize);

	this->addScene(CSceneManager::ESCENETYPE::TAG_TEST_SCENE, &oTestScene);

	this->changeScene(CSceneManager::ESCENETYPE::TAG_TEST_SCENE);
}

void CSceneManager::addScene(ESCENETYPE a_eKey, CDirect3DApplication* a_pScene)
{
	m_oSceneList.insert(decltype(m_oSceneList)::value_type(a_eKey, a_pScene));
}

CDirect3DApplication* CSceneManager::getCurrentScene(void)
{
	auto oIterator = m_oSceneList.find(m_eCurrentScene);
	return (oIterator == m_oSceneList.end()) ? nullptr : oIterator->second;
}

void CSceneManager::changeScene(ESCENETYPE a_eSceneType)
{
	m_eCurrentScene = a_eSceneType;

	this->getCurrentScene()->run();
}
