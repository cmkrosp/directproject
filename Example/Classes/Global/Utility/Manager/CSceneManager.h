#pragma once

#include "../../Define/KGlobalDefine.h"

class CDirect3DApplication;

class CSceneManager
{
public:

	enum class ESCENETYPE
	{
		TAG_NONE = 0,
		TAG_TEST_SCENE,
		TAG_TITLE_SCENE,
		TAG_LOADING_SCENE,
		TAG_BATTLE_SCENE,
		TAG_ENDING_SCENE,
	};

public:
	DECLARE_SINGLETON(CSceneManager);

	virtual void init(HINSTANCE a_hInstance,
		int a_nShowOptions, const SIZE& a_rstWindowSize);

	void addScene(ESCENETYPE a_eKey, CDirect3DApplication* a_pScene);

	CDirect3DApplication* getCurrentScene(void);

	void changeScene(ESCENETYPE a_eSceneType);

private:

	ESCENETYPE m_eCurrentScene = ESCENETYPE::TAG_NONE;

	std::unordered_map<ESCENETYPE, CDirect3DApplication*> m_oSceneList;
};