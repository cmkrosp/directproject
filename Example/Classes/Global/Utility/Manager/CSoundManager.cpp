#include "CSoundManager.h"
#include "../../Function/GlobalFunction.h"
#include "CWindowManager.h"
#include "../Sound/CSound.h"

CSoundManager::CSoundManager(void)
{
	// Do Nothing
}

CSoundManager::~CSoundManager(void)
{
	SAFE_DELETE(m_pBackgroundSound);

	this->enumerateEffectSoundListContainer([=](CSound *a_pSound) -> void {
		SAFE_DELETE(a_pSound);
	});
}

void CSoundManager::init(void)
{
	m_pDirectSound = this->createDirectSound();
	m_pPrimaryBuffer = this->createPrimaryBuffer();
}

void CSoundManager::playEffectSound(const std::string & a_rFilepath, bool a_bIsLoop)
{
	auto pEffectSound = this->findPlayableEffectSound(a_rFilepath);

	if (pEffectSound != nullptr) {
		pEffectSound->playSound(a_bIsLoop);
		this->setEffectSoundsVolume(m_fEffectSoundsVolume);
	}
}

void CSoundManager::playBackgroundSound(const std::string & a_rFilepath, bool a_bIsLoop)
{
	if (m_pBackgroundSound == nullptr) {
		m_pBackgroundSound = Create<CSound>(a_rFilepath);
	}

	m_pBackgroundSound->resetSound(a_rFilepath);
	m_pBackgroundSound->playSound(a_bIsLoop);

	this->setBackgroundSoundVolume(m_fBackgroundSoundVolume);
}

LPDIRECTSOUND CSoundManager::getDirectSound(void)
{
	return m_pDirectSound;
}

LPDIRECTSOUNDBUFFER CSoundManager::getPrimaryBuffer(void)
{
	return m_pPrimaryBuffer;
}

void CSoundManager::setEffectSoundsVolume(float a_fVolume)
{
	m_fEffectSoundsVolume = ClampValue<float>(a_fVolume, 0.0f, 1.0f);

	this->enumerateEffectSoundListContainer([=](CSound *a_pSound) -> void {
		if (a_pSound != nullptr) {
			a_pSound->setVolume(m_fEffectSoundsVolume);
		}
	});
}

void CSoundManager::setBackgroundSoundVolume(float a_fVolume)
{
	m_fBackgroundSoundVolume = ClampValue<float>(a_fVolume, 0.0f, 1.0f);

	if (m_pBackgroundSound != nullptr) {
		m_pBackgroundSound->setVolume(m_fBackgroundSoundVolume);
	}
}

void CSoundManager::enumerateEffectSoundListContainer(const std::function<void(CSound*)>& a_rCallback)
{
	for (auto &rValueType : m_oEffectSoundListContainer) {
		for (auto pSound : rValueType.second) {
			a_rCallback(pSound);
		}
	}
}

CSound * CSoundManager::findPlayableEffectSound(const std::string & a_rFilepath)
{
	// 효과음 리스트가 없을 경우
	if (m_oEffectSoundListContainer.find(a_rFilepath) == 
		m_oEffectSoundListContainer.end())
	{
		TEffectSoundList oEffectSoundList{
			nullptr
		};

		m_oEffectSoundListContainer.insert(decltype(m_oEffectSoundListContainer)::value_type(a_rFilepath, oEffectSoundList));
	}

	auto oIterator = m_oEffectSoundListContainer.find(a_rFilepath);

	// 재생 가능한 효과음을 탐색한다
	for (int i = 0; i < oIterator->second.size(); ++i) {
		if (oIterator->second[i] == nullptr ||
			!oIterator->second[i]->isPlaying())
		{
			if (oIterator->second[i] == nullptr) {
				oIterator->second[i] = Create<CSound>(a_rFilepath);
			}

			return oIterator->second[i];
		}
	}

	return nullptr;
}

LPDIRECTSOUND CSoundManager::createDirectSound(void)
{
	// 다이렉트 사운드를 생성한다
	LPDIRECTSOUND pDirectSound = nullptr;
	DirectSoundCreate(NULL, &pDirectSound, NULL);

	pDirectSound->SetCooperativeLevel(GET_WINDOW_HANDLE(), DSSCL_PRIORITY);
	return pDirectSound;
}

LPDIRECTSOUNDBUFFER CSoundManager::createPrimaryBuffer(void)
{
	// 주 버퍼를 생성한다
	// {
	DSBUFFERDESC stBufferDesc;
	ZeroMemory(&stBufferDesc, sizeof(stBufferDesc));

	stBufferDesc.dwSize = sizeof(stBufferDesc);
	stBufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;

	LPDIRECTSOUNDBUFFER pPrimaryBuffer = nullptr;
	m_pDirectSound->CreateSoundBuffer(&stBufferDesc, &pPrimaryBuffer, NULL);
	// }

	// 버퍼 포맷을 설정한다
	// {
	WAVEFORMATEX stWaveFormat;
	ZeroMemory(&stWaveFormat, sizeof(stWaveFormat));

	stWaveFormat.cbSize = sizeof(stWaveFormat);
	stWaveFormat.wFormatTag = WAVE_FORMAT_PCM;
	stWaveFormat.nChannels = 2;
	stWaveFormat.nSamplesPerSec = 22050;
	stWaveFormat.wBitsPerSample = 16;
	stWaveFormat.nBlockAlign = (stWaveFormat.wBitsPerSample / 8) * stWaveFormat.nChannels;
	stWaveFormat.nAvgBytesPerSec = stWaveFormat.nSamplesPerSec * stWaveFormat.nBlockAlign;

	pPrimaryBuffer->SetFormat(&stWaveFormat);
	// }

	return pPrimaryBuffer;
}
