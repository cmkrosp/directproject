#pragma once

#include "../../Define/KGlobalDefine.h"

class CSound;

//! 사운드 관리자
class CSoundManager
{
public:

	enum
	{
		MAX_NUM_DUPLICATE_EFFECT_SOUNDS = 5
	};

	// 타입 재정의
	typedef std::array<CSound *, MAX_NUM_DUPLICATE_EFFECT_SOUNDS> TEffectSoundList;
	typedef std::unordered_map<std::string, TEffectSoundList> TEffectSoundListContainer;

public:			// getter, setter

	//! 다이렉트 사운드를 반환한다
	LPDIRECTSOUND getDirectSound(void);
	
	//! 주 버퍼를 반환한다
	LPDIRECTSOUNDBUFFER getPrimaryBuffer(void);

	//! 효과음 볼륨을 변경한다
	void setEffectSoundsVolume(float a_fVolume);

	//! 배경음 볼륨을 변경한다
	void setBackgroundSoundVolume(float a_fVolume);

public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CSoundManager);

	//! 초기화
	virtual void init(void);

	//! 효과음을 재생한다
	void playEffectSound(const std::string &a_rFilepath, bool a_bIsLoop = false);

	//! 배경음을 재생한다
	void playBackgroundSound(const std::string &a_rFilepath, bool a_bIsLoop = true);

private:			// private 함수

	//! 효과음 리스트 컨테이너를 순회한다
	void enumerateEffectSoundListContainer(const std::function<void(CSound *)> &a_rCallback);

	//! 재생 가능한 효과음을 탐색한다
	CSound * findPlayableEffectSound(const std::string &a_rFilepath);

	//! 다이렉트 사운드를 생성한다
	LPDIRECTSOUND createDirectSound(void);

	//! 주 버퍼를 생성한다
	LPDIRECTSOUNDBUFFER createPrimaryBuffer(void);

private:			// private 변수

	float m_fEffectSoundsVolume = 1.0f;
	float m_fBackgroundSoundVolume = 1.0f;

	LPDIRECTSOUND m_pDirectSound = nullptr;
	LPDIRECTSOUNDBUFFER m_pPrimaryBuffer = nullptr;

	CSound *m_pBackgroundSound = nullptr;
	TEffectSoundListContainer m_oEffectSoundListContainer;
};
