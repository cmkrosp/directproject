#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

//! 시간 관리자
class CTimeManager : public IUpdateable
{
public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;

public:			// getter, setter

	//! 시간 간격을 반환한다
	float getDeltaTime(void);

	//! 구동 시간을 반환한다
	float getRunningTime(void);

	//! 시간 비율을 변경한다
	void setTimeScale(float a_fTimeScale);

public:			// public 함수

	//! 싱글턴
	DECLARE_SINGLETON(CTimeManager);

	//! 초기화
	virtual void init(void);

private:			// private 변수

	float m_fTimeScale = 1.0f;
	float m_fDeltaTime = 0.0f;
	float m_fRunningTime = 0.0f;

	std::chrono::system_clock::time_point m_oPrevTimePoint;
	std::chrono::system_clock::time_point m_oStartTimePoint;
};
