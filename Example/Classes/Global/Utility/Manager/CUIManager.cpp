#include "CUIManager.h"
#include "../Manager/CInputManager.h"
#include "../UI/CUIProgressBar.h"
#include "../UI/CUIItemShutCut.h"
#include "../../../Item/CItem.h"

CUIManager::CUIManager(Player* a_pPlayer, CUIWindow* a_pWindow)
	: m_pPlayer(a_pPlayer)
	, m_pUIRoot(a_pWindow)
{
	m_fHPBarValue = m_pPlayer->getHp();
	
}

CUIManager::~CUIManager(void)
{
}


void CUIManager::update(void)
{
	if (IS_KEY_PRESSED(DIK_RIGHT))
	{
		auto pOpenWindow = m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_OPENWINDOW);

		auto pItemset1 = (CUIImage*)pOpenWindow->findChildObjectbyTag(1);
		auto pItemset2 = (CUIImage*)pOpenWindow->findChildObjectbyTag(2);

		pItemset1->setIsHide(!pItemset1->getIsHide());
		pItemset2->setIsHide(!pItemset2->getIsHide());
	}

	if (m_fHPBarValue != m_pPlayer->getHp())
	{
		if (m_fHPBarValue < m_pPlayer->getHp())
		{
			m_fHPBarValue += 0.3f;

			if (m_fHPBarValue > m_pPlayer->getHp())
			{
				m_fHPBarValue = m_pPlayer->getHp();
			}
		}
		else
		{
			m_fHPBarValue = m_pPlayer->getHp();
		}

		CUIProgressBar* pHPBar = (CUIProgressBar*)(m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_HP));
		pHPBar->setCurrentValue(m_fHPBarValue);
	}

	if (m_pPlayer->getPoison())
	{
		auto pPoison = (CUIImage*)m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_POISON);

		if (pPoison->getIsHide())
		{
			pPoison->setIsHide(false);
		}
	}
	else
	{
		auto pPoison = (CUIImage*)m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_POISON);
		pPoison->setIsHide(true);
	}

	CUIProgressBar* pStaminaBar = (CUIProgressBar*)(m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_STAMINA));
	pStaminaBar->setCurrentValue(m_pPlayer->getSp());

	CUIItemShutCut* pItemShutCut = (CUIItemShutCut*)(m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_ITEMSHOTCUT));

	auto stParameters = pItemShutCut->getCurrentItem()->getParameters();

	if (IS_KEY_PRESSED(DIK_C))
	{
		pItemShutCut->itemReplacement();

		stParameters = pItemShutCut->getCurrentItem()->getParameters();

		m_pPlayer->setCurrentItem(stParameters.m_eItemType);
	}

	if (IS_KEY_PRESSED(DIK_F))
	{
		switch (stParameters.m_eItemType)
		{
		case CItem::EITEMTYPE::HPPOTION:
		{
			if (m_pPlayer->getHp() + stParameters.m_nOption > m_pPlayer->getMaxHp())
			{
				m_pPlayer->setHp(m_pPlayer->getMaxHp());
				break;
			}

			m_pPlayer->setHp(m_pPlayer->getHp() + stParameters.m_nOption);
		}
			break;
		case CItem::EITEMTYPE::MEAT:
		{
			m_pPlayer->setMaxSp(m_pPlayer->getMaxSp() + stParameters.m_nOption);

			CUIProgressBar* pStaminaBar = (CUIProgressBar*)(m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_STAMINA));
			pStaminaBar->setMaxValue(m_pPlayer->getMaxSp());
		}
			break;
		case CItem::EITEMTYPE::CUREPOTION:
		{
			auto pPoison = (CUIImage*)m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_POISON);

			pPoison->setIsHide(true);
		}
			break;
		}
	}
}
