#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"
#include "../../../Player/Player.h"
#include "../UI/CUIWindow.h"

class CUIManager : public IUpdateable
{

public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;

public:			// getter



public:			// public 함수

	//! 초기화
	CUIManager(Player * a_pPlayer, CUIWindow * a_pWindow);
	~CUIManager(void);

private:			// private 함수



private:			// private 변수

	float m_fHPBarValue = 0.0f;

	Player* m_pPlayer = nullptr;
	CUIWindow* m_pUIRoot = nullptr;
};