#include "CWindowManager.h"
#include "../Base/CWindowApplication.h"

//! 윈도우 프로시저
static LRESULT CALLBACK WndProc(HWND a_hWindow,
	UINT a_nMessage, WPARAM a_wParam, LPARAM a_lParam)
{
	static IWindowMessageHandler *pMessageHandler = nullptr;

	/*
	WM_CREATE 메세지는 윈도우 생성 될 때 전달되며, 전달 인자로 넘겨지는
	LPARAM 데이터는 CreateWindow 함수 호출 시 전달했던 모든 인자를 보관하고
	있는 CREATESTRUCT 구조체 변수의 포인터이다.

	(즉, 윈도우를 생성하는 시점에서 특정 값을 전달하고 싶다면 해당
	매개 변수를 이용하면 된다.)
	*/
	switch (a_nMessage) {
	case WM_CREATE: {
		auto pstCreateStruct = (CREATESTRUCT *)a_lParam;
		pMessageHandler = (IWindowMessageHandler *)pstCreateStruct->lpCreateParams;

		break;
	}
	}

	if (pMessageHandler != nullptr) {
		return pMessageHandler->handleWindowMessage(a_hWindow,
			a_nMessage, a_wParam, a_lParam);
	}

	return DefWindowProc(a_hWindow, a_nMessage, a_wParam, a_lParam);
}

CWindowManager::CWindowManager(void)
{
	ZeroMemory(&m_stWindowSize, sizeof(m_stWindowSize));
	ZeroMemory(&m_stWindowClass, sizeof(m_stWindowClass));
}

CWindowManager::~CWindowManager(void)
{
	UnregisterClass(m_stWindowClass.lpszClassName, m_hInstance);
}

void CWindowManager::init(HINSTANCE a_hInstance, const SIZE & a_rstWindowSize, IWindowMessageHandler * a_pMessageHandler)
{
	m_hInstance = a_hInstance;
	m_stWindowSize = a_rstWindowSize;
	m_pMessageHandler = a_pMessageHandler;

	m_hWindow = this->createWindow();
}


LRESULT CWindowManager::handleWindowMessage(HWND a_hWindow, UINT a_nMessage, WPARAM a_wParam, LPARAM a_lParam)
{
	switch (a_nMessage) {
	case WM_SIZE: {
		m_stWindowSize.cx = LOWORD(a_lParam);
		m_stWindowSize.cy = HIWORD(a_lParam);

		GET_WINDOW_APPLICATION()->setWindowSize(m_stWindowSize);
		break;
	}
	}

	return m_pMessageHandler->handleWindowMessage(a_hWindow,
		a_nMessage, a_wParam, a_lParam);
}

SIZE CWindowManager::getWindowSize(void)
{
	return m_stWindowSize;
}

HWND CWindowManager::getWindowHandle(void)
{
	return m_hWindow;
}

HINSTANCE CWindowManager::getInstanceHandle(void)
{
	return m_hInstance;
}


HWND CWindowManager::createWindow(void)
{

	WNDCLASS stWindowClass = {
		CS_VREDRAW | CS_HREDRAW,
		WndProc,
		0,
		0,
		m_hInstance,
		LoadIcon(NULL, IDI_APPLICATION),
		LoadCursor(NULL, IDC_ARROW),
		(HBRUSH)GetStockObject(COLOR_WINDOW + 1),
		NULL,
		_T("Example")
	};

	// 윈도우 클래스를 등록한다
	RegisterClass(&stWindowClass);

	RECT stWindowRect = {
		0, 0, m_stWindowSize.cx, m_stWindowSize.cy
	};

	// 윈도우 크기를 계산한다
	AdjustWindowRect(&stWindowRect, WS_OVERLAPPEDWINDOW, FALSE);

	// 윈도우를 생성한다
	return CreateWindow(stWindowClass.lpszClassName,
		stWindowClass.lpszClassName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		stWindowRect.right - stWindowRect.left,
		stWindowRect.bottom - stWindowRect.top,
		GetDesktopWindow(),
		NULL,
		stWindowClass.hInstance,
		this);
}
