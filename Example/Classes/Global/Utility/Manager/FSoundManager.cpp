#include "FSoundManager.h"
#include "fmod_errors.h"

FSoundManager::FSoundManager()
{
}

FSoundManager::~FSoundManager()
{
	//사운드 채널 삭제
	if (_channel != NULL || _sound != NULL)
	{
		for (int i = 0; i < TOTALSOUNDBUFFER; i++)
		{
			if (_channel != NULL)
			{
				if (_channel[i])
				{
					_channel[i]->stop();
				}
			}
			if (_sound != NULL)
			{
				if (_sound[i])
				{
					_sound[i]->release();
				}
			}
		}
	}

	SAFE_DELETE_ARRAY(_sound);
	SAFE_DELETE_ARRAY(_channel);

	//사운드 시스템 닫기
	if (_system != NULL)
	{
		_system->release();
		_system->close();
	}
}

void FSoundManager::init(void)
{
	//사운드 시스템 생성
	System_Create(&_system);
	//사운드 채널 수 설정
	_system->init(TOTALSOUNDBUFFER, FMOD_INIT_NORMAL, 0);

	//사운드 , 채널클래스를 동적할당 해준다
	_sound = new Sound * [TOTALSOUNDBUFFER];
	_channel = new Channel * [TOTALSOUNDBUFFER];

	memset(_sound, 0, sizeof(Sound*) * TOTALSOUNDBUFFER);
	memset(_channel, 0, sizeof(Channel*) * TOTALSOUNDBUFFER);


}

void FSoundManager::release(void)
{

}

void FSoundManager::update(void)
{
	//사운드 시스템의 볼륨을 변경하거나 , 재생이 끝난 후 사운드를 변경하거나 등등
	//사운드 관련 담당을 하고 변경을 처리해준다.

	_system->update();

}


void FSoundManager::addSound(string keyName, string soundName, bool bgm, bool loop)
{
	if (loop)   //bgm
	{
		if (bgm)
		{
			_system->createStream(soundName.c_str(), FMOD_LOOP_NORMAL, 0, &_sound[_mTotalSound.size()]);
		}
		else     //효과음인데 ? 루프냐?
		{
			_system->createSound(soundName.c_str(), FMOD_LOOP_NORMAL, 0, &_sound[_mTotalSound.size()]);
		}
	}

	else //효과음
	{
		_system->createSound(soundName.c_str(), FMOD_DEFAULT, 0, &_sound[_mTotalSound.size()]);
	}

	//셋팅이 끝난후에 사운드를 키값과 함꼐 담아둔다 맵에
	_mTotalSound.insert(make_pair(keyName, &_sound[_mTotalSound.size()]));

}

void FSoundManager::play(string keyName, float volume)
{
	//몇번쨰 채널이냐?
	int count = 0;
	arrSoundIter iter = _mTotalSound.begin();
	for (iter; iter != _mTotalSound.end(); ++iter, count++)
	{
		if (keyName == iter->first)
		{
			//빈채널을 찾아서 볼륨을 성정하고 빈 채널에서 재생한다.
			_system->playSound(FMOD_CHANNEL_FREE, *iter->second, false, &_channel[count]);

			//볼륨세팅
			_channel[count]->setVolume(volume);
		}
	}
}

void FSoundManager::stop(string keyName)
{
	int count = 0;
	arrSoundIter iter = _mTotalSound.begin();
	for (iter; iter != _mTotalSound.end(); ++iter, count++)
	{
		if (keyName == iter->first)
		{
			//사운드 정지
			_channel[count]->stop();
			break;
		}
	}
}

void FSoundManager::pause(string keyName)
{
	int count = 0;
	arrSoundIter iter = _mTotalSound.begin();
	for (iter; iter != _mTotalSound.end(); ++iter, count++)
	{
		if (keyName == iter->first)
		{
			//사운드 일시정지
			_channel[count]->setPaused(true);
			break;
		}
	}
}

bool FSoundManager::isPlaying(string keyName)
{
	int count = 0;
	bool bIsPlaying;
	arrSoundIter iter = _mTotalSound.begin();
	for (iter; iter != _mTotalSound.end(); ++iter, count++)
	{
		if (keyName == iter->first)
		{
			return _channel[count]->isPlaying(&bIsPlaying);
			break;
		}
	}
	return bIsPlaying;
}

void FSoundManager::resume(string keyName)
{
	int count = 0;
	arrSoundIter iter = _mTotalSound.begin();
	for (iter; iter != _mTotalSound.end(); ++iter, count++)
	{
		if (keyName == iter->first)
		{
			//사운드 다시재생
			_channel[count]->setPaused(false);
			break;
		}
	}
}

