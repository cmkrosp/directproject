#pragma once
#include "../../Define/KGlobalDefine.h"
#include "fmod.hpp"
#define MAX_CHANNEL 100

using namespace FMOD;
using namespace std;

//채널 , 사운드 버퍼 설정
#define EXTRACHANNELBUFFER 10
#define SOUNDBUFFER 100

//총 사운드 버퍼설정
#define TOTALSOUNDBUFFER (EXTRACHANNELBUFFER + SOUNDBUFFER)

class FSoundManager
{

private:
	typedef unordered_map<string, Sound**> arrSounds;
	typedef unordered_map<string, Sound**>::iterator arrSoundIter;

private:
	System* _system;				//시스템 클래스
	Sound** _sound;					//사운드 클래스
	Channel** _channel;				//채널 클래스
	arrSounds _mTotalSound;			//맵에 담아둘 사운드 들
public:

	DECLARE_SINGLETON(FSoundManager);

	virtual void init(void);
	void release(void);
	void update(void);

	//사운드추가 (키값 ,파일이름 , BGM , LOOP ?)
	void addSound(string keyName, string soundName, bool bgm = false, bool loop = false);
	//사운드 플레이 (0.0f ~ 1.0f)
	void play(string keyName, float volume = 1.0f);
	//사운드정지
	void stop(string keyName);
	//사운드 일시정지
	void pause(string keyName);
	//사운드 재생여부
	bool isPlaying(string keyName);
	//사운드 다시재생
	void resume(string keyName);
	
};

