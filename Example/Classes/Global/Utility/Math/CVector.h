#pragma once

#include "../../Define/KGlobalDefine.h"

class CMatrix;

//! 벡터
class CVector
{
public:			// getter

	//! 벡터의 길이를 반환한다
	float getLength(void);

	//! 단위 벡터를 반환한다
	CVector getNormalizeVector(void);

	//! 벡터의 덧셈 결과를 반환한다
	CVector getAddVector(const CVector &a_rVector);

	//! 벡터의 뺄셈 결과를 반환한다
	CVector getSubVector(const CVector &a_rVector);

	//! 벡터의 스칼라 곱셈 결과를 반환한다
	CVector getMultiplyScalar(float a_fScalar);

	//! 벡터의 내적 결과를 반환한다
	float getDotVector(const CVector &a_rVector);

	//! 벡터의 외적 결과를 반환한다
	CVector getCrossVector(const CVector &a_rVector);

	//! 변환 결과를 반환한다
	CVector getTransformationVector(const CMatrix &a_rMatrix);

public:			// 생성자

	//! 생성자
	CVector(float a_fX, float a_fY, float a_fZ, float a_fW = 0.0f);

public:			// public 변수

	float m_fX = 0.0f;
	float m_fY = 0.0f;
	float m_fZ = 0.0f;
	float m_fW = 0.0f;
};
