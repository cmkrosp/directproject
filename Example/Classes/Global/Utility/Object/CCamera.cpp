#include "CCamera.h"
#include "../Manager/CInputManager.h"

CCamera::CCamera(float a_fFov, float a_fAspect)
:
m_fFov(a_fFov),
m_fAspect(a_fAspect)
{
	D3DXMatrixIdentity(&m_stViewMatrix);
	D3DXMatrixIdentity(&m_stProjectionMatrix);
}

float CCamera::getFov(void)
{
	return m_fFov;
}

float CCamera::getAspect(void)
{
	return m_fAspect;
}

D3DXMATRIXA16 & CCamera::getViewMatrix(void)
{
	return m_stViewMatrix;
}

D3DXMATRIXA16 & CCamera::getProjectionMatrix(void)
{
	return m_stProjectionMatrix;
}

void CCamera::setFov(float a_fFov)
{
	m_fFov = a_fFov;
	this->setupProjectionMatrix();
}

void CCamera::setAspect(float a_fAspect)
{
	m_fAspect = a_fAspect;
	this->setupProjectionMatrix();
}

void CCamera::setLookOffset(const D3DXVECTOR3 & a_rstLookOffset)
{
	m_stLookOffset = a_rstLookOffset;
}

void CCamera::setFollowOffset(const D3DXVECTOR3 & a_rstFollowOffset)
{
	m_stFollowOffset = a_rstFollowOffset;
}

void CCamera::setTargetObject(CObject * a_pTargetObject, const D3DXVECTOR3 & a_rstLookOffset, const D3DXVECTOR3 & a_rstFollowOffset, const EFollowType a_eFollowType)
{
	m_pTargetObject = a_pTargetObject;
	m_stLookOffset = a_rstLookOffset;
	m_stFollowOffset = a_rstFollowOffset;
	m_eFollowType = a_eFollowType;
}

void CCamera::doUpdate(void)
{
	CObject::doUpdate();

	// 格钎 按眉啊 乐阑 版快
	if (m_pTargetObject != nullptr) {
		auto stPosition = m_pTargetObject->getPosition();

		if (m_eFollowType == EFollowType::LOCK) {
			this->setPosition(stPosition + m_stFollowOffset);
			this->rotateByPosition(stPosition + m_stLookOffset);
		}
		else {
			D3DXMATRIXA16 stRotate;
			D3DXMatrixIdentity(&stRotate);

			CopyMemory(&stRotate(0, 0), &m_stRightDirection, sizeof(m_stRightDirection));
			CopyMemory(&stRotate(1, 0), &m_stUpDirection, sizeof(m_stUpDirection));
			CopyMemory(&stRotate(2, 0), &m_stForwardDirection, sizeof(m_stForwardDirection));

			auto stFollowOffset = D3DXVECTOR3(0.0f, 0.0f, m_stFollowOffset.z);
			D3DXVec3TransformCoord(&stFollowOffset, &stFollowOffset, &stRotate);

			this->setPosition(stPosition + stFollowOffset + m_stLookOffset);
			this->rotateByPosition(stPosition + m_stLookOffset);
		}

		if (m_eFollowType == EFollowType::LOCK) {
			m_stFollowOffset += m_stForwardDirection * (GET_MOUSE_WHEEL() * 0.025f);
		}
		else {
			m_stFollowOffset.z += GET_MOUSE_WHEEL() * 0.025f;
		}
	}

	/*
	轰 青纺 夸家
	:
	1青 :				Rx,			   Ux,				   Fx,  0,
	2青 :				Ry,			   Uy,				   Fy,  0,
	3青 :				Rz,			   Uz,				   Fz,  0,
	4青 : -(Pos dot Right), -(Pos dot Up), -(Pos dot Forward),  1
	*/
	if (m_pTargetObject != nullptr || m_bIsUpdateEnable) {
		D3DXMatrixIdentity(&m_stViewMatrix);

		m_stViewMatrix(0, 0) = m_stRightDirection.x;
		m_stViewMatrix(1, 0) = m_stRightDirection.y;
		m_stViewMatrix(2, 0) = m_stRightDirection.z;
		m_stViewMatrix(3, 0) = -D3DXVec3Dot(&m_stPosition, &m_stRightDirection);
		
		m_stViewMatrix(0, 1) = m_stUpDirection.x;
		m_stViewMatrix(1, 1) = m_stUpDirection.y;
		m_stViewMatrix(2, 1) = m_stUpDirection.z;
		m_stViewMatrix(3, 1) = -D3DXVec3Dot(&m_stPosition, &m_stUpDirection);

		m_stViewMatrix(0, 2) = m_stForwardDirection.x;
		m_stViewMatrix(1, 2) = m_stForwardDirection.y;
		m_stViewMatrix(2, 2) = m_stForwardDirection.z;
		m_stViewMatrix(3, 2) = -D3DXVec3Dot(&m_stPosition, &m_stForwardDirection);
	}
}

void CCamera::setupProjectionMatrix(void)
{
	D3DXMatrixPerspectiveFovLH(&m_stProjectionMatrix,
		D3DXToRadian(m_fFov), m_fAspect, 0.1f, 10000);
}
