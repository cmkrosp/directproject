#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CObject.h"

//! 카메라
class CCamera : public CObject
{
public:			// getter, setter

	//! 시야각을 반환한다
	float getFov(void);

	//! 종횡비를 반환한다
	float getAspect(void);

	//! 뷰 행렬을 반환한다
	D3DXMATRIXA16 & getViewMatrix(void);

	//! 투영 행렬을 반환한다
	D3DXMATRIXA16 & getProjectionMatrix(void);

	//! 시야각을 변경한다
	void setFov(float a_fFov);

	//! 종횡비를 변경한다
	void setAspect(float a_fAspect);

	//! 간격을 변경한다
	void setLookOffset(const D3DXVECTOR3 &a_rstLookOffset);

	//! 주시 간격을 변경한다
	void setFollowOffset(const D3DXVECTOR3 &a_rstFollowOffset);

	//! 목표 객체를 변경한다
	void setTargetObject(CObject *a_pTargetObject,
		const D3DXVECTOR3 &a_rstLookOffset, 
		const D3DXVECTOR3 &a_rstFollowOffset,
		const EFollowType a_eFollowType = EFollowType::LOCK);

public:			// 생성자

	//! 생성자
	CCamera(float a_fFov, float a_fAspect);

protected:			// protected 함수

	//! 상태를 갱신한다
	virtual void doUpdate(void) override;

	//! 투영 행렬을 설정한다
	void setupProjectionMatrix(void);

protected:			// protected 변수

	float m_fFov = 0.0f;
	float m_fAspect = 0.0f;

	D3DXMATRIXA16 m_stViewMatrix;
	D3DXMATRIXA16 m_stProjectionMatrix;
	
	D3DXVECTOR3 m_stLookOffset;
	D3DXVECTOR3 m_stFollowOffset;

	CObject *m_pTargetObject = nullptr;
	EFollowType m_eFollowType = EFollowType::NONE;
};
