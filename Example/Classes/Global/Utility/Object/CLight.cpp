#include "CLight.h"

CLight::CLight(const D3DXCOLOR & a_rstDiffuseColor, const D3DXCOLOR & a_rstSpecularColor, const D3DXCOLOR & a_rstAmbientColor)
:
m_stDiffuseColor(a_rstDiffuseColor),
m_stSpecularColor(a_rstSpecularColor),
m_stAmbientColor(a_rstAmbientColor)
{
	D3DXMatrixIdentity(&m_stLightViewMatrix);
	D3DXMatrixIdentity(&m_stLightProjectionMatrix);
}

D3DXCOLOR & CLight::getDiffuseColor(void)
{
	return m_stDiffuseColor;
}

D3DXCOLOR & CLight::getSpecularColor(void)
{
	return m_stSpecularColor;
}

D3DXCOLOR & CLight::getAmbientColor(void)
{
	return m_stAmbientColor;
}

void CLight::doUpdate(void)
{
	CObject::doUpdate();

	if (m_bIsUpdateEnable) {
		D3DXMatrixIdentity(&m_stLightViewMatrix);

		m_stLightViewMatrix(0, 0) = m_stRightDirection.x;
		m_stLightViewMatrix(1, 0) = m_stRightDirection.y;
		m_stLightViewMatrix(2, 0) = m_stRightDirection.z;
		m_stLightViewMatrix(3, 0) = -D3DXVec3Dot(&m_stPosition, &m_stRightDirection);

		m_stLightViewMatrix(0, 1) = m_stUpDirection.x;
		m_stLightViewMatrix(1, 1) = m_stUpDirection.y;
		m_stLightViewMatrix(2, 1) = m_stUpDirection.z;
		m_stLightViewMatrix(3, 1) = -D3DXVec3Dot(&m_stPosition, &m_stUpDirection);
			
		m_stLightViewMatrix(0, 2) = m_stForwardDirection.x;
		m_stLightViewMatrix(1, 2) = m_stForwardDirection.y;
		m_stLightViewMatrix(2, 2) = m_stForwardDirection.z;
		m_stLightViewMatrix(3, 2) = -D3DXVec3Dot(&m_stPosition, &m_stForwardDirection);
	}
}
