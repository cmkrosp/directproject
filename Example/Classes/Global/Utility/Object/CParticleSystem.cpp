#include "CParticleSystem.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CTimeManager.h"
#include "../Manager/CResourceManager.h"

CParticleSystem::CParticleSystem(const STParameters & a_rstParameters)
:
CRenderObject(CRenderObject::STParameters{
	sizeof(STColorTextureVertex),
	DXGI_FORMAT_UNKNOWN,

	KEY_COLOR_TEXTURE_SHADER,
	GetFormatString("ParticleSystemVertexBuffer%d", a_rstParameters.m_stEmitterParameters.m_nMaxNumParticles),
	"",
	KEY_TRANSFORM_BUFFER,
	KEY_COLOR_TEXTURE_RENDER_BUFFER,
	KEY_COLOR_TEXTURE_INPUT_LAYOUT
}),

m_stParticleParameters(a_rstParameters)
{
	m_pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);

	if (m_pVertexBuffer == nullptr) {
		m_pVertexBuffer = this->createVertexBuffer();

		GET_RESOURCE_MANAGER()->addBuffer(m_stParameters.m_oVertexBufferKey,
			m_pVertexBuffer);
	}

	m_pParticleEmitter = Create<CParticleEmitter>(m_stPosition, a_rstParameters.m_stEmitterParameters);
}

CParticleSystem::~CParticleSystem(void)
{
	SAFE_DELETE(m_pParticleEmitter);
}

void CParticleSystem::startParticleEmit(float a_fActiveTime)
{
 	m_bIsEmitEnable = true;
	m_fLeftActiveTime = a_fActiveTime;

	m_pParticleEmitter->setEmitEnable(true);
}

void CParticleSystem::stopParticleEmit(void)
{
	m_bIsEmitEnable = false;
	m_fLeftActiveTime = 0.0f;

	m_pParticleEmitter->setEmitEnable(false);
}

void CParticleSystem::setPosition(const D3DXVECTOR3& a_rstPosition)
{
	CRenderObject::setPosition(a_rstPosition);

	m_pParticleEmitter->setPosition(a_rstPosition);
}

void CParticleSystem::doUpdate(void)
{
	CRenderObject::doUpdate();
	m_pParticleEmitter->update();

	if (m_bIsEmitEnable) {
		m_fLeftActiveTime -= GET_DELTA_TIME();

		if (m_fLeftActiveTime <= 0.0f) {
			this->stopParticleEmit();
		}
	}
}

void CParticleSystem::doRender(void)
{
	CRenderObject::doRender();
	auto &rParticleList = m_pParticleEmitter->getParticleList();

	// 정점 정보를 설정한다
	// {
	auto pVertexBuffer = GET_BUFFER(m_stParameters.m_oVertexBufferKey);

	if (pVertexBuffer != nullptr) {
		auto oVertexBufferPtr = GetBufferPointer<STColorTextureVertex>(pVertexBuffer);
		auto pstVertices = oVertexBufferPtr.get();

		for (int i = 0; i < rParticleList.size(); ++i) {
			D3DXVECTOR3 astVertices[6] = {
				D3DXVECTOR3(-0.5f, -0.5f, 0.0f),
				D3DXVECTOR3(-0.5f, 0.5f, 0.0f),
				D3DXVECTOR3(0.5f, 0.5f, 0.0f),

				D3DXVECTOR3(-0.5f, -0.5f, 0.0f),
				D3DXVECTOR3(0.5f, 0.5f, 0.0f),
				D3DXVECTOR3(0.5f, -0.5f, 0.0f)
			};

			// 행렬을 설정한다
			// {
			D3DXMATRIXA16 stTranslate;

			D3DXMatrixTranslation(&stTranslate, 
				rParticleList[i].m_stPosition.x,
				rParticleList[i].m_stPosition.y,
				rParticleList[i].m_stPosition.z);

			D3DXMATRIXA16 stScale;

			D3DXMatrixScaling(&stScale,
				rParticleList[i].m_fScale,
				rParticleList[i].m_fScale,
				rParticleList[i].m_fScale);

			D3DXMATRIXA16 stRotate;

			D3DXMatrixRotationYawPitchRoll(&stRotate,
				D3DXToRadian(rParticleList[i].m_stRotation.y),
				D3DXToRadian(rParticleList[i].m_stRotation.x),
				D3DXToRadian(rParticleList[i].m_stRotation.z));

			D3DXMATRIXA16 stWorldMatrix = stScale * stRotate * stTranslate;

			D3DXVec3TransformCoordArray(astVertices,
				sizeof(astVertices[0]),
				astVertices,
				sizeof(astVertices[0]),
				&stWorldMatrix,
				sizeof(astVertices) / sizeof(astVertices[0]));
			// }

			int nIndex = i * 6;

			pstVertices[nIndex + 0].m_stUV = D3DXVECTOR2(0.0f, 1.0f);
			pstVertices[nIndex + 1].m_stUV = D3DXVECTOR2(0.0f, 0.0f);
			pstVertices[nIndex + 2].m_stUV = D3DXVECTOR2(1.0f, 0.0f);

			pstVertices[nIndex + 3].m_stUV = D3DXVECTOR2(0.0f, 1.0f);
			pstVertices[nIndex + 4].m_stUV = D3DXVECTOR2(1.0f, 0.0f);
			pstVertices[nIndex + 5].m_stUV = D3DXVECTOR2(1.0f, 1.0f);

			for (int j = 0; j < sizeof(astVertices) / sizeof(astVertices[0]); ++j) {
				pstVertices[nIndex + j].m_stPosition = astVertices[j];
				pstVertices[nIndex + j].m_stColor = rParticleList[i].m_stColor;
			}
		}
	}
	// }

	// 텍스처를 설정한다
	auto pDiffuseMap = GET_SHADER_RESOURCE_VIEW(m_stParticleParameters.m_oDiffuseMapFilepath);
	GET_CONTEXT()->PSSetShaderResources(0, 1, &pDiffuseMap);

	// 물체를 그린다
	GET_CONTEXT()->Draw(rParticleList.size() * 6, 0);
}

ID3D11Buffer * CParticleSystem::createVertexBuffer(void)
{
	int nVertexCount = m_stParticleParameters.m_stEmitterParameters.m_nMaxNumParticles * 6;
	
	return CreateBuffer(sizeof(STColorTextureVertex) * nVertexCount,
		D3D11_BIND_VERTEX_BUFFER);
}
