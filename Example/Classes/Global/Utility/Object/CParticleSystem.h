#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"
#include "../Particle/CParticleEmitter.h"

class CParticleSystem : public CRenderObject
{
public:

	//! 매개 변수
	struct STParameters
	{
		std::string m_oDiffuseMapFilepath;
		CParticleEmitter::STParameters m_stEmitterParameters;
	};

public:			// public 함수

	//! 파티클을 재생한다
	void startParticleEmit(float a_fActiveTime);

	//! 파티클을 중지한다
	void stopParticleEmit(void);

	//! 위치를 변경한다
	void setPosition(const D3DXVECTOR3& a_rstPosition);

public:			// 생성자

	//! 생성자
	CParticleSystem(const STParameters &a_rstParameters);

	//! 소멸자
	virtual ~CParticleSystem(void);

protected:			// protected 함수

	//! 상태를 갱신한다
	virtual void doUpdate(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;

private:			// prviate 함수

	//! 정점 버퍼를 생성한다
	ID3D11Buffer * createVertexBuffer(void);

private:			// private 변수

	bool m_bIsEmitEnable = false;
	float m_fLeftActiveTime = 0.0f;

	STParameters m_stParticleParameters;

	ID3D11Buffer *m_pVertexBuffer = nullptr;
	CParticleEmitter *m_pParticleEmitter = nullptr;
};
