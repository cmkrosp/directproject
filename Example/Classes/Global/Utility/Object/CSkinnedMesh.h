#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Skinned/CAllocateHierarchy.h"
#include "../Base/CRenderObject.h"

class CMesh;
class CAnimationController;

//! 스킨드 메시
class CSkinnedMesh : public CRenderObject
{
public:

	//! 매개 변수
	struct STParameters
	{
		std::string m_oMeshKey;
		std::string m_oShaderKey;
		std::string m_oTransformBufferKey;
		std::string m_oRenderBufferKey;
		std::string m_oInputLayoutKey;

		std::vector<D3D11_INPUT_ELEMENT_DESC> m_oElementDescList;
	};

public:			// getter, setter

	D3DXMATRIX getBoneMatrix(std::string a_rstBoneName);

	CAllocateHierarchy::STBone* getBone(std::string a_rstBoneName);

	CAllocateHierarchy::STBone* getTailBone();

	D3DXMATRIX& getTailBone2Matrix();

	CAllocateHierarchy::STBone * getRootBone();

	LPD3DXFRAME getNullBone();

	//! 애니메이션 이름 리스트를 반환한다
	std::vector<std::string> & getAnimationNameList(void);

	//! 시간 비율을 변경한다
	void setTimeScale(float a_fTimeScale);

	//! 애니메이션을 반환한다
	LPD3DXANIMATIONSET getAnimationSet(int num);

	//! DBL Percent를 반환한다
	double getDblPercent(void);

	double getDblPercent(std::string name);

	void setTailBone2Matrix(const D3DXMATRIXA16 &matrix);

public:			// public 함수

		//! 애니메이션을 복제한다
	void clonAnimation(LPD3DXANIMATIONSET a_pAnimationSet);

	//! 애니메이션을 시작한다
	void playAnimation(const std::string &a_rAnimationName, bool a_bIsLoop);

	void playAnimationWithFrame(int a_nStartFrame,int a_nEndFrame,const std::string& a_rAnimationName, bool a_bIsLoop);

	//! 애니메이션을 중지한다
	void stopAnimation(void);

public:			// 생성자, 소멸자

	//! 생성자
	CSkinnedMesh(const STParameters &a_rstParameters);

	//! 소멸자
	virtual ~CSkinnedMesh(void);

protected:			// protected 함수

	//! 상태를 갱신한다
	virtual void doUpdate(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;

private:			// private 함수

	//! 본을 갱신한다
	void updateBone(LPD3DXFRAME a_pstFrame, const D3DXMATRIXA16 &a_rstMatrix);

	//! 본을 그린다
	void drawBone(LPD3DXFRAME a_pstFrame);

	//! 메시 컨테이너를 그린다
	void drawMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer);

	//! 본을 설정한다
	void setupBone(LPD3DXFRAME a_pstFrame);

	//! 본을 메시 컨테이너에 설정한다
	void setupBoneOnMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer);

	//! 스킨드 메시를 생성한다
	CMesh * createSkinnedMesh(LPD3DXMESHCONTAINER a_pstMeshContainer,
		int a_nMeshContainerNumber);

	//! 스킨드 메시를 생성한다
	CMesh * createSkinnedMeshFromX(void);

	CAllocateHierarchy::STBone *serchBone(LPD3DXFRAME a_pstFrame, std::string a_rstBoneName);

private:			// private 변수

	STParameters m_stSkinnedParameters;

	CMesh *m_pSkinnedMesh = nullptr;
	CAnimationController *m_pAnimationController = nullptr;

	CAllocateHierarchy::STBone *m_pstRootBone = nullptr;
	std::vector<CAllocateHierarchy::STMeshContainer *> m_oMeshContainerList;

	// 장원석 추가
	CAllocateHierarchy::STBone * m_pBone;
	CAllocateHierarchy::STBone *m_pTailBone = nullptr;
	CAllocateHierarchy::STBone *m_pTailBone2 = nullptr;
	LPD3DXFRAME m_pNullBone = nullptr;
};
