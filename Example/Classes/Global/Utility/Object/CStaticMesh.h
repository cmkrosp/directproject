#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

class CMesh;

//! 정적 메시
class CStaticMesh : public CRenderObject
{
public:

	//! 매개 변수
	struct STParameters
	{
		std::string m_oMeshKey;
		std::string m_oShaderKey;
		std::string m_oTransformBufferKey;
		std::string m_oRenderBufferKey;
		std::string m_oInputLayoutKey;

		EStaticMeshType m_eStaticMeshType;
		std::vector<D3D11_INPUT_ELEMENT_DESC> m_oElementDescList;
	};

public:			// setter

	//! 재질을 변경한다
	void setMaterial(const STMaterial &a_rstMaterial);

	std::string getMeshKey();

public:			// 생성자

	//! 생성자
	CStaticMesh(const STParameters &a_rstParameters);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void) override;

	//! 물체를 그린다
	virtual void doRender(void) override;

private:			// private 함수

	//! 메시를 생성한다
	CMesh * createMesh(void);

private:			// private 변수

	STMaterial m_stMaterial;
	STParameters m_stStaticParameters;
};
