#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CRenderObject.h"

class CMesh;

//! 지형
class CTerrain : public CRenderObject
{
public:

	enum
	{
		MAX_NUM_TEXTURES = 4
	};

	//! 매개 변수
	struct STParameters
	{
		std::string m_oMeshKey;
		std::string m_oShaderKey;
		std::string m_oTransformBufferKey;
		std::string m_oRenderBufferKey;
		std::string m_oInputLayoutKey;

		int m_nSmoothLevel;
		float m_fHeightScale;

		SIZE m_stMapSize;
		D3DXVECTOR2 m_stTileSize;

		std::string m_oSplatMapFilepath;
		std::string m_oHeightMapFilepath;

		std::vector<std::string> m_oDiffuseMapFilepathList;
		std::vector<D3D11_INPUT_ELEMENT_DESC> m_oElementDescList;
	};

public:			// getter

	//! 높이를 반환한다
	float getHeightAtPosition(const D3DXVECTOR3 &a_rstPosition);

public:			// 생성자

	//! 생성자
	CTerrain(const STParameters &a_rstParameters);

protected:			// protected 함수

	//! 물체를 그리기 전
	virtual void preRender(void) override;
	
	//! 물체를 그린다
	virtual void doRender(void) override;

private:			// private 함수

	//! 지형을 부드럽게 만든다
	void smoothTerrain(void);

	//! 지형 정보를 설정한다
	void setupTerrainInfo(void);

	//! 지형 메시를 생성한다
	CMesh * createTerrainMesh(void);

private:			// private 변수

	SIZE m_stTotalSize;
	STParameters m_stTerrainParameters;

	std::vector<float> m_oHeightList;
};
