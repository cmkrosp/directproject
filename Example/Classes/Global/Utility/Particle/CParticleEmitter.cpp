#include "CParticleEmitter.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CTimeManager.h"

CParticleEmitter::CParticleEmitter(const D3DXVECTOR3& a_rstPosition, const STParameters& a_rstParameters)
	:
	m_stPosition(a_rstPosition),
	m_stParameters(a_rstParameters),
	m_fSkipTime(1.0f / a_rstParameters.m_nMaxNumParticlesPerSecond)
{
	// Do Nothing
}

void CParticleEmitter::update(void)
{
	if (m_bIsEmitEnable) {
		this->emitParticle();
	}

	this->updateParticle();
	this->removeParticle();
}

std::vector<CParticleEmitter::STParticle>& CParticleEmitter::getParticleList(void)
{
	return m_oParticleList;
}

void CParticleEmitter::setEmitEnable(bool a_bIsEmitEnable)
{
	m_bIsEmitEnable = a_bIsEmitEnable;
}

void CParticleEmitter::emitParticle(void)
{
	m_fSkipTime += GET_DELTA_TIME();

	while (m_fSkipTime >= 1.0f / m_stParameters.m_nMaxNumParticlesPerSecond) {
		m_fSkipTime -= 1.0f / m_stParameters.m_nMaxNumParticlesPerSecond;

		// 파티클 생성이 가능 할 경우
		if (m_oParticleList.size() < m_stParameters.m_nMaxNumParticles) {
			STParticle stParticle;
			ZeroMemory(&stParticle, sizeof(stParticle));

			stParticle.m_bIsActive = true;
			stParticle.m_fMaxActiveTime = m_stParameters.m_fMaxActiveTime;
			stParticle.m_stPosition = m_stPosition;

			// 비율을 설정한다
			stParticle.m_fStartScale = CreateFloatRandomValue(0.25f, m_stParameters.m_fStartScale);
			stParticle.m_fEndScale = CreateFloatRandomValue(3.0f, m_stParameters.m_fEndScale);

			// 색상을 설정한다
			// {
			stParticle.m_stStartColor = m_stParameters.m_stStartColor;

			stParticle.m_stEndColor = m_stParameters.m_stEndColor;
			// }

			// 속도를 설정한다
			// {
			stParticle.m_stVelocity = D3DXVECTOR3(
				CreateFloatRandomValue(-m_stParameters.m_stVelocityBLD.x, m_stParameters.m_stVelocityFRU.x),
				CreateFloatRandomValue(-m_stParameters.m_stVelocityBLD.y, m_stParameters.m_stVelocityFRU.y),
				CreateFloatRandomValue(-m_stParameters.m_stVelocityBLD.z, m_stParameters.m_stVelocityFRU.z));

			D3DXVec3Normalize(&stParticle.m_stVelocity,
				&stParticle.m_stVelocity);

			stParticle.m_stVelocity *= m_stParameters.m_fSpeed;
			// }

			// 회전 속도를 설정한다
			stParticle.m_stAngleVelocity = D3DXVECTOR3(CreateFloatRandomValue(40.0f, 240.0f),
				CreateFloatRandomValue(40.0f, 240.0f),
				CreateFloatRandomValue(40.0f, 240.0f));

			m_oParticleList.push_back(stParticle);
		}
	}
}

void CParticleEmitter::updateParticle(void)
{
	for (auto& rstParticle : m_oParticleList) {
		rstParticle.m_fActiveTime += GET_DELTA_TIME();

		// 활동 시간이 종료되었을 경우
		if (rstParticle.m_fActiveTime >= rstParticle.m_fMaxActiveTime) {
			rstParticle.m_bIsActive = false;
		}
		else {
			rstParticle.m_stPosition += rstParticle.m_stVelocity * GET_DELTA_TIME();
			rstParticle.m_stRotation += rstParticle.m_stAngleVelocity * GET_DELTA_TIME();

			float fDeltaTime = rstParticle.m_fActiveTime / rstParticle.m_fMaxActiveTime;
			fDeltaTime = ClampValue<float>(fDeltaTime, 0.0f, 1.0f);

			rstParticle.m_fScale = rstParticle.m_fStartScale +
				((rstParticle.m_fEndScale - rstParticle.m_fStartScale) * fDeltaTime);

			rstParticle.m_stColor = rstParticle.m_stStartColor +
				((rstParticle.m_stEndColor - rstParticle.m_stStartColor) * fDeltaTime);
		}
	}
}

void CParticleEmitter::removeParticle(void)
{
	auto oIterator = m_oParticleList.begin();

	while (oIterator != m_oParticleList.end()) {
		if (oIterator->m_bIsActive) {
			++oIterator;
		}
		else {
			oIterator = m_oParticleList.erase(oIterator);
		}
	}
}
