#include "CMesh.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"

CMesh::CMesh(const STParameters & a_rstParameters)
:
m_stParameters(a_rstParameters)
{
	// 사본 메시를 설정한다
	// {
	auto pOriginMesh = m_stParameters.m_pMesh;

	m_stParameters.m_pMesh = GetCloneMesh(pOriginMesh,
		m_stParameters.m_pAdjacency, m_stParameters.m_oElementDescList);

	SAFE_RELEASE(pOriginMesh);
	// }

	// 속성을 설정한다
	this->setupAttributes();

	// 정점/인덱스 버퍼를 설정한다
	m_pVertexBuffer = this->createVertexBuffer();
	m_pIndexBuffer = this->createIndexBuffer();
}

CMesh::~CMesh(void)
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pIndexBuffer);

	SAFE_RELEASE(m_stParameters.m_pMesh);
	SAFE_RELEASE(m_stParameters.m_pAdjacency);
}

LPD3DXMESH CMesh::getMesh(void)
{
	return m_stParameters.m_pMesh;
}

LPD3DXBUFFER CMesh::getAdjacency(void)
{
	return m_stParameters.m_pAdjacency;
}

DWORD CMesh::getNumAttributes(void)
{
	return m_nNumAttributes;
}

D3DXATTRIBUTERANGE CMesh::getAttribute(DWORD a_nAttributeID)
{
	auto oIterator = std::find_if(m_oAttributeList.begin(),
		m_oAttributeList.end(), [=](D3DXATTRIBUTERANGE a_stAttribute) -> bool
	{
		return a_stAttribute.AttribId == a_nAttributeID;
	});

	assert(oIterator != m_oAttributeList.end());
	return *oIterator;
}

void CMesh::setupBuffers(void)
{
	// 정점 버퍼를 설정한다
	// {
	UINT nStride = m_stParameters.m_pMesh->GetNumBytesPerVertex();
	UINT nOffset = 0;

	GET_CONTEXT()->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &nStride, &nOffset);
	// }

	// 인덱스 버퍼를 설정한다
	GET_CONTEXT()->IASetIndexBuffer(m_pIndexBuffer, m_eIndexFormat, 0);
}

void CMesh::setupAttributes(void)
{
	// 속성 정보를 설정한다
	// {
	m_stParameters.m_pMesh->GetAttributeTable(NULL, 
		&m_nNumAttributes);

	int nSize = sizeof(D3DXATTRIBUTERANGE);
	auto pstAttributes = (D3DXATTRIBUTERANGE *)malloc(nSize * m_nNumAttributes);

	m_stParameters.m_pMesh->GetAttributeTable(pstAttributes,
		&m_nNumAttributes);

	for (int i = 0; i < m_nNumAttributes; ++i) {
		m_oAttributeList.push_back(pstAttributes[i]);
	}

	SAFE_FREE(pstAttributes);
	// }

	m_eIndexFormat = (m_stParameters.m_pMesh->GetOptions() & D3DXMESH_32BIT) ? DXGI_FORMAT_R32_UINT
		: DXGI_FORMAT_R16_UINT;
}

ID3D11Buffer * CMesh::createVertexBuffer(void)
{
	// 정점 버퍼를 생성하다
	auto pVertexBuffer = CreateBuffer(m_stParameters.m_pMesh->GetNumVertices() * m_stParameters.m_pMesh->GetNumBytesPerVertex(),
		D3D11_BIND_VERTEX_BUFFER);

	// 정점 정보를 설정한다
	// {
	auto oVertexBufferPtr = GetBufferPointer<BYTE>(pVertexBuffer);
	auto pnVertices = oVertexBufferPtr.get();

	BYTE *pnTempVertices = nullptr;

	if (SUCCEEDED(m_stParameters.m_pMesh->LockVertexBuffer(0, (void **)&pnTempVertices))) {
		CopyMemory(pnVertices,
			pnTempVertices, m_stParameters.m_pMesh->GetNumVertices() * m_stParameters.m_pMesh->GetNumBytesPerVertex());

		m_stParameters.m_pMesh->UnlockVertexBuffer();
	}
	// }

	return pVertexBuffer;
}

ID3D11Buffer * CMesh::createIndexBuffer(void)
{
	UINT nIndexSize = (m_eIndexFormat == DXGI_FORMAT_R16_UINT) ? sizeof(WORD) 
		: sizeof(DWORD);

	// 인덱스 버퍼를 생성한다
	auto pIndexBuffer = CreateBuffer(nIndexSize * (m_stParameters.m_pMesh->GetNumFaces() * 3),
		D3D11_BIND_INDEX_BUFFER);

	// 인덱스 정보를 설정한다
	// {
	auto oIndexBufferPtr = GetBufferPointer<BYTE>(pIndexBuffer);
	auto pnIndices = oIndexBufferPtr.get();

	BYTE *pnTempIndices = nullptr;

	if (SUCCEEDED(m_stParameters.m_pMesh->LockIndexBuffer(0, (void **)&pnTempIndices))) {
		CopyMemory(pnIndices,
			pnTempIndices, nIndexSize * (m_stParameters.m_pMesh->GetNumFaces() * 3));

		m_stParameters.m_pMesh->UnlockIndexBuffer();
	}
	// }

	return pIndexBuffer;
}
