#pragma once

#include "../../Define/KGlobalDefine.h"

//! 메시
class CMesh
{
public:

	//! 매개 변수
	struct STParameters
	{
		LPD3DXMESH m_pMesh;
		LPD3DXBUFFER m_pAdjacency;

		std::vector<D3D11_INPUT_ELEMENT_DESC> m_oElementDescList;
	};

public:			// getter

	//! 메시를 반환한다
	LPD3DXMESH getMesh(void);

	//! 인집 정보를 반환한다
	LPD3DXBUFFER getAdjacency(void);

	//! 속성 개수를 반환한다
	DWORD getNumAttributes(void);

	//! 속성을 반환한다
	D3DXATTRIBUTERANGE getAttribute(DWORD a_nAttributeID);

public:			// public 함수

	//! 버퍼를 설정한다
	void setupBuffers(void);

public:			// 생성자, 소멸자

	//! 생성자
	CMesh(const STParameters &a_rstParameters);

	//! 소멸자
	virtual ~CMesh(void);

private:			// private 함수

	//! 속성을 설정한다
	void setupAttributes(void);

	//! 정점 버퍼를 생성한다
	ID3D11Buffer * createVertexBuffer(void);

	//! 인덱스 버퍼를 생성한다
	ID3D11Buffer * createIndexBuffer(void);

private:			// private 변수

	DXGI_FORMAT m_eIndexFormat = DXGI_FORMAT_UNKNOWN;
	STParameters m_stParameters;

	DWORD m_nNumAttributes;
	std::vector<D3DXATTRIBUTERANGE> m_oAttributeList;

	ID3D11Buffer *m_pVertexBuffer = nullptr;
	ID3D11Buffer *m_pIndexBuffer = nullptr;
};
