#include "CShader.h"
#include "../../Function/GlobalFunction.h"

CShader::CShader(const STParameters & a_rstParameters)
:
m_stParameters(a_rstParameters)
{
	m_pVertexShader = this->createVertexShader(&m_pVertexShaderCode);
	m_pPixelShader = this->createPixelShader(&m_pPixelShaderCode);
}

CShader::~CShader(void)
{
	SAFE_RELEASE(m_pVertexShaderCode);
	SAFE_RELEASE(m_pPixelShaderCode);

	SAFE_RELEASE(m_pVertexShader);
	SAFE_RELEASE(m_pPixelShader);
}

ID3D10Blob * CShader::getVertexShaderCode(void)
{
	return m_pVertexShaderCode;
}

ID3D10Blob * CShader::getPixelShaderCode(void)
{
	return m_pPixelShaderCode;
}

ID3D11VertexShader * CShader::getVertexShader(void)
{
	return m_pVertexShader;
}

ID3D11PixelShader * CShader::getPixelShader(void)
{
	return m_pPixelShader;
}

ID3D11VertexShader * CShader::createVertexShader(ID3D10Blob ** a_pOutVertexShaderCode)
{
	return CreateVertexShader(m_stParameters.m_oVertexShaderFilepath,
		a_pOutVertexShaderCode);
}

ID3D11PixelShader * CShader::createPixelShader(ID3D10Blob ** a_pOutPixelShaderCode)
{
	return CreatePixelShader(m_stParameters.m_oPixelShaderFilepath,
		a_pOutPixelShaderCode);
}
