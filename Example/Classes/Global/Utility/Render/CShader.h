#pragma once

#include "../../Define/KGlobalDefine.h"

//! 쉐이더
class CShader
{
public:

	//! 매개 변수
	struct STParameters
	{
		std::string m_oVertexShaderFilepath;
		std::string m_oPixelShaderFilepath;
	};

public:			// getter

	//! 정점 쉐이더 코드를 반환한다
	ID3D10Blob * getVertexShaderCode(void);

	//! 픽셀 쉐이더 코드를 반환한다
	ID3D10Blob * getPixelShaderCode(void);

	//! 정점 쉐이더를 반환한다
	ID3D11VertexShader * getVertexShader(void);

	//! 픽셀 쉐이더를 반환한다
	ID3D11PixelShader * getPixelShader(void);

public:			// 생성자, 소멸자

	//! 생성자
	CShader(const STParameters &a_rstParameters);

	//! 소멸자
	virtual ~CShader(void);

private:			// private 함수

	//! 정점 쉐이더를 생성한다
	ID3D11VertexShader * createVertexShader(ID3D10Blob **a_pOutVertexShaderCode);

	//! 픽셀 쉐이더를 생성한다
	ID3D11PixelShader * createPixelShader(ID3D10Blob **a_pOutPixelShaderCode);

private:			// private 변수

	STParameters m_stParameters;

	ID3D10Blob *m_pVertexShaderCode = nullptr;
	ID3D10Blob *m_pPixelShaderCode = nullptr;

	ID3D11VertexShader *m_pVertexShader = nullptr;
	ID3D11PixelShader *m_pPixelShader = nullptr;
};
