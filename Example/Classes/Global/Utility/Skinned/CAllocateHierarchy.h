#pragma once

#include "../../Define/KGlobalDefine.h"

class CMesh;

//! 계층 구조
class CAllocateHierarchy : public ID3DXAllocateHierarchy
{
public:

	//! 본
	struct STBone : D3DXFRAME
	{
		D3DXMATRIXA16 m_stCombineMatrix;
	};

	//! 메시 컨테이너
	struct STMeshContainer : D3DXMESHCONTAINER
	{
		DWORD m_nNumBlends;
		DWORD m_nNumBoneCombinations;

		CMesh *m_pSkinnedMesh;

		std::vector<STBone *> m_oBoneList;
		std::vector<D3DXMATRIXA16> m_oBoneMatrixList;
		std::vector<D3DXBONECOMBINATION> m_oBoneCombinationList;
		std::vector<ID3D11ShaderResourceView *> m_oDiffuseMapList;
	};

	//! 매개 변수
	struct STParameters 
	{
		std::string m_oBasePath;
		std::function<CMesh * (LPD3DXMESHCONTAINER, int)> m_oSkinnedMeshCreator;
	};

public:			// 인터페이스 구현
	
	//! 본을 생성한다
	virtual HRESULT CALLBACK CreateFrame(LPCSTR a_pszName,
		LPD3DXFRAME *a_pstOutFrame) override;

	//! 메시 컨테이너를 생성한다
	virtual HRESULT CALLBACK CreateMeshContainer(LPCSTR a_pszName,
		const D3DXMESHDATA *a_pstMeshData,
		const D3DXMATERIAL *a_pstXMaterials,
		const D3DXEFFECTINSTANCE *a_pstEffectInstances,
		DWORD a_nNumMaterials,
		const DWORD *a_pnAdjacency,
		LPD3DXSKININFO a_pSkinInfo,
		LPD3DXMESHCONTAINER *a_pstOutMeshContainer) override;

	//! 본을 제거한다
	virtual HRESULT CALLBACK DestroyFrame(LPD3DXFRAME a_pstFrame) override;

	//! 메시 컨테이너를 제거한다
	virtual HRESULT CALLBACK DestroyMeshContainer(LPD3DXMESHCONTAINER a_pstMeshContainer) override;

public:			// 생성자

	//! 생성자
	CAllocateHierarchy(void);

	//! 생성자
	CAllocateHierarchy(const STParameters &a_rstParameters);

private:			// private 변수

	int m_nNumMeshContainers = 0;
	STParameters m_stParameters;
};
