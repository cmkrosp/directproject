#include "CAnimationController.h"
#include "../Manager/CTimeManager.h"

CAnimationController::CAnimationController(LPD3DXANIMATIONCONTROLLER a_pAnimationController)
:
m_pAnimationController(a_pAnimationController)
{
	for (int i = 0; i < m_pAnimationController->GetNumAnimationSets(); ++i) {
		LPD3DXANIMATIONSET pAnimationSet = nullptr;
		m_pAnimationController->GetAnimationSet(i, &pAnimationSet);

		std::string oName = pAnimationSet->GetName();

		m_oAnimationNameList.push_back(oName);
		m_oAnimationSetList.insert(decltype(m_oAnimationSetList)::value_type(oName, pAnimationSet));
	}
}

CAnimationController::~CAnimationController(void)
{
	for (auto &rValueType : m_oAnimationSetList) {
		SAFE_RELEASE(rValueType.second);
	}

	SAFE_RELEASE(m_pAnimationController);
}

void CAnimationController::update(void)
{

	if (m_bIsPlaying) {
		float fDeltaTime = GET_DELTA_TIME() * m_fTimeScale;

		// 다음 애니메이션이 존재 할 경우
		if (m_pNextAnimationSet != nullptr) {
			m_fLeftBlendTime -= fDeltaTime;

			D3DXTRACK_DESC stTrackDesc;
			m_pAnimationController->GetTrackDesc(0, &stTrackDesc);

			double dblPercent = stTrackDesc.Position / m_pAnimationSet->GetPeriod();
			m_fDblPercent = dblPercent - (int)dblPercent;

			m_fLeftBlendTime -= fDeltaTime;
			// 애니메이션이 전환 중 일 경우
			if (m_fLeftBlendTime > 0.0f) {
				float fWeightA = m_fLeftBlendTime / m_fBlendTime;
				float fWeightB = 1.0f - fWeightA;

				m_pAnimationController->SetTrackWeight(0, fWeightA);
				m_pAnimationController->SetTrackWeight(1, fWeightB);
			}
			else {
				bool bIsLoop = m_bIsLoop;
				auto pAnimationSet = m_pNextAnimationSet;

				D3DXTRACK_DESC stTrackDesc;
				m_pAnimationController->GetTrackDesc(1, &stTrackDesc);

				//m_fDblPercent = 0.0f;
				this->stopAnimation();
				this->doPlayAnimation(pAnimationSet, bIsLoop);
				m_pAnimationController->SetTrackPosition(0, stTrackDesc.Position);

				m_pAnimationController->GetTrackDesc(0, &stTrackDesc);
				double dblPercent = stTrackDesc.Position / pAnimationSet->GetPeriod();
				m_fDblPercent = dblPercent - (int)dblPercent;

				m_pAnimationController->SetTrackAnimationSet(0, pAnimationSet);
			}

			m_pAnimationController->AdvanceTime(fDeltaTime, NULL);
		}
		else {
			D3DXTRACK_DESC stTrackDesc;
			m_pAnimationController->GetTrackDesc(0, &stTrackDesc);

			double dblPercent = stTrackDesc.Position / m_pAnimationSet->GetPeriod();

			m_fDblPercent = dblPercent - (int)dblPercent;
			// 애니메이션이 진행 중 일 경우
			if (m_bIsLoop || dblPercent < 1.0) {
				m_pAnimationController->AdvanceTime(fDeltaTime, NULL);
			}
			else {
				this->stopAnimation();
			}
		}
	}
}

std::vector<std::string>& CAnimationController::getAnimationNameList(void)
{
	return m_oAnimationNameList;
}

LPD3DXANIMATIONSET CAnimationController::getAnimationSet(int num)
{
	if (num == 0)
	{
		return m_pAnimationSet;
	}
	else
	{
		return m_pNextAnimationSet;
	}
}

double CAnimationController::getDblPercent(std::string name)
{
	if (m_pAnimationSet != NULL)
	{
		if (strcmp(m_pAnimationSet->GetName(), name.c_str()) == 0)
		{
			D3DXTRACK_DESC stTrackDesc;
			m_pAnimationController->GetTrackDesc(0, &stTrackDesc);

			double dblPercent = stTrackDesc.Position / m_pAnimationSet->GetPeriod();
			return dblPercent;
		}
	}

	if (m_pNextAnimationSet != NULL)
	{
		if (strcmp(m_pNextAnimationSet->GetName(), name.c_str()) == 0)
		{
			D3DXTRACK_DESC stTrackDesc;
			m_pAnimationController->GetTrackDesc(1, &stTrackDesc);

			double dblPercent = stTrackDesc.Position / m_pNextAnimationSet->GetPeriod();
			return dblPercent;
		}
		return 0.0f;
	}
	
	return 0.0f;
}

double CAnimationController::getDblPercent(void)
{
	return m_fDblPercent;
}

void CAnimationController::setTimeScale(float a_fTimeScale)
{
	m_fTimeScale = a_fTimeScale;
}

void CAnimationController::playAnimation(const std::string & a_rAnimationName, bool a_bIsLoop)
{
	auto oIterator = m_oAnimationSetList.find(a_rAnimationName);

	if (oIterator != m_oAnimationSetList.end()) {
		this->doPlayAnimation(oIterator->second, a_bIsLoop);
	}
}

void CAnimationController::stopAnimation(void)
{
	m_bIsLoop = false;
	m_bIsPlaying = false;
	m_pAnimationSet = nullptr;
	m_pNextAnimationSet = nullptr;

	m_pAnimationController->SetTrackPosition(0, 0.0f);
	m_pAnimationController->SetTrackAnimationSet(0, NULL);

	m_pAnimationController->SetTrackEnable(1, FALSE);
	m_pAnimationController->SetTrackPosition(1, 0.0f);
	m_pAnimationController->SetTrackAnimationSet(1, NULL);

	
}

void CAnimationController::playAnimationWithFrame(int a_nStartFrame, int a_nEndFrame, const std::string& a_rAnimationName, bool a_bIsLoop)
{
	auto oIterator = m_oAnimationSetList.find(a_rAnimationName);

	if (oIterator != m_oAnimationSetList.end()) {
		this->doPlayAnimationWithFrame(a_nStartFrame,a_nEndFrame,oIterator->second, a_bIsLoop);
	}
}

void CAnimationController::doPlayAnimationWithFrame(int a_nStartFrame, int a_nEndFrame, LPD3DXANIMATIONSET a_pAnimationSet, bool a_bIsLoop)
{
	
	m_pAnimationSet = a_pAnimationSet;
	//m_pAnimationController->SetTrackWeight(0, 1.0f);
	m_pAnimationController->SetTrackPosition(0, a_nStartFrame);
	m_pAnimationController->SetTrackAnimationSet(0, a_pAnimationSet);
	
	m_bIsLoop = a_bIsLoop;
	m_bIsPlaying = true;
}

void CAnimationController::clonAnimation(LPD3DXANIMATIONSET a_pAnimationSet)
{
	std::string oName = a_pAnimationSet->GetName();
	m_oAnimationNameList.push_back(oName);
	m_oAnimationSetList.insert(decltype(m_oAnimationSetList)::value_type(oName, a_pAnimationSet));

	m_pAnimationController->RegisterAnimationSet(a_pAnimationSet);
}

void CAnimationController::doPlayAnimation(LPD3DXANIMATIONSET a_pAnimationSet, bool a_bIsLoop)
{
	if (!m_bIsPlaying) {
		m_pAnimationSet = a_pAnimationSet;
		m_pNextAnimationSet = nullptr;

		m_pAnimationController->SetTrackWeight(0, 1.0f);
		m_pAnimationController->SetTrackPosition(0, 0.0f);
		m_pAnimationController->SetTrackAnimationSet(0, a_pAnimationSet);
	}
	else if (m_pNextAnimationSet != a_pAnimationSet) {
		m_fBlendTime = 0.15f;
		m_pNextAnimationSet = a_pAnimationSet;

		// 현재 애니메이션과 다를 경우
		if (m_pAnimationSet != a_pAnimationSet) {
			m_fLeftBlendTime = m_fBlendTime;

			m_pAnimationController->SetTrackWeight(1, 0.0f);
			m_pAnimationController->SetTrackPosition(1, 0.0f);
		}
		else {
			// 애니메이션이 전환 중 일 경우
			if (m_fLeftBlendTime > 0.0f) {
				m_pAnimationController->SetTrackPosition(1, 0.0f);
			}
			else {
				D3DXTRACK_DESC stTrackDesc;
				m_pAnimationController->GetTrackDesc(0, &stTrackDesc);

				m_pAnimationController->SetTrackPosition(1, stTrackDesc.Position);
			}

			m_fLeftBlendTime = 0.0f;
			m_pAnimationController->SetTrackWeight(1, 1.0f);
		}

		m_pAnimationController->SetTrackEnable(1, TRUE);
		m_pAnimationController->SetTrackAnimationSet(1, a_pAnimationSet);
	}

	m_bIsLoop = a_bIsLoop;
	m_bIsPlaying = true;
}
