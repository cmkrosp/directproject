#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"

//! 애니메이션 컨트롤러
class CAnimationController : public IUpdateable
{
public:			// 인터페이스 구현

	//! 상태를 갱신한다
	virtual void update(void) override;

public:			// getter, setter

	//! 애니메이션 이름 리스트를 반환한다
	std::vector<std::string> & getAnimationNameList(void);

	//! 애니메이션을 반환한다
	LPD3DXANIMATIONSET getAnimationSet(int num);

	double getDblPercent(std::string name);

	//! DBL Percent를 반환한다.
	double getDblPercent(void);

	//! 시간 비율을 변경한다
	void setTimeScale(float a_fTimeScale);

public:			// public 함수

	//! 애니메이션을 시작한다
	void playAnimation(const std::string &a_rAnimationName, bool a_bIsLoop = false);

	//! 애니메이션을 중지한다
	void stopAnimation(void);

	//! 프레임 구간을 기준으로 애니메이션을 작동한다
	void playAnimationWithFrame(int a_nStartFrame,
		int a_nEndFrame, const std::string& a_rAnimationName, bool a_bIsLoop = true);

	void doPlayAnimationWithFrame(int a_nStartFrame,int a_nEndFrame,LPD3DXANIMATIONSET a_pAnimationSet, bool a_bIsLoop);

	//! 애니메이션을 복제한다
	void clonAnimation(LPD3DXANIMATIONSET a_pAnimationSet);
	


public:			// 생성자, 소멸자

	//! 생성자
	CAnimationController(LPD3DXANIMATIONCONTROLLER a_pAnimationController);

	//! 소멸자
	virtual ~CAnimationController(void);

private:			// private 함수

	//! 애니메이션을 시작한다
	void doPlayAnimation(LPD3DXANIMATIONSET a_pAnimationSet, bool a_bIsLoop);

private:			// private 변수

	bool m_bIsLoop = false;
	bool m_bIsPlaying = false;

	float m_fTimeScale = 1.0f;
	float m_fBlendTime = 0.0f;
	float m_fLeftBlendTime = 0.0f;

	double m_fDblPercent = 0.0f;

	LPD3DXANIMATIONSET m_pAnimationSet = nullptr;
	LPD3DXANIMATIONSET m_pNextAnimationSet = nullptr;
	LPD3DXANIMATIONCONTROLLER m_pAnimationController = nullptr;

	std::vector<std::string> m_oAnimationNameList;
	std::unordered_map<std::string, LPD3DXANIMATIONSET> m_oAnimationSetList;
};
