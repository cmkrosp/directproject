#include "CSound.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CSoundManager.h"
#include "../Manager/CResourceManager.h"

CSound::CSound(const std::string & a_rFilepath)
{
	this->resetSound(a_rFilepath);
}

CSound::~CSound(void)
{
	SAFE_RELEASE(m_pSoundBuffer);
}

DWORD CSound::getStatus(void)
{
	DWORD nStatus = 0;
	m_pSoundBuffer->GetStatus(&nStatus);

	return nStatus;
}

void CSound::setVolume(float a_fVolume)
{
	a_fVolume = ClampValue<float>(a_fVolume, 0.0f, 1.0f);
	m_pSoundBuffer->SetVolume((1.0f - a_fVolume) * DSBVOLUME_MIN);
}

bool CSound::isPlaying(void)
{
	return this->getStatus() & DSBSTATUS_PLAYING;
}

void CSound::playSound(bool a_bIsLoop)
{
	if (m_pSoundBuffer != nullptr) {
		DWORD nFlags = a_bIsLoop ? DSBPLAY_LOOPING : 0;
		m_pSoundBuffer->Play(0, 0, nFlags);
	}
}

void CSound::stopSound(void)
{
	if (m_pSoundBuffer != nullptr) {
		m_pSoundBuffer->Stop();
	}
}

void CSound::resetSound(const std::string & a_rFilepath)
{
	this->stopSound();

	m_stWaveSound = GET_WAVE_SOUND(a_rFilepath);
	m_pSoundBuffer = this->createSoundBuffer();
}

LPDIRECTSOUNDBUFFER CSound::createSoundBuffer(void)
{
	// 사운드 버퍼를 생성한다
	// {
	DSBUFFERDESC stBufferDesc;
	ZeroMemory(&stBufferDesc, sizeof(stBufferDesc));

	stBufferDesc.dwSize = sizeof(stBufferDesc);
	stBufferDesc.dwBufferBytes = m_stWaveSound.m_nNumBytes;
	stBufferDesc.guid3DAlgorithm = GUID_NULL;
	stBufferDesc.lpwfxFormat = &m_stWaveSound.m_stWaveFormat;
	stBufferDesc.dwFlags = DSBCAPS_STATIC | DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLVOLUME;

	LPDIRECTSOUNDBUFFER pSoundBuffer = nullptr;
	GET_DIRECT_SOUND()->CreateSoundBuffer(&stBufferDesc, &pSoundBuffer, NULL);
	// }

	// 사운드 정보를 설정한다
	// {
	DWORD nNumBytesA = 0;
	DWORD nNumBytesB = 0;

	BYTE *pnBytesA = nullptr;
	BYTE *pnBytesB = nullptr;

	if (SUCCEEDED(pSoundBuffer->Lock(0, m_stWaveSound.m_nNumBytes,
		(void **)&pnBytesA, &nNumBytesA, (void **)&pnBytesB, &nNumBytesB, 0)))
	{
		CopyMemory(pnBytesA, 
			m_stWaveSound.m_pnBytes, m_stWaveSound.m_nNumBytes);

		pSoundBuffer->Unlock(NULL, 0, NULL, 0);
	}
	// }

	return pSoundBuffer;
}
