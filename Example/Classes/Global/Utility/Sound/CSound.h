#pragma once

#include "../../Define/KGlobalDefine.h"

//! 사운드
class CSound
{
public:			// getter, setter

	//! 상태를 반환한다
	DWORD getStatus(void);

	//! 볼륨을 변경한다
	void setVolume(float a_fVolume);

public:			// public 함수

	//! 재생 여부를 반환한다
	bool isPlaying(void);

	//! 사운드를 재생한다
	void playSound(bool a_bIsLoop);

	//! 사운드를 중지한다
	void stopSound(void);

	//! 사운드를 초기화한다
	void resetSound(const std::string &a_rFilepath);

public:			// 생성자, 소멸자

	//! 생성자
	CSound(const std::string &a_rFilepath);

	//! 소멸자
	virtual ~CSound(void);

private:			// private 함수

	//! 사운드 버퍼를 생성한다
	LPDIRECTSOUNDBUFFER createSoundBuffer(void);

private:			// private 변수

	STWaveSound m_stWaveSound;
	LPDIRECTSOUNDBUFFER m_pSoundBuffer = nullptr;
};
