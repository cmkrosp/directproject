#include "CArrayList.h"

template<typename T>
inline CArrayList<T>::CArrayList(int a_nSize)
:
m_nSize(a_nSize)
{
	m_ptValues = new T[a_nSize];
}

template<typename T>
inline CArrayList<T>::CArrayList(const CArrayList & a_rArrayList)
{
	*this = a_rArrayList;
}

template<typename T>
inline CArrayList<T>::~CArrayList(void)
{
	SAFE_DELETE_ARRAY(m_ptValues);
}

template<typename T>
inline void CArrayList<T>::operator=(const CArrayList & a_rArrayList)
{
	SAFE_DELETE_ARRAY(m_ptValues);

	m_nSize = a_rArrayList.m_nSize;
	m_nCount = a_rArrayList.m_nCount;
	m_ptValues = new T[m_nSize];

	for (int i = 0; i < m_nCount; ++i) {
		m_ptValues[i] = a_rArrayList.m_ptValues[i];
	}
}

template<typename T>
inline T & CArrayList<T>::operator[](int a_nIndex)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);
	return m_ptValues[a_nIndex];
}

template<typename T>
inline T CArrayList<T>::operator[](int a_nIndex) const
{
	return const_cast<CArrayList &>(*this)[a_nIndex];
}

template<typename T>
inline int CArrayList<T>::getSize(void) const
{
	return m_nSize;
}

template<typename T>
inline int CArrayList<T>::getCount(void) const
{
	return m_nCount;
}

template<typename T>
inline T CArrayList<T>::getValue(int a_nIndex) const
{
	return (*this)[a_nIndex];
}

template<typename T>
inline void CArrayList<T>::resize(int a_nSize)
{
	if (m_nSize != a_nSize) {
		int nNewCount = (m_nSize < a_nSize)? m_nCount : a_nSize;
		T *ptPrevValues = m_ptValues;

		m_ptValues = new T[a_nSize];

		for (int i = 0; i < nNewCount; ++i) {
			m_ptValues[i] = ptPrevValues[i];
		}

		m_nSize = a_nSize;
		m_nCount = nNewCount;

		SAFE_DELETE_ARRAY(ptPrevValues);
	}
}

template<typename T>
inline void CArrayList<T>::reserve(int a_nSize)
{
	if (m_nSize < a_nSize) {
		this->resize(a_nSize);
	}

	m_nCount = a_nSize;
}

template<typename T>
inline void CArrayList<T>::addValue(T a_tValue)
{
	// 추가 할 공간이 없을 경우
	if (m_nCount >= m_nSize) {
		this->resize(m_nSize * 2);

		//int nNewSize = m_nSize * 2;
		//T *ptPrevValues = m_ptValues;

		//m_ptValues = new T[nNewSize];

		//for (int i = 0; i < m_nCount; ++i) {
		//	m_ptValues[i] = ptPrevValues[i];
		//}

		//m_nSize = nNewSize;
		//SAFE_DELETE_ARRAY(ptPrevValues);
	}

	m_ptValues[m_nCount++] = a_tValue;
}

template<typename T>
inline void CArrayList<T>::insertValue(int a_nIndex, T a_tValue)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);

	// 추가 할 공간이 없을 경우
	if (m_nCount >= m_nSize) {
		this->resize(m_nSize * 2);

		//int nNewSize = m_nSize * 2;
		//T *ptPrevValues = m_ptValues;

		//m_ptValues = new T[nNewSize];

		//for (int i = 0; i < m_nCount; ++i) {
		//	m_ptValues[i] = ptPrevValues[i];
		//}

		//m_nSize = nNewSize;
		//SAFE_DELETE_ARRAY(ptPrevValues);
	}

	for (int i = m_nCount; i > a_nIndex; --i) {
		m_ptValues[i] = m_ptValues[i - 1];
	}

	m_nCount += 1;
	m_ptValues[a_nIndex] = a_tValue;
}

template<typename T>
inline void CArrayList<T>::replaceValue(int a_nIndex, T a_tValue)
{
	(*this)[a_nIndex] = a_tValue;
}

template<typename T>
inline void CArrayList<T>::removeValue(T a_tValue)
{
	for (int i = 0; i < m_nCount; ++i) {
		if (m_ptValues[i] == a_tValue) {
			this->removeValueByIndex(i);
			break;
		}
	}
}

template<typename T>
inline void CArrayList<T>::removeValueByIndex(int a_nIndex)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);

	for (int i = a_nIndex; i < m_nCount - 1; ++i) {
		m_ptValues[i] = m_ptValues[i + 1];
	}

	m_nCount -= 1;
}

template<typename T>
inline bool CArrayList<T>::isValidIndex(int a_nIndex)
{
	return a_nIndex >= 0 && a_nIndex < m_nCount;
}
