#include "CBinaryTree.h"

template<typename K, typename V>
inline CBinaryTree<K, V>::CBinaryTree(void)
{
	// Do Nothing
}

template<typename K, typename V>
inline CBinaryTree<K, V>::CBinaryTree(const CBinaryTree & a_rBinaryTree)
{
	*this = a_rBinaryTree;
}

template<typename K, typename V>
inline CBinaryTree<K, V>::~CBinaryTree(void)
{
	this->clear();
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::operator=(const CBinaryTree & a_rBinaryTree)
{
	this->clear();

	const_cast<CBinaryTree &>(a_rBinaryTree).enumerateBinaryTree(EEnumerateType::PRE_ORDER, 
		[=](STNode *a_pstNode) -> void 
	{
		this->addValue(a_pstNode->m_tKey, a_pstNode->m_tValue);
	});
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::clear(void)
{
	this->enumerateBinaryTree(EEnumerateType::POST_ORDER,
		[=](STNode *a_pstNode) -> void
	{
		SAFE_DELETE(a_pstNode);
	});

	m_pstRootNode = nullptr;
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::addValue(K a_tKey, V a_tValue)
{
	auto pstCurrentNode = &m_pstRootNode;

	while (*pstCurrentNode != nullptr) {
		if (a_tKey < (*pstCurrentNode)->m_tKey) {
			pstCurrentNode = &(*pstCurrentNode)->m_pstLChildNode;
		}
		else {
			pstCurrentNode = &(*pstCurrentNode)->m_pstRChildNode;
		}
	}

	auto pstNode = this->createNode(a_tKey, a_tValue);
	*pstCurrentNode = pstNode;
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::removeValue(K a_tKey)
{
	auto pstCurrentNode = &m_pstRootNode;

	while (*pstCurrentNode != nullptr &&
		(*pstCurrentNode)->m_tKey != a_tKey)
	{
		if (a_tKey < (*pstCurrentNode)->m_tKey) {
			pstCurrentNode = &(*pstCurrentNode)->m_pstLChildNode;
		}
		else {
			pstCurrentNode = &(*pstCurrentNode)->m_pstRChildNode;
		}
	}

	if (*pstCurrentNode != nullptr) {
		// 자식이 두개 일 경우
		if ((*pstCurrentNode)->m_pstLChildNode != nullptr &&
			(*pstCurrentNode)->m_pstRChildNode != nullptr)
		{
			auto pstRemoveNode = &(*pstCurrentNode)->m_pstLChildNode;

			while ((*pstRemoveNode)->m_pstRChildNode != nullptr) {
				pstRemoveNode = &(*pstRemoveNode)->m_pstRChildNode;
			}

			(*pstCurrentNode)->m_tKey = (*pstRemoveNode)->m_tKey;
			(*pstCurrentNode)->m_tValue = (*pstRemoveNode)->m_tValue;

			pstCurrentNode = pstRemoveNode;
		}

		auto pstRemoveNode = *pstCurrentNode;

		// 왼쪽 자식이 있을 경우
		if (pstRemoveNode->m_pstLChildNode != nullptr) {
			*pstCurrentNode = pstRemoveNode->m_pstLChildNode;
		}
		else {
			*pstCurrentNode = pstRemoveNode->m_pstRChildNode;
		}

		SAFE_DELETE(pstRemoveNode);
	}
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::enumerateBinaryTree(EEnumerateType a_eEnumerateType, const std::function<void(STNode*)>& a_rCallback)
{
	switch (a_eEnumerateType) {
	case PRE_ORDER:		this->enumerateByPreOrder(m_pstRootNode, a_rCallback);	break;
	case IN_ORDER:		this->enumerateByInOrder(m_pstRootNode, a_rCallback);		break;
	case POST_ORDER:	this->enumerateByPostOrder(m_pstRootNode, a_rCallback);	break;
	case LEVEL_ORDER:	this->enumerateByLevelOrder(m_pstRootNode, a_rCallback);	break;
	}
}

template<typename K, typename V>
inline typename CBinaryTree<K, V>::STNode * CBinaryTree<K, V>::createNode(K a_tKey, V a_tValue)
{
	auto pstNode = new STNode();
	pstNode->m_tKey = a_tKey;
	pstNode->m_tValue = a_tValue;
	pstNode->m_pstLChildNode = nullptr;
	pstNode->m_pstRChildNode = nullptr;

	return pstNode;
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::enumerateByPreOrder(STNode *a_pstNode, const std::function<void(STNode*)>& a_rCallback)
{
	if (a_pstNode != nullptr) {
		a_rCallback(a_pstNode);

		this->enumerateByPreOrder(a_pstNode->m_pstLChildNode, a_rCallback);
		this->enumerateByPreOrder(a_pstNode->m_pstRChildNode, a_rCallback);
	}
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::enumerateByInOrder(STNode *a_pstNode, const std::function<void(STNode*)>& a_rCallback)
{
	if (a_pstNode != nullptr) {
		this->enumerateByInOrder(a_pstNode->m_pstLChildNode, a_rCallback);

		a_rCallback(a_pstNode);
		this->enumerateByInOrder(a_pstNode->m_pstRChildNode, a_rCallback);
	}
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::enumerateByPostOrder(STNode *a_pstNode, const std::function<void(STNode*)>& a_rCallback)
{
	if (a_pstNode != nullptr) {
		this->enumerateByPostOrder(a_pstNode->m_pstLChildNode, a_rCallback);
		this->enumerateByPostOrder(a_pstNode->m_pstRChildNode, a_rCallback);

		a_rCallback(a_pstNode);
	}
}

template<typename K, typename V>
inline void CBinaryTree<K, V>::enumerateByLevelOrder(STNode *a_pstNode, const std::function<void(STNode*)>& a_rCallback)
{
	std::queue<STNode *> oNodeList;
	oNodeList.push(a_pstNode);

	while (!oNodeList.empty()) {
		auto pstNode = oNodeList.front();
		oNodeList.pop();

		if (pstNode != nullptr) {
			a_rCallback(pstNode);

			if (pstNode->m_pstLChildNode != nullptr) {
				oNodeList.push(pstNode->m_pstLChildNode);
			}

			if (pstNode->m_pstRChildNode != nullptr) {
				oNodeList.push(pstNode->m_pstRChildNode);
			}
		}
	}
}
