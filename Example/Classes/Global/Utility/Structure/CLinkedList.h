#pragma once

#include "../../Define/KGlobalDefine.h"

//! 연결 리스트
template <typename T>
class CLinkedList
{
public:

	//! 노드
	struct STNode
	{
		T m_tValue;
		STNode *m_pstNextNode;
	};

public:			// operator

	//! operator =
	void operator=(const CLinkedList &a_rLinkedList);

	//! operator []
	T & operator[](int a_nIndex);

	//! operator []
	T operator[](int a_nIndex) const;

public:			// getter

	//! 개수를 반환한다
	int getCount(void) const;

	//! 값을 반환한다
	T getValue(int a_nIndex) const;

public:			// public 함수

	//! 데이터를 제거한다
	void clear(void);

	//! 값을 추가한다
	void addValue(T a_tValue);

	//! 값을 추가한다
	void insertValue(int a_nIndex, T a_tValue);

	//! 값을 대치한다
	void replaceValue(int a_nIndex, T a_tValue);

	//! 값을 제거한다
	void removeValue(T a_tValue);

	//! 값을 제거한다
	void removeValueByIndex(int a_nIndex);

public:			// 생성자, 소멸자

	//! 생성자
	CLinkedList(void);

	//! 복사 생성자
	CLinkedList(const CLinkedList &a_rLinkedList);

	//! 소멸자
	virtual ~CLinkedList(void);
	
private:			// private 함수

	//! 노드를 생성한다
	STNode * createNode(T a_tValue);

private:			// private 변수

	int m_nCount = 0;
	STNode *m_pstHeadNode = nullptr;
};

#include "CLinkedList.inl"
