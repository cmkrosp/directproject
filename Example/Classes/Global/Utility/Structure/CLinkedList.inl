#include "CLinkedList.h"

template<typename T>
inline CLinkedList<T>::CLinkedList(void)
{
	m_pstHeadNode = new STNode();
	m_pstHeadNode->m_pstNextNode = nullptr;
}

template<typename T>
inline CLinkedList<T>::CLinkedList(const CLinkedList & a_rLinkedList)
:
CLinkedList()
{
	*this = a_rLinkedList;
}

template<typename T>
inline CLinkedList<T>::~CLinkedList(void)
{
	this->clear();
	SAFE_DELETE(m_pstHeadNode);
}

template<typename T>
inline void CLinkedList<T>::operator=(const CLinkedList & a_rLinkedList)
{
	this->clear();

	for (int i = 0; i < a_rLinkedList.getCount(); ++i) {
		this->addValue(a_rLinkedList.getValue(i));
	}
}

template<typename T>
inline T & CLinkedList<T>::operator[](int a_nIndex)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);
	auto pstCurrentNode = m_pstHeadNode->m_pstNextNode;

	for (int i = 0; i < a_nIndex; ++i) {
		pstCurrentNode = pstCurrentNode->m_pstNextNode;
	}

	return pstCurrentNode->m_tValue;
}

template<typename T>
inline T CLinkedList<T>::operator[](int a_nIndex) const
{
	return const_cast<CLinkedList &>(*this)[a_nIndex];
}

template<typename T>
inline int CLinkedList<T>::getCount(void) const
{
	return m_nCount;
}

template<typename T>
inline T CLinkedList<T>::getValue(int a_nIndex) const
{
	return (*this)[a_nIndex];
}

template<typename T>
inline void CLinkedList<T>::clear(void)
{
	m_nCount = 0;
	auto pstCurrentNode = m_pstHeadNode->m_pstNextNode;

	while (pstCurrentNode != nullptr) {
		auto pstRemoveNode = pstCurrentNode;
		pstCurrentNode = pstCurrentNode->m_pstNextNode;

		SAFE_DELETE(pstRemoveNode);
	}
}

template<typename T>
inline void CLinkedList<T>::addValue(T a_tValue)
{
	auto pstNode = this->createNode(a_tValue);
	auto pstCurrentNode = m_pstHeadNode;

	while (pstCurrentNode->m_pstNextNode != nullptr) {
		pstCurrentNode = pstCurrentNode->m_pstNextNode;
	}

	m_nCount += 1;
	pstCurrentNode->m_pstNextNode = pstNode;
}

template<typename T>
inline void CLinkedList<T>::insertValue(int a_nIndex, T a_tValue)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);
	auto pstCurrentNode = m_pstHeadNode;

	for (int i = 0; i < a_nIndex; ++i) {
		pstCurrentNode = pstCurrentNode->m_pstNextNode;
	}

	m_nCount += 1;
	auto pstNode = this->createNode(a_tValue);

	pstNode->m_pstNextNode = pstCurrentNode->m_pstNextNode;
	pstCurrentNode->m_pstNextNode = pstNode;
}

template<typename T>
inline void CLinkedList<T>::replaceValue(int a_nIndex, T a_tValue)
{
	(*this)[a_nIndex] = a_tValue;
}

template<typename T>
inline void CLinkedList<T>::removeValue(T a_tValue)
{
	int nIndex = 0;
	auto pstCurrentNode = m_pstHeadNode;

	while (pstCurrentNode->m_pstNextNode != nullptr &&
		pstCurrentNode->m_pstNextNode->m_tValue != a_tValue)
	{
		nIndex += 1;
		pstCurrentNode = pstCurrentNode->m_pstNextNode;
	}

	// 제거 할 노드를 탐색했을 경우
	if (nIndex < m_nCount) {
		this->removeValueByIndex(nIndex);
	}
}

template<typename T>
inline void CLinkedList<T>::removeValueByIndex(int a_nIndex)
{
	assert(a_nIndex >= 0 && a_nIndex < m_nCount);
	auto pstCurrentNode = m_pstHeadNode;

	for (int i = 0; i < a_nIndex; ++i) {
		pstCurrentNode = pstCurrentNode->m_pstNextNode;
	}

	// 제거 할 노드를 탐색했을 경우
	if (pstCurrentNode->m_pstNextNode != nullptr) {
		auto pstRemoveNode = pstCurrentNode->m_pstNextNode;
		pstCurrentNode->m_pstNextNode = pstRemoveNode->m_pstNextNode;

		m_nCount -= 1;
		SAFE_DELETE(pstRemoveNode);
	}
}

template<typename T>
inline typename CLinkedList<T>::STNode * CLinkedList<T>::createNode(T a_tValue)
{
	auto pstNode = new STNode();
	pstNode->m_tValue = a_tValue;
	pstNode->m_pstNextNode = nullptr;

	return pstNode;
}
