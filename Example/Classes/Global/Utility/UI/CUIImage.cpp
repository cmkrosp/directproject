#include "CUIImage.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Manager/CWindowManager.h"
#include "../Render/CShader.h"
#include "../Object/CCamera.h"
#include "../Base/CDirect3DApplication.h"


CUIImage::CUIImage(std::string a_oImageFile)
	: m_pVertexBuffer(nullptr)
	, m_pTransformBuffer(nullptr)
	, m_oImageFile(a_oImageFile)
	, m_bIsHide(false)
{
	D3D11_DEPTH_STENCIL_DESC stDepthStencilDesc = { 0 };
	stDepthStencilDesc.DepthEnable = true;
	stDepthStencilDesc.StencilEnable = true;
	GET_DEVICE()->CreateDepthStencilState(&stDepthStencilDesc, &m_pDepthStencilState);

	D3D11_DEPTH_STENCIL_DESC stDepthDisabledStencilDesc = { 0 };
	stDepthDisabledStencilDesc.DepthEnable = false;
	stDepthDisabledStencilDesc.StencilEnable = false;
	GET_DEVICE()->CreateDepthStencilState(&stDepthDisabledStencilDesc, &m_pDepthDisabledStencilState);

	D3DX11_IMAGE_INFO stII;
	D3DX11GetImageInfoFromFileA(a_oImageFile.c_str(), NULL, &stII, NULL);

	m_stImageSize.x = stII.Width;
	m_stImageSize.y = stII.Height;

	// Create Vertex Buffer
	{
		std::vector<STTextureVertex> vecVertex(6);
		vecVertex[0].m_stPosition = D3DXVECTOR3(-0.5f, -0.5f, 0.0f);
		vecVertex[1].m_stPosition = D3DXVECTOR3(-0.5f, 0.5f, 0.0f);
		vecVertex[2].m_stPosition = D3DXVECTOR3(0.5f, -0.5f, 0.0f);
		vecVertex[3].m_stPosition = D3DXVECTOR3(0.5f, -0.5f, 0.0f);
		vecVertex[4].m_stPosition = D3DXVECTOR3(-0.5f, 0.5f, 0.0f);
		vecVertex[5].m_stPosition = D3DXVECTOR3(0.5f, 0.5f, 0.0f);

		vecVertex[0].m_stUV = D3DXVECTOR2(0.0f, 1.0f);
		vecVertex[1].m_stUV = D3DXVECTOR2(0.0f, 0.0f);
		vecVertex[2].m_stUV = D3DXVECTOR2(1.0f, 1.0f);
		vecVertex[3].m_stUV = D3DXVECTOR2(1.0f, 1.0f);
		vecVertex[4].m_stUV = D3DXVECTOR2(0.0f, 0.0f);
		vecVertex[5].m_stUV = D3DXVECTOR2(1.0f, 0.0f);

		D3D11_BUFFER_DESC stBufferDesc;
		ZeroMemory(&stBufferDesc, sizeof(stBufferDesc));

		stBufferDesc.ByteWidth = sizeof(STTextureVertex) * 6;
		stBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		stBufferDesc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA stData = { 0 };
		stData.pSysMem = &vecVertex[0];

		GET_DEVICE()->CreateBuffer(&stBufferDesc, &stData, &m_pVertexBuffer);
	}

	//Create Transform Buffer
	{
		D3D11_BUFFER_DESC desc = { 0 };
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(TransformBufferDesc);
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		HRESULT hr = GET_DEVICE()->CreateBuffer(&desc, NULL, &m_pTransformBuffer);
		assert(SUCCEEDED(hr));
	}
}

CUIImage::~CUIImage(void)
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pTransformBuffer);

	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthDisabledStencilState);
}

void CUIImage::update(void)
{
	CUIObject::update();

	SIZE s = GET_WINDOW_SIZE();

	D3DXVECTOR2 stPosition = m_stWorldPosition;
	stPosition.x = (((stPosition.x + m_stScale.x / 2.0f) / (float)s.cx) - 0.5f) * 2.0f;
	stPosition.y = (((stPosition.y + m_stScale.y / 2.0f) / (float)s.cy) - 0.5f) * 2.0f;

	D3DXMATRIXA16 matT;
	D3DXMatrixTranslation(&matT, stPosition.x, stPosition.y, 0);

	D3DXVECTOR2 stSize = m_stScale;
	stSize.x = (stSize.x * 2.0f / (float)s.cx);
	stSize.y = (stSize.y * 2.0f / (float)s.cy);

	D3DXMATRIXA16 matS;
	D3DXMatrixScaling(&matS, stSize.x, stSize.y, 1);

	D3DXMATRIXA16 matR;
	D3DXMatrixRotationAxis(&matR,
		&WORLD_FORWARD_DIRECTION, D3DXToRadian(m_fAngle));

	m_stTransformBufferDesc.stMatrix = matS * matR * matT;
}

void CUIImage::render(void)
{
	if (m_bIsHide)
	{
		return;
	}

	UINT nStride = sizeof(STTextureVertex);
	UINT nOffset = 0;
	auto srv = GET_SHADER_RESOURCE_VIEW(m_oImageFile.c_str());
	GET_CONTEXT()->PSSetShaderResources(0, 1, &srv);
	GET_CONTEXT()->UpdateSubresource(m_pTransformBuffer, 0, NULL, &m_stTransformBufferDesc, 0, 0);
	GET_CONTEXT()->VSSetConstantBuffers(0, 1, &m_pTransformBuffer);
	GET_CONTEXT()->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &nStride, &nOffset);
	GET_CONTEXT()->VSSetShader(GET_VERTEX_SHADER(KEY_TEXTURE_SHADER), NULL, 0);
	GET_CONTEXT()->PSSetShader(GET_PIXEL_SHADER(KEY_TEXTURE_SHADER), NULL, 0);
	GET_CONTEXT()->IASetInputLayout(GET_INPUT_LAYOUT(KEY_TEXTURE_INPUT_LAYOUT));
	GET_CONTEXT()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	GET_CONTEXT()->OMSetDepthStencilState(m_pDepthDisabledStencilState, 0xFF);

	auto pBlendState = GET_BLEND_STATE(KEY_ALPHA_BLEND_ENABLE_STATE);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);

	// 사각형을 그린다
	GET_CONTEXT()->Draw(6, 0);

	GET_CONTEXT()->OMSetDepthStencilState(m_pDepthStencilState, 0xFF);

	pBlendState = GET_BLEND_STATE(KEY_ALPHA_BLEND_DISABLE_STATE);
	GET_CONTEXT()->OMSetBlendState(pBlendState, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f), UINT_MAX);
	
	CUIObject::render();
}
