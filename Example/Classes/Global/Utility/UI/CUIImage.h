#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Interface/IUpdateable.h"
#include "../Base/CUIObject.h"

class CUIImage : public CUIObject
{
public:
	virtual void update(void) override;
	virtual void render(void) override;

public:

	CUIImage(std::string a_oImageFile);
	virtual ~CUIImage(void);

protected:
	CC_SYNTHESIZE(std::string, m_oImageFile, ImageFile);
	CC_SYNTHESIZE(D3DXVECTOR2, m_stImageSize, ImageSize);
	CC_SYNTHESIZE(float, m_fAngle, Angle);
	CC_SYNTHESIZE(bool, m_bIsHide, IsHide);

protected:
	struct TransformBufferDesc
	{
		D3DXMATRIX stMatrix;
	};

	ID3D11DepthStencilState* m_pDepthStencilState;
	ID3D11DepthStencilState* m_pDepthDisabledStencilState;

	ID3D11Buffer*			m_pVertexBuffer;
	ID3D11Buffer*			m_pTransformBuffer;
	TransformBufferDesc		m_stTransformBufferDesc;
};
