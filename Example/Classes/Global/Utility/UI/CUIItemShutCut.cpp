#include "CUIItemShutCut.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CWindowManager.h"
#include "../Manager/CInputManager.h"
#include "../../../Item/CItem.h"
#include "CUIImage.h"


CUIItemShutCut::CUIItemShutCut(void)
	:
	CUIObject()
{
	CUIImage* pItemShotCut = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut.png");
	pItemShotCut->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));
	pItemShotCut->setScale(pItemShotCut->getImageSize() * 2.0f);
	pItemShotCut->setTag(TAG_SHUTCUTMAIN);

	this->addChildObject(pItemShotCut);

	CUIImage* pItemShotCutArrowL = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut_Arrow.png");
	pItemShotCutArrowL->setLocalPosition(D3DXVECTOR2(-25.0f, 63.0f));
	pItemShotCutArrowL->setScale(pItemShotCutArrowL->getImageSize() * 2.0f);
	pItemShotCutArrowL->setTag(TAG_SHUTCUTLARROW);

	pItemShotCut->addChildObject(pItemShotCutArrowL);

	CUIImage* pItemShotCutArrowR = Create<CUIImage>("Resources/Textures/UI/cock00_d_itemshotcut_Arrow.png");
	pItemShotCutArrowR->setAngle(180.0f);
	pItemShotCutArrowR->setLocalPosition(D3DXVECTOR2(125.0f, 63.0f));
	pItemShotCutArrowR->setScale(pItemShotCutArrowR->getImageSize() * 2.0f);
	pItemShotCutArrowR->setTag(TAG_SHUTCUTRARROW);

	pItemShotCut->addChildObject(pItemShotCutArrowR);

	m_oItemList.clear();
	m_oItemList.push_back(Create<CItem>("Resources/Textures/UI/icon_hpposion.png",
		CItem::STParameters{ CItem::EITEMTYPE::HPPOTION, 40 }));
	m_oItemList.push_back(Create<CItem>("Resources/Textures/UI/icon_meat.png",
		CItem::STParameters{ CItem::EITEMTYPE::MEAT, 25 }));
	m_oItemList.push_back(Create<CItem>("Resources/Textures/UI/icon_cureposion.png",
		CItem::STParameters{ CItem::EITEMTYPE::CUREPOTION, 0 }));
	m_oItemList.push_back(Create<CItem>("Resources/Textures/UI/icon_flashbomb.png",
		CItem::STParameters{ CItem::EITEMTYPE::FLASHBOMB, 0 }));
	m_oItemList.push_back(Create<CItem>("Resources/Textures/UI/icon_rare.png",
		CItem::STParameters{ CItem::EITEMTYPE::RARE, 0 }));

	for (auto item : m_oItemList)
	{
		item->setScale(item->getImageSize() * 1.2f);
		item->setLocalPosition(D3DXVECTOR2(78.0f - (item->getScale().x / 2.0f),
			88.0f - (item->getScale().y / 2.0f)));

		item->setTag(TAG_ITEMICON);
	}

	D3DXVECTOR2 stPosition = D3DXVECTOR2(-77.0f, -105.0f);

	auto pItemTag = Create<CUIImage>("Resources/Textures/UI/itemtag_posion.png");
	pItemTag->setScale(pItemTag->getImageSize());
	pItemTag->setLocalPosition(stPosition + (m_oItemList[0]->getScale() / 2.0f));

	m_oItemList[0]->addChildObject(pItemTag);

	pItemTag = Create<CUIImage>("Resources/Textures/UI/itemtag_meat.png");
	pItemTag->setScale(pItemTag->getImageSize());
	pItemTag->setLocalPosition(stPosition + (m_oItemList[1]->getScale() / 2.0f));

	m_oItemList[1]->addChildObject(pItemTag);

	pItemTag = Create<CUIImage>("Resources/Textures/UI/itemtag_cureposion.png");
	pItemTag->setScale(pItemTag->getImageSize());
	pItemTag->setLocalPosition(stPosition + (m_oItemList[2]->getScale() / 2.0f));

	m_oItemList[2]->addChildObject(pItemTag);

	pItemTag = Create<CUIImage>("Resources/Textures/UI/itemtag_flashbomb.png");
	pItemTag->setScale(pItemTag->getImageSize());
	pItemTag->setLocalPosition(stPosition + (m_oItemList[3]->getScale() / 2.0f));

	m_oItemList[3]->addChildObject(pItemTag);

	pItemTag = Create<CUIImage>("Resources/Textures/UI/itemtag_rare.png");
	pItemTag->setScale(pItemTag->getImageSize());
	pItemTag->setLocalPosition(stPosition + (m_oItemList[4]->getScale() / 2.0f));

	m_oItemList[4]->addChildObject(pItemTag);

	m_nItemIndex = 0;

	pItemShotCut->addChildObject(m_oItemList[m_nItemIndex]);
}

CUIItemShutCut::~CUIItemShutCut(void)
{
}

void CUIItemShutCut::itemReplacement(void)
{
	auto pItemShotCut = this->findChildObjectbyTag(TAG_SHUTCUTMAIN);
	pItemShotCut->removeChildObjectbyTag(TAG_ITEMICON);

	m_nItemIndex += 1;

	if (m_oItemList.size() == m_nItemIndex)
	{
		m_nItemIndex = 0;
	}

	pItemShotCut->addChildObject(m_oItemList[m_nItemIndex]);
}

CItem* CUIItemShutCut::getCurrentItem(void)
{
	return m_oItemList[m_nItemIndex];
}
