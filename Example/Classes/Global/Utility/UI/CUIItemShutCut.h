#pragma once

#include "../../Define/KGlobalDefine.h"
#include "../Base/CUIObject.h"

class CItem;

class CUIItemShutCut : public CUIObject
{
public:

	enum EITEMMENUTAG
	{
		TAG_SHUTCUTMAIN = 10,
		TAG_SHUTCUTRARROW,
		TAG_SHUTCUTLARROW,
		TAG_SHUTCUTL,
		TAG_SHUTCUTRR,
		TAG_SHUTCUTLL,
		TAG_SHUTCUTR,
		TAG_ITEMICON,
	};

public:

	CItem* getCurrentItem(void);

	void itemReplacement(void);

public:
	CUIItemShutCut(void);
	virtual ~CUIItemShutCut(void);

private:

	int m_nItemIndex = 0;

	std::vector<CItem*> m_oItemList;
};
