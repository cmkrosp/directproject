#include "CUIProgressBar.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "CUIImage.h"


CUIProgressBar::CUIProgressBar(EProgressBarType a_eProgressBarType, int a_nMaxValue)
	:
	m_eProgressBarType(a_eProgressBarType),
	m_nMaxValue((a_nMaxValue / PROGRESSBARMIN) * PROGRESSBARMIN),
	m_nCurrentValue(m_nMaxValue),
	m_bIsViewEnable(false)
{
	switch (a_eProgressBarType)
	{
	case CUIProgressBar::EProgressBarType::HPBar:
		setupImagepath("HP");
		break;
	case CUIProgressBar::EProgressBarType::StaminaBar:
		setupImagepath("Stamina");
		break;
	}
}

CUIProgressBar::~CUIProgressBar(void)
{
}

void CUIProgressBar::update(void)
{
	if (m_bIsEmitEnable)
	{
		CUIObject::update();

		m_bIsEmitEnable = false;
	}
}

void CUIProgressBar::setupImagepath(const std::string a_oImagepath)
{
	m_oImageFilepath[0] = "Resources/Textures/UI/cock00_d_" + a_oImagepath + "_Start.png";
	m_oImageFilepath[1] = "Resources/Textures/UI/cock00_d_" + a_oImagepath + "_Bar.png";
	m_oImageFilepath[2] = "Resources/Textures/UI/cock00_d_" + a_oImagepath + "_End.png";
	m_oImageFilepath[3] = "Resources/Textures/UI/cock00_d_" + a_oImagepath + "_ProgressBar.png";

	m_pStartImage = Create<CUIImage>(m_oImageFilepath[0]);
	m_pStartImage->setScale(m_pStartImage->getImageSize());
	m_pStartImage->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));

	this->addChildObject(m_pStartImage);

	auto pBarImage = Create<CUIImage>(m_oImageFilepath[1]);
	pBarImage->setScale(pBarImage->getImageSize());
	pBarImage->setLocalPosition(
		m_pStartImage->getLocalPosition() +
		D3DXVECTOR2(m_pStartImage->getImageSize().x, 0.0f)
	);

	m_fBarLength += pBarImage->getScale().x;

	m_oBarImageList.push_back(pBarImage);

	this->addChildObject(pBarImage);

	for (int i = 1; i < m_nMaxValue / PROGRESSBARMIN; ++i)
	{
		auto pBarImage = Create<CUIImage>(m_oImageFilepath[1]);
		pBarImage->setScale(pBarImage->getImageSize());
		pBarImage->setLocalPosition(
			m_oBarImageList.back()->getLocalPosition() +
			D3DXVECTOR2(m_oBarImageList.back()->getImageSize().x, 0.0f)
		);
		
		m_fBarLength += pBarImage->getScale().x;

		m_oBarImageList.push_back(pBarImage);

		this->addChildObject(pBarImage);
	}

	m_pEndImage = Create<CUIImage>(m_oImageFilepath[2]);
	m_pEndImage->setScale(m_pEndImage->getImageSize());
	m_pEndImage->setLocalPosition(
		m_oBarImageList.back()->getLocalPosition() +
		D3DXVECTOR2(m_oBarImageList.back()->getImageSize().x, 0.0f)
	);

	this->addChildObject(m_pEndImage);

	float stScaleY = 0.0f;

	switch (m_eProgressBarType)
	{
	case CUIProgressBar::EProgressBarType::HPBar:
		stScaleY = 6.0f;
		break;
	case CUIProgressBar::EProgressBarType::StaminaBar:
		stScaleY = 4.0f;
		break;
	}

	m_pProgressBarImage = Create<CUIImage>(m_oImageFilepath[3]);
	m_pProgressBarImage->setScale(D3DXVECTOR2(
		(m_oBarImageList[0]->getScale().x / PROGRESSBARMIN) *
		((float)m_nCurrentValue / m_nMaxValue), stScaleY
	));
	m_pProgressBarImage->setLocalPosition(
		m_oBarImageList[0]->getLocalPosition() + D3DXVECTOR2(0.0f, 3.0f));

	this->addChildObject(m_pProgressBarImage);
}

void CUIProgressBar::setCurrentValue(int a_nCurrentValue)
{
	if (m_nCurrentValue != a_nCurrentValue)
	{
		m_bIsEmitEnable = true;

		if (a_nCurrentValue < 0)
		{
			m_nCurrentValue = 0;
		}
		else if (a_nCurrentValue > m_nMaxValue)
		{
			m_nCurrentValue = m_nMaxValue;
		}
		else
		{
			m_nCurrentValue = a_nCurrentValue;
		}

		m_pProgressBarImage->setScale(D3DXVECTOR2(
			m_fBarLength * ((float)m_nCurrentValue / m_nMaxValue),
			m_pProgressBarImage->getScale().y
		));
	}
}

void CUIProgressBar::setMaxValue(int a_nMaxValue)
{
	if (m_nMaxValue != a_nMaxValue)
	{
		m_bIsEmitEnable = true;

		m_nMaxValue = a_nMaxValue;
		m_fBarLength = 0.0f;
		
		for (int i = 0; i < m_oBarImageList.size(); ++i)
		{
			this->removeChildObject(m_oBarImageList[i]);
			SAFE_DELETE(m_oBarImageList[i]);
		}

		m_oBarImageList.clear();

		this->removeChildObject(m_pProgressBarImage);
		SAFE_DELETE(m_pProgressBarImage);

		auto pBarImage = Create<CUIImage>(m_oImageFilepath[1]);
		pBarImage->setScale(pBarImage->getImageSize());
		pBarImage->setLocalPosition(
			m_pStartImage->getLocalPosition() +
			D3DXVECTOR2(m_pStartImage->getImageSize().x, 0.0f)
		);

		m_fBarLength += pBarImage->getScale().x;

		m_oBarImageList.push_back(pBarImage);

		this->addChildObject(pBarImage);

		for (int i = 1; i < m_nMaxValue / PROGRESSBARMIN; ++i)
		{
			auto pBarImage = Create<CUIImage>(m_oImageFilepath[1]);
			pBarImage->setScale(pBarImage->getImageSize());
			pBarImage->setLocalPosition(
				m_oBarImageList.back()->getLocalPosition() +
				D3DXVECTOR2(m_oBarImageList.back()->getImageSize().x, 0.0f)
			);

			m_fBarLength += pBarImage->getScale().x;

			m_oBarImageList.push_back(pBarImage);

			this->addChildObject(pBarImage);
		}

		m_pEndImage->setLocalPosition(
			m_oBarImageList.back()->getLocalPosition() +
			D3DXVECTOR2(m_oBarImageList.back()->getScale().x, 0.0f)
		);

		if (m_nMaxValue < m_nCurrentValue)
		{
			m_nCurrentValue = m_nMaxValue;
		}

		float stScaleY = 0.0f;

		switch (m_eProgressBarType)
		{
		case CUIProgressBar::EProgressBarType::HPBar:
			stScaleY = 6.0f;
			break;
		case CUIProgressBar::EProgressBarType::StaminaBar:
			stScaleY = 4.0f;
			break;
		}

		m_pProgressBarImage = Create<CUIImage>(m_oImageFilepath[3]);
		m_pProgressBarImage->setScale(D3DXVECTOR2(
			m_fBarLength *
			((float)m_nCurrentValue / m_nMaxValue), stScaleY
		));
		m_pProgressBarImage->setLocalPosition(
			m_oBarImageList[0]->getLocalPosition() + D3DXVECTOR2(0.0f, 3.0f));

		this->addChildObject(m_pProgressBarImage);

		this->setScale(m_stScale);
	}
}

void CUIProgressBar::setScale(const D3DXVECTOR2 & a_rstScale)
{
	m_bIsEmitEnable = true;
	m_fBarLength = 0.0f;

	m_stScale = a_rstScale;

	m_pStartImage->setScale(D3DXVECTOR2(
		m_pStartImage->getImageSize().x * m_stScale.x,
		m_pStartImage->getImageSize().y * m_stScale.y));

	auto pBarImage = m_oBarImageList[0];
	pBarImage->setScale(D3DXVECTOR2(
		pBarImage->getImageSize().x * m_stScale.x,
		pBarImage->getImageSize().y * m_stScale.y));
	pBarImage->setLocalPosition(
		m_pStartImage->getLocalPosition() +
		D3DXVECTOR2(m_pStartImage->getScale().x, 0.0f)
	);

	m_fBarLength += pBarImage->getScale().x;

	for (int i = 1; i < m_nMaxValue / PROGRESSBARMIN; ++i)
	{
		auto pBarImage = m_oBarImageList[i];
		pBarImage->setScale(D3DXVECTOR2(
			pBarImage->getImageSize().x * m_stScale.x,
			pBarImage->getImageSize().y * m_stScale.y));
		pBarImage->setLocalPosition(
			m_oBarImageList[i - 1]->getLocalPosition() +
			D3DXVECTOR2(m_oBarImageList[i - 1]->getScale().x, 0.0f)
		);

		m_fBarLength += pBarImage->getScale().x;
	}

	m_pEndImage->setScale(D3DXVECTOR2(
		m_pEndImage->getImageSize().x * m_stScale.x,
		m_pEndImage->getImageSize().y * m_stScale.y));
	m_pEndImage->setLocalPosition(
		m_oBarImageList.back()->getLocalPosition() +
		D3DXVECTOR2(m_oBarImageList.back()->getScale().x, 0.0f)
	);

	float stScaleY = 0.0f;

	switch (m_eProgressBarType)
	{
	case CUIProgressBar::EProgressBarType::HPBar:
		stScaleY = 6.0f;
		break;
	case CUIProgressBar::EProgressBarType::StaminaBar:
		stScaleY = 4.0f;
		break;
	}

	m_pProgressBarImage->setLocalPosition(
		m_oBarImageList[0]->getLocalPosition() + D3DXVECTOR2(0.0f, 3.0f * m_stScale.y));

	m_pProgressBarImage->setScale(D3DXVECTOR2(
		m_fBarLength * ((float)m_nCurrentValue / m_nMaxValue),
		stScaleY * m_stScale.y
	));
}
