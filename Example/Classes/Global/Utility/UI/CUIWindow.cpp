#include "CUIWindow.h"
#include "../../Function/GlobalFunction.h"
#include "../Manager/CDeviceManager.h"
#include "../Manager/CResourceManager.h"
#include "../Render/CShader.h"
#include "../Manager/CWindowManager.h"
#include "CUIProgressBar.h"
#include "CUIItemShutCut.h"
#include "CUIImage.h"


CUIWindow::CUIWindow(void)
	: m_oClockImage("Resources/Textures/UI/cock00_d_clock.png")
	, m_oTimeImage("Resources/Textures/UI/cock00_d_time.png")
	, m_oSharpImage("Resources/Textures/UI/cock00_d_sharp.png")
	, m_oIDImage("Resources/Textures/UI/cock00_d_ID.png")
	, m_oHPImage("Resources/Textures/UI/cock00_d_HP.png")
	, m_oStaminaImage("Resources/Textures/UI/cock00_d_itemshotcut.png")
	, m_oItemShotCutImage("Resources/Textures/UI/cock00_d_itemshotcut.png")
	, m_oPoisonImage("Resources/Textures/UI/icon_poison.png")
{
	D3DXVECTOR2 stWindowSize = D3DXVECTOR2(
		GET_WINDOW_SIZE().cx, GET_WINDOW_SIZE().cy);

	CUIImage* pClock = Create<CUIImage>(m_oClockImage);
	pClock->setLocalPosition(D3DXVECTOR2(40.0f, stWindowSize.y - 200.0f));
	pClock->setScale(pClock->getImageSize() * 2.0f);
	pClock->setTag(TAG_CLOCK);

	this->addChildObject(pClock);

	CUIImage* pSharp = Create<CUIImage>(m_oSharpImage);
	pSharp->setLocalPosition(D3DXVECTOR2(180.0f, stWindowSize.y - 200.0f));
	pSharp->setScale(pSharp->getImageSize() * 2.0f);
	pSharp->setTag(TAG_SHARP);

	this->addChildObject(pSharp);

	CUIImage* pID = Create<CUIImage>(m_oIDImage);
	pID->setLocalPosition(D3DXVECTOR2(40.0f, stWindowSize.y - 280.0f));
	pID->setScale(pID->getImageSize());
	pID->setTag(TAG_ID);

	this->addChildObject(pID);

	CUIImage* pTime = Create<CUIImage>(m_oTimeImage);
	pTime->setScale(pTime->getImageSize() * 2.0f);
	pTime->setLocalPosition((pClock->getScale() / 2.0f) -
		(pTime->getScale() / 2.0f));
	pTime->setTag(TAG_TIME);

	pClock->addChildObject(pTime);

	auto pHPBar = Create<CUIProgressBar>(CUIProgressBar::EProgressBarType::HPBar, 100);
	pHPBar->setLocalPosition(D3DXVECTOR2(160.0f, stWindowSize.y - 130.0f));
	pHPBar->setScale(D3DXVECTOR2(2.0f, 2.0f));
	pHPBar->setTag(TAG_HP);

	this->addChildObject(pHPBar);

	m_oProgressBarList.push_back(pHPBar);

	auto pStamina = Create<CUIProgressBar>(CUIProgressBar::EProgressBarType::StaminaBar, 100);
	pStamina->setLocalPosition(D3DXVECTOR2(184.0f, stWindowSize.y - 154.0f));
	pStamina->setScale(D3DXVECTOR2(2.0f, 2.0f));
	pStamina->setTag(TAG_STAMINA);

	this->addChildObject(pStamina);

	m_oProgressBarList.push_back(pStamina);

	auto pPoison = Create<CUIImage>("Resources/Textures/UI/icon_poison.png");
	pPoison->setLocalPosition(D3DXVECTOR2(350.0f, stWindowSize.y - 200.0f));
	pPoison->setScale(pPoison->getImageSize() * 1.5f);
	pPoison->setIsHide(false);
	pPoison->setTag(TAG_POISON);

	this->addChildObject(pPoison);

	m_pItemShutCut = Create<CUIItemShutCut>();
	m_pItemShutCut->setLocalPosition(D3DXVECTOR2(stWindowSize.x - 250.0f, 80.0f));
	m_pItemShutCut->setTag(TAG_ITEMSHOTCUT);

	this->addChildObject(m_pItemShutCut);
}

CUIWindow::~CUIWindow(void)
{
}

void CUIWindow::render(void)
{
	CUIObject::render();
}

void CUIWindow::openItemWindow(void)
{
	CUIImage* pOpenUI = Create<CUIImage>("Resources/Textures/UI/a_hou_back1.png");
	pOpenUI->setLocalPosition(D3DXVECTOR2(GET_WINDOW_SIZE().cx / 3.0f * 2.0f, 0.0f));
	pOpenUI->setScale(D3DXVECTOR2(GET_WINDOW_SIZE().cx / 3.0f, GET_WINDOW_SIZE().cy));
	pOpenUI->setTag(TAG_OPENWINDOW);

	this->addChildObject(pOpenUI);

	CUIImage* pItemset1 = Create<CUIImage>("Resources/Textures/UI/itemset_1.png");
	pItemset1->setLocalPosition(D3DXVECTOR2(35.0f, 30.0f));
	pItemset1->setScale(pItemset1->getImageSize() * 0.85f);
	pItemset1->setTag(1);

	pOpenUI->addChildObject(pItemset1);

	CUIImage* pItemset2 = Create<CUIImage>("Resources/Textures/UI/itemset_2.png");
	pItemset2->setLocalPosition(pItemset1->getLocalPosition());
	pItemset2->setScale(pItemset2->getImageSize() * 0.85f);
	pItemset2->setIsHide(true);
	pItemset2->setTag(2);

	pOpenUI->addChildObject(pItemset2);
}