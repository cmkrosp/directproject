#pragma once

#include "../Global/Define/KGlobalDefine.h"
#include "../Global/Utility/UI/CUIImage.h"

class CItem : public CUIImage
{
public:

	enum class EITEMTYPE
	{
		NONE = 0,
		HPPOTION,
		MEAT,
		CUREPOTION,
		FLASHBOMB,
		RARE,
	};

	struct STParameters
	{
		EITEMTYPE m_eItemType;
		int m_nOption;
	};

public:

	CC_SYNTHESIZE_READONLY(STParameters, m_stParameters, Parameters);

public:

	CItem(std::string a_oImageFile, const STParameters& a_rstParameters);
	virtual ~CItem(void);
};

