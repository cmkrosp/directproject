#include "CTown.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Global/Utility/Object/CTerrain.h"

CTown::CTown(const std::string a_oFilepath, float a_fHeightCorrectionValue)
	:
	CSkinnedMesh(CSkinnedMesh::STParameters{
	"Resources/Meshes/" + a_oFilepath + ".X",
	KEY_SKINNED_MESH_SHADER,
	KEY_TRANSFORM_BUFFER,
	KEY_SKINNED_MESH_RENDER_BUFFER,
	KEY_SKINNED_MESH_INPUT_LAYOUT,

	INPUT_SKINNED_MESH_ELEMENT_DESCS
		})
, m_fHeightCorrectionValue(a_fHeightCorrectionValue)
, m_oFilepath(a_oFilepath)
{
	m_stPosition = D3DXVECTOR3(0.0f, m_fHeightCorrectionValue, 0.0f);

	m_bIsAlphaBlendEnable = true;

	CTerrain::STParameters stParameters{
		m_oFilepath + "TerrainMesh",
		KEY_TERRAIN_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_TERRAIN_RENDER_BUFFER,
		KEY_TERRAIN_INPUT_LAYOUT
	};

	stParameters.m_nSmoothLevel = 0;
	stParameters.m_fHeightScale = 0.0105f;
	stParameters.m_stMapSize.cx = 513;
	stParameters.m_stMapSize.cy = 513;
	stParameters.m_stTileSize.x = 1.0f;
	stParameters.m_stTileSize.y = 1.0f;

	stParameters.m_oHeightMapFilepath = "Resources/Meshes/" + m_oFilepath + ".raw";
	stParameters.m_oElementDescList = INPUT_STATIC_MESH_ELEMENT_DESCS;

	m_pTerrain = Create<CTerrain>(stParameters);
}

float CTown::getHeightAtPosition(const D3DXVECTOR3 & a_rstPosition)
{
	return m_pTerrain->getHeightAtPosition(a_rstPosition);
}

void CTown::setScale(const D3DXVECTOR3 & a_rstScale)
{
	CSkinnedMesh::setScale(a_rstScale);

	m_stPosition = D3DXVECTOR3(0.0f, m_fHeightCorrectionValue * m_stScale.y, 0.0f);

	CTerrain::STParameters stParameters{
		m_oFilepath + "TerrainMesh",
		KEY_TERRAIN_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_TERRAIN_RENDER_BUFFER,
		KEY_TERRAIN_INPUT_LAYOUT
	};

	stParameters.m_nSmoothLevel = 0;
	stParameters.m_fHeightScale = 0.0105f * m_stScale.y;
	stParameters.m_stMapSize.cx = 513;
	stParameters.m_stMapSize.cy = 513;
	stParameters.m_stTileSize.x = 1.0f * m_stScale.x;
	stParameters.m_stTileSize.y = 1.0f * m_stScale.z;

	stParameters.m_oHeightMapFilepath = "Resources/Meshes/" + m_oFilepath + ".raw";
	stParameters.m_oElementDescList = INPUT_STATIC_MESH_ELEMENT_DESCS;

	m_pTerrain = Create<CTerrain>(stParameters);
}
