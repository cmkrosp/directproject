#include "Monster.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CStaticMesh.h"
#include "../Global/Utility/Object/CParticleSystem.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"
#include "../Global/Utility/Manager/FSoundManager.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Player/Player.h"

Monster::Monster()
{
}

Monster::~Monster()
{
}

void Monster::init()
{
	this->createMesh();

	//m_pParticle = Create<CParticleSystem>(CParticleSystem::STParameters{
	//"Resources/Textures/particle_fire.png",					//�̹����ļ�
	//0.5f, 1.0f, 7.0f,										//���� �ؽ�ó ȿ���ð�, ���� �ִ� ������, �� �ִ� ������
	//D3DXCOLOR(0.7f, 0.3f, 0.3f, 1.0f),						//���� �ؽ�ó ����
	//D3DXCOLOR(1.0f, 0.0f, 0.0f, 0.5f),						//�� �ؽ�ó ����
	//D3DXVECTOR3(0.0f, 0.0f, 0.1f),							//BACK, LEFT, DOWN �̵� �ִ�ġ
	//D3DXVECTOR3(0.0f, 0.0f, 0.1f),							//FRONT, RIGHT, UP �̵� �ִ�ġ
	//0.2f, 150, 50											//�̵� �ӵ�, �ִ� ��ƼŬ, ���� ��ƼŬ
	//	});

	//m_pParticle->setAlphaBlendEnable(true);

	GET_FMOD()->init();
	GET_FMOD()->addSound("M��������", "Resources/Sounds/Effect/monster/����Ҹ�ġ��.wav");
	GET_FMOD()->addSound("M������", "Resources/Sounds/Effect/monster/������4.wav");
	GET_FMOD()->addSound("M������2", "Resources/Sounds/Effect/monster/������4.wav");
	GET_FMOD()->addSound("M������3", "Resources/Sounds/Effect/monster/������4.wav");
	GET_FMOD()->addSound("M������", "Resources/Sounds/Effect/monster/������2.wav");
	GET_FMOD()->addSound("M�Ҹ�ġ��", "Resources/Sounds/Effect/monster/��ū�Ҹ�ġ��.wav");
	GET_FMOD()->addSound("M�ȱ�", "Resources/Sounds/Effect/monster/�ȱ�2.wav");
	GET_FMOD()->addSound("M�ȱ�2", "Resources/Sounds/Effect/monster/�ȱ�2.wav");
	GET_FMOD()->addSound("M�ȱ�3", "Resources/Sounds/Effect/monster/�ȱ�2.wav");
	GET_FMOD()->addSound("M�ȱ�4", "Resources/Sounds/Effect/monster/�ȱ�2.wav");
	GET_FMOD()->addSound("M�߹ٴھ���", "Resources/Sounds/Effect/monster/�߼Ҹ�.wav");
	GET_FMOD()->addSound("M���غ�", "Resources/Sounds/Effect/monster/�Ҹ�����.wav");
	GET_FMOD()->addSound("M�ҹ߻�", "Resources/Sounds/Effect/monster/�ҹ߻�Ҹ�.wav");
	GET_FMOD()->addSound("M��������", "Resources/Sounds/Effect/monster/�����ֵθ���.wav");
	GET_FMOD()->addSound("M����", "Resources/Sounds/Effect/monster/����ź.wav");
	GET_FMOD()->addSound("M����", "Resources/Sounds/Effect/monster/����2.wav");
	GET_FMOD()->addSound("M�״�", "Resources/Sounds/Effect/monster/�״¼Ҹ�.wav");
	GET_FMOD()->addSound("M��", "Resources/Sounds/Effect/monster/��.wav");
	GET_FMOD()->addSound("MŬ����", "Resources/Sounds/BGM/Ŭ����.wav", true);

	backStatic->setPosition(D3DXVECTOR3(-800.0f, 50.0f, 0.0f));
	backStatic->setScale(D3DXVECTOR3(600.0f, 300.0f, 600.0f));
	backStatic->setRenderEnable(false);
	

	frontStatic->setPosition(D3DXVECTOR3(500.0f, 50.0f, 0.0f));
	frontStatic->setScale(D3DXVECTOR3(600.0f, 300.0f, 600.0f));
	frontStatic->setRenderEnable(false);


	bodyStatic->setScale(D3DXVECTOR3(300.0f, 150.0f, 150.0f));
	bodyStatic->setRenderEnable(false);

	tailStatic->setScale(D3DXVECTOR3(300.0f, 300.0f, 300.0f));
	tailStatic->setRenderEnable(false);

	hipStatic->setScale(D3DXVECTOR3(300.0f, 300.0f, 200.0f));

	remoteStatic->setPosition(D3DXVECTOR3(1200.0f, 50.0f, 0.0f));
	remoteStatic->setScale(D3DXVECTOR3(600.0f, 300.0f, 600.0f));
	remoteStatic->setRenderEnable(false);


	tailPosition->addChildObject(tailStatic);
	bodyPosition->addChildObject(bodyStatic);
	hipPosition->addChildObject(hipStatic);

	monster->setPosition(D3DXVECTOR3(0.0f, 120.0f, -20.0f));
	monster->setScale(D3DXVECTOR3(0.02f, 0.02f, 0.02f));
	monster->rotateByUpDirection(-90.0f);
	monster->setTimeScale(1.5f);
	monster->addChildObject(backStatic);
	monster->addChildObject(frontStatic);
	monster->addChildObject(remoteStatic);
	monster->setAlphaBlendEnable(true);

	m_oAnimationNameList = monster->getAnimationNameList();

	m_eState = EState::APPEAR;

	monster->playAnimation(m_oAnimationNameList[2], true);

	monsterDirection = m_stGoalPosition - monster->getPosition();
	D3DXVec3Normalize(&monsterDirection, &monsterDirection);

	this->m_fHP = 200.0f;
	this->m_fTailHP = 100.0f;
}

void Monster::update()
{
	monster->update();
	tailPosition->update();
	bodyPosition->update();
	hipPosition->update();
	//m_pParticle->update();

	//m_pParticle->setPosition(D3DXVECTOR3(bodyPosition->getPosition().x / 2 - 5.0f, bodyPosition->getPosition().y / 2 + 2.0f, bodyPosition->getPosition().z / 2));
	//m_pParticle->setPosition(D3DXVECTOR3(monster->getBoneMatrix("HD-00")._41 - 2.0f, monster->getBoneMatrix("HD-00")._42 - 2.0f, monster->getBoneMatrix("HD-00")._43 + 0.0f));

	if (ballStatic != NULL)
		ballStatic->update();

	if(m_eState != EState::APPEAR && m_eState != EState::WING && m_eState != EState::START_IDLE)
		this->setHeightMap(monster);

	tailPosition->setWorldMatrix(monster->getBoneMatrix("TL-05"));
	bodyPosition->setWorldMatrix(monster->getBoneMatrix("HD-00"));
	hipPosition->setWorldMatrix(monster->getBoneMatrix("WST-00"));

	if (m_fHP < 100)
	{
		monster->setTimeScale(2.0f);
	//	m_pParticle->startParticleEmit(0.0f);
	}

	if (m_eState == EState::WALK)
	{
		this->walkTurnState();
	}
	else if (m_eState == EState::BITE)
	{
		this->biteState();
	}
	else if (m_eState == EState::RUN_BITE)
	{
		runBiteAttack = true;
		this->runBiteState();
	}
	else if (m_eState == EState::TAIL_ATTACK)
	{
		tailAttack = true;
		this->tailState();
	}
	else if (m_eState == EState::FIRE_BALL)
	{
		fireBallAttack = true;
		this->fireBallState();
	}
	else if (m_eState == EState::LEFT_TURN)
	{
		this->leftTurnState();
	}
	else if (m_eState == EState::RIGHT_TURN)
	{
		this->rightTurnState();
	}
	else if (m_eState == EState::DIE)
	{
		this->dieState();
	}
	else if (m_eState == EState::WING)
	{
		this->wingState();
	}
	else if (m_eState == EState::APPEAR)
	{
		this->appearState();
	}
	else if (m_eState == EState::START_IDLE)
	{
		this->startIdleState();
	}
	else if (m_eState == EState::BOUNDING_IDLE)
	{
		this->boundingIdleState();
	}
	else if (m_eState == EState::STUN)
	{
		this->stunState();
	}
	else if (m_eState == EState::MY_EYES)
	{
		this->myEyesState();
	}

	if (ballStatic != NULL)
	{
		ballStatic->moveByDirection(monster->getRightDirection() * 0.5f);
		ballStatic->rotateByRightDirection(15.0f);
		ballStatic->rotateByUpDirection(15.0f);

		if (m_eState != EState::FIRE_BALL)
		{
			SAFE_DELETE(ballStatic);
		}
	}

	if (m_eState == EState::IDLE)
	{
		this->idleState();
		m_ftheta = 0.0f;
		player = NULL;
	}

	if (m_eState == EState::LEFT_TURN || m_eState == EState::RIGHT_TURN)
	{
		if (player != NULL)
		{
			D3DXVECTOR3 v1 = player->m_pBaseMesh->getPosition() - monster->getPosition();
			D3DXVECTOR3 v2 = monster->getRightDirection();
			D3DXVec3Normalize(&v1, &v1);

			float vs = D3DXVec3Dot(&v1, &v2);

			float vt = acosf(vs);
			float vtheta = (180 * vt) / D3DX_PI;

			D3DXVECTOR3 v3;
			D3DXVec3Cross(&v3, &v1, &v2);

			if (m_ftheta == 0.0f)
			{
				m_ftheta = vtheta;
			}

			//printf("radian : %f,\ntheta : %f, y : %f\n", vt, vtheta, v3.y);
			if (v3.y < 0)
			{
				monster->rotateByUpDirection(m_ftheta * GET_DELTA_TIME());
			}
			else
			{
				monster->rotateByUpDirection(-m_ftheta * GET_DELTA_TIME());
			}
		}
	}

	if (m_eState == EState::WALK)
	{
		monster->moveByRightDirection(5.0f * GET_DELTA_TIME());
	}

	if (m_eState == EState::APPEAR)
	{
		if (abs(this->getHeight() - monster->getPosition().y) < 20.0f)
		{
			m_eState = EState::WING;
			m_bIsAnimationRun = false;
		}
		else
		{
			monster->moveByUpDirection(-5.0f * GET_DELTA_TIME());
		}
	}


}

void Monster::render()
{
	monster->render();
	if (ballStatic != NULL)
		ballStatic->render();

	//if (m_fHP < 100)
	//{
	//	m_pParticle->render();
	//}
}

void Monster::setHeight(float a_fHeight)
{
	auto prevPos = monster->getPosition();
	monster->setPosition(D3DXVECTOR3(prevPos.x, a_fHeight, prevPos.z));
}

bool Monster::getBiteAttack()
{
	return biteAttack;
}

float Monster::getHP()
{
	return m_fHP;
}

Monster::EState Monster::getState()
{
	return m_eState;
}

Monster::EState Monster::getPrevState()
{
	return m_ePrevState;
}

float Monster::getTailHP()
{
	return m_fTailHP;
}

std::vector<STBoundingBox> Monster::getTailBoundingBoxList()
{
	return tailStatic->getFinalBoundingBoxList();
}

double Monster::getBiteDblPercent()
{
	return monster->getDblPercent(m_oAnimationNameList[4]);
}

float Monster::getHeight()
{
	return m_fHeight;
}

float Monster::getDmg()
{
	return m_fDmg;
}

bool Monster::getAnimationRun()
{
	return m_bIsAnimationRun;
}

Player* Monster::getTurnTarget()
{
	return player;
}

D3DXVECTOR3 Monster::getMonsterPosition()
{
	return monster->getPosition();
}

D3DXVECTOR3 Monster::getMonsterRightDirection()
{
	return monster->getRightDirection();
}

STObjectBox & Monster::getBodyObjectBox()
{
	return bodyStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getHipObjectBox()
{
	return hipStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getFrontObjectBox()
{
	return frontStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getBackObjectBox()
{
	return backStatic->getFinalObjectBoxList()[0];
}

STObjectBox & Monster::getTailObjectBox()
{
	return tailStatic->getFinalObjectBoxList()[0];
}

STObjectBox& Monster::getRemoteObjectBox()
{
	return remoteStatic->getFinalObjectBoxList()[0];
}

CStaticMesh* Monster::getBallStaticMesh()
{
	return ballStatic;
}

void Monster::setHP(float a_fHP)
{
	m_fHP = a_fHP;
}

void Monster::setTailHP(float a_fTailHP)
{
	m_fTailHP = a_fTailHP;
}

void Monster::setHeightMap(CSkinnedMesh * a_pMesh)
{
	auto pstPos = a_pMesh->getPosition();
	a_pMesh->setPosition(D3DXVECTOR3(pstPos.x, m_fHeight + 2.5f, pstPos.z));
}

void Monster::setPlayerPosition(Player* player)
{
	this->player = player;
}

void Monster::setState(EState state)
{
	m_eState = state;
}

void Monster::setBiteAttack(bool a_bIsBiteAttack)
{
	biteAttack = a_bIsBiteAttack;
}

void Monster::setAnimationRun(bool a_bIsBool)
{
	m_bIsAnimationRun = a_bIsBool;
}

void Monster::stopAnimation()
{
	monster->stopAnimation();
}

void Monster::createMesh()
{
	monster = new CSkinnedMesh(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Monster/TestMonster.X",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,

		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	tailPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	tailStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	backStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	frontStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	bodyStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	bodyPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	hipPosition = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	hipStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});

	remoteStatic = new CStaticMesh(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,

		EStaticMeshType::BOX,
		INPUT_COLOR_ELEMENT_DESCS
		});
}

void Monster::idleState()
{
	monster->playAnimation(m_oAnimationNameList[2], false);
	m_bIsAnimationRun = false;
	m_fRunBiteSpeed = 15.0f;
}

void Monster::biteState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[4], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[4]) > 0.25f)
		{
			if (GET_FMOD()->isPlaying("M������"))
				GET_FMOD()->play("M������");
		}
		if (monster->getDblPercent(m_oAnimationNameList[4]) > 0.98f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
		}
	}
}

void Monster::runBiteState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[5], false);
		if (GET_FMOD()->isPlaying("M�ȱ�"))
			GET_FMOD()->play("M�ȱ�");
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[5]) > 0.1f && monster->getDblPercent(m_oAnimationNameList[5]) < 0.11f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�2"))
				GET_FMOD()->play("M�ȱ�2");
		}
		if (monster->getDblPercent(m_oAnimationNameList[5]) > 0.25f && monster->getDblPercent(m_oAnimationNameList[5]) < 0.26f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�3"))
				GET_FMOD()->play("M�ȱ�3");
		}
		if (monster->getDblPercent() > 0.3f)
		{
			if (m_fRunBiteSpeed > 0.0f)
				m_fRunBiteSpeed -= 0.3f;
			else
				m_fRunBiteSpeed = 0.0f;

			if (monster->getDblPercent(m_oAnimationNameList[5]) > 0.95f)
			{
				m_ePrevState = m_eState;
				m_eState = EState::IDLE;
			}
		}
	}
	monster->moveByRightDirection(m_fRunBiteSpeed * GET_DELTA_TIME());
}

void Monster::tailState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[1], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[1]) > 0.1f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�"))
				GET_FMOD()->play("M�ȱ�");
			if (GET_FMOD()->isPlaying("M��������"))
				GET_FMOD()->play("M��������");
		}
		if (monster->getDblPercent(m_oAnimationNameList[1]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
		}
	}
}

void Monster::fireBallState()
{
	if (!m_bIsAnimationRun)
	{
		GET_FMOD()->play("M���غ�");
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[6], false);
		ah = rand() % 3;
	}
	else
	{
		
		auto dbl = monster->getDblPercent(m_oAnimationNameList[6]);
		if (monster->getDblPercent(m_oAnimationNameList[6]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
		}
		if (dbl > 0.25f)
		{
			if (ballStatic == NULL)
			{
				if (ah == 0)
				{
					ballStatic = new CStaticMesh(CStaticMesh::STParameters{
						"Resources/Meshes/MonsterHunter/Monster/Face.X",
						KEY_STATIC_MESH_SHADER,
						KEY_TRANSFORM_BUFFER,
						KEY_STATIC_MESH_RENDER_BUFFER,
						KEY_STATIC_MESH_INPUT_LAYOUT,

						EStaticMeshType::XMESH,
						INPUT_STATIC_MESH_ELEMENT_DESCS
						});
					ballStatic->setPosition(D3DXVECTOR3(monster->getPosition().x, monster->getPosition().y + 3.0f, monster->getPosition().z));
				}
				else
				{
					ballStatic = new CStaticMesh(CStaticMesh::STParameters{
						"Resources/Meshes/MonsterHunter/Monster/FireBall.X",
						KEY_STATIC_MESH_SHADER,
						KEY_TRANSFORM_BUFFER,
						KEY_STATIC_MESH_RENDER_BUFFER,
						KEY_STATIC_MESH_INPUT_LAYOUT,

						EStaticMeshType::XMESH,
						INPUT_STATIC_MESH_ELEMENT_DESCS
						});
					ballStatic->setPosition(D3DXVECTOR3(monster->getPosition().x, monster->getPosition().y + 3.0f, monster->getPosition().z));
				}
			}
		}
		if (dbl > 0.25f && dbl < 0.27f && ah == 0)
		{
			if (GET_FMOD()->isPlaying("M��"))
				GET_FMOD()->play("M��");
		}
		else if (dbl > 0.15f && dbl < 0.17f && ah >= 1)
		{
			if (GET_FMOD()->isPlaying("M�ҹ߻�"))
				GET_FMOD()->play("M�ҹ߻�");
		}
	}
}

void Monster::leftTurnState()
{
	if (!m_bIsAnimationRun)
	{
		if (GET_FMOD()->isPlaying("M�ȱ�"))
			GET_FMOD()->play("M�ȱ�");
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[7], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[7]) > 0.65f && monster->getDblPercent(m_oAnimationNameList[7]) < 0.8f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�2"))
				GET_FMOD()->play("M�ȱ�2");
		}
		if (monster->getDblPercent(m_oAnimationNameList[7]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
			GET_FMOD()->stop("M�ȱ�");
			GET_FMOD()->stop("M�ȱ�2");
		}
	}
}

void Monster::rightTurnState()
{
	if (!m_bIsAnimationRun)
	{
		if (GET_FMOD()->isPlaying("M�ȱ�"))
			GET_FMOD()->play("M�ȱ�");
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[8], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[8]) > 0.35f && monster->getDblPercent(m_oAnimationNameList[8]) < 0.6f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�2"))
				GET_FMOD()->play("M�ȱ�2");
		}
		if (monster->getDblPercent(m_oAnimationNameList[8]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
			GET_FMOD()->stop("M�ȱ�");
			GET_FMOD()->stop("M�ȱ�2");
		}
	}
}

void Monster::walkTurnState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[0], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[0]) > 0.04f && monster->getDblPercent(m_oAnimationNameList[0]) < 0.06f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�"))
				GET_FMOD()->play("M�ȱ�");
		}
		else if (monster->getDblPercent(m_oAnimationNameList[0]) > 0.65f && monster->getDblPercent(m_oAnimationNameList[0]) < 0.8f)
		{
			if(GET_FMOD()->isPlaying("M�ȱ�2"))
				GET_FMOD()->play("M�ȱ�2");
		}
		if (monster->getDblPercent(m_oAnimationNameList[0]) > 0.95f)
		{
			GET_FMOD()->stop("M�ȱ�");
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
		}
	}
}

void Monster::dieState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[9], false);
		GET_FMOD()->play("M�״�");
		GET_FMOD()->stop("������");
		GET_FMOD()->play("MŬ����",0.8f);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[9]) > 0.95f)
		{
			if (monster->getDblPercent(m_oAnimationNameList[9]) > 0.99f)
			{
				monster->stopAnimation();
			}
			m_ePrevState = m_eState;
		}
	}
}

void Monster::appearState()
{
	if (!m_bIsAnimationRun)
	{
		GET_FMOD()->play("M��������");
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[10], true);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[10]) > 0.95f)
		{
			m_ePrevState = m_eState;
		}
		if (monster->getDblPercent(m_oAnimationNameList[10]) > 0.1f && monster->getDblPercent(m_oAnimationNameList[10]) < 0.1001f)
		{
				GET_FMOD()->play("M������");
		}
	}
}

void Monster::wingState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[11], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[11]) > 0.4f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�"))
				GET_FMOD()->play("M�ȱ�");
		}
		if (monster->getDblPercent(m_oAnimationNameList[11]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_bIsAnimationRun = false;
			m_eState = EState::START_IDLE;
		}
		else
		{
			monster->moveByUpDirection(-15.5f * GET_DELTA_TIME());
		}
	}
}

void Monster::startIdleState()
{
	monster->playAnimation(m_oAnimationNameList[2], false);
	if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.1f && monster->getDblPercent(m_oAnimationNameList[2]) < 0.15f)
	{
		if (GET_FMOD()->isPlaying("M�߹ٴھ���"))
			GET_FMOD()->play("M�߹ٴھ���");
	}
	else if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.6f && monster->getDblPercent(m_oAnimationNameList[2]) < 0.7f)
	{
		if (GET_FMOD()->isPlaying("M�Ҹ�ġ��"))
			GET_FMOD()->play("M�Ҹ�ġ��");
	}
	if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.95f)
	{
		m_ePrevState = m_eState;
		m_eState = EState::IDLE;
	}
}

void Monster::boundingIdleState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[2], false);
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.1f && monster->getDblPercent(m_oAnimationNameList[2]) < 0.15f)
		{
			if (GET_FMOD()->isPlaying("M�߹ٴھ���"))
				GET_FMOD()->play("M�߹ٴھ���");
		}
		else if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.6f && monster->getDblPercent(m_oAnimationNameList[2]) < 0.7f)
		{
			if (GET_FMOD()->isPlaying("M�Ҹ�ġ��"))
				GET_FMOD()->play("M�Ҹ�ġ��");
		}
		if (monster->getDblPercent(m_oAnimationNameList[2]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::IDLE;
		}
	}
}

void Monster::stunState()
{
	if (!m_bIsAnimationRun)
	{
		m_bIsAnimationRun = true;
		monster->playAnimation(m_oAnimationNameList[12], false);
		if (GET_FMOD()->isPlaying("M����"))
			GET_FMOD()->play("M����");
	}
	else
	{
		if (monster->getDblPercent(m_oAnimationNameList[12]) > 0.47f && monster->getDblPercent(m_oAnimationNameList[12]) < 0.48f)
		{
			if (GET_FMOD()->isPlaying("M�ȱ�"))
				GET_FMOD()->play("M�ȱ�");

		}
		if (monster->getDblPercent(m_oAnimationNameList[12]) > 0.52f)
		{
			monster->setTimeScale(0.2f);
		}
		if (monster->getDblPercent(m_oAnimationNameList[12]) > 0.55f)
		{
			monster->setTimeScale(1.5f);
		}
		if (monster->getDblPercent(m_oAnimationNameList[12]) > 0.95f)
		{
			m_ePrevState = m_eState;
			m_eState = EState::MY_EYES;
		}
	}
	
}

void Monster::myEyesState()
{
	m_bIsAnimationRun = true;
	monster->playAnimation(m_oAnimationNameList[13], false);
	if (monster->getDblPercent(m_oAnimationNameList[13]) > 0.5f && monster->getDblPercent(m_oAnimationNameList[13]) < 0.6f)
	{
		if (GET_FMOD()->isPlaying("M����"))
			GET_FMOD()->play("M����");
	}
	if (monster->getDblPercent(m_oAnimationNameList[13]) > 0.95f)
	{
		m_ePrevState = m_eState;
		m_eState = EState::IDLE;
	}
}
