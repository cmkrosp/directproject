#pragma once

#include "../Global/Define/KGlobalDefine.h"

class CSkinnedMesh;
class CStaticMesh;
class Player;
class CParticleSystem;

class Monster
{
public:
	enum class EState
	{
		NONE,
		APPEAR,
		WING,
		START_IDLE,
		BOUNDING_IDLE,
		IDLE,
		WALK,
		TAIL_ATTACK,
		RUN_BITE,
		BITE,
		FIRE_BALL,
		LEFT_TURN,
		RIGHT_TURN,
		DIE,
		STUN,
		MY_EYES
	};

public:
	Monster();
	virtual ~Monster();

public:
	void init();
	void update();
	void render();

	void setHeight(float a_fHeight);

public:			// getter, setter

	bool getBiteAttack();

	float getHP();

	EState getState();

	EState getPrevState();

	float getTailHP();

	float getHeight();

	float getDmg();

	bool getAnimationRun();

	Player* getTurnTarget();

	D3DXVECTOR3 getMonsterPosition();

	D3DXVECTOR3 getMonsterRightDirection();

	STObjectBox& getBodyObjectBox();

	STObjectBox& getHipObjectBox();

	STObjectBox& getFrontObjectBox();

	STObjectBox& getBackObjectBox();

	STObjectBox& getTailObjectBox();

	STObjectBox& getRemoteObjectBox();
	
	std::vector<STBoundingBox> getTailBoundingBoxList();

	CStaticMesh* getBallStaticMesh();

	double getBiteDblPercent();

	void setHP(float a_fHP);

	void setTailHP(float a_fTailHP);

	void setHeightMap(CSkinnedMesh * a_pMesh);

	void setPlayerPosition(Player* position);

	void setState(EState state);

	void setBiteAttack(bool a_bIsBiteAttack);

	void setAnimationRun(bool a_bIsBool);

	void stopAnimation();
private:

	void createMesh();

	void idleState();
	void biteState();
	void runBiteState();
	void tailState();
	void fireBallState();
	void leftTurnState();
	void rightTurnState();
	void walkTurnState();
	void dieState();
	void appearState();
	void wingState();
	void startIdleState();
	void boundingIdleState();
	void stunState();
	void myEyesState();

private:
	CSkinnedMesh* monster;
	
	bool m_bIsGoal = false;
	bool idle = true;

	bool tailAttack = false;
	bool runBiteAttack = false;
	bool biteAttack = false;
	bool fireBallAttack = false;
	bool walk = false;
	bool die = false;

	bool m_bIsAnimationRun = false;

	int attackVecIndex = 0;
	int ah = -1;

	float m_fHP;
	float m_fTailHP;
	float m_fHeight;
	float m_fDmg = 20.0f;

	float m_fRunBiteSpeed = 15.0f;

	float m_ftheta = 0.0f;

	std::vector<std::string> m_oAnimationNameList;

	D3DXVECTOR3 m_stGoalPosition;
	D3DXVECTOR3 monsterDirection;

	CStaticMesh* tailPosition;
	CStaticMesh* bodyPosition;
	CStaticMesh* hipPosition;

	CStaticMesh* tailStatic;
	CStaticMesh* backStatic;
	CStaticMesh* frontStatic;
	CStaticMesh* bodyStatic;
	CStaticMesh* hipStatic;
	CStaticMesh* ballStatic = NULL;
	CStaticMesh* remoteStatic;

	EState m_ePrevState;
	EState m_eState;

	Player* player = NULL;

	//CParticleSystem* m_pParticle = nullptr;
};