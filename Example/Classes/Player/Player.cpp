#include "Player.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CStaticMesh.h"
#include "../Global/Utility/Object/CParticleSystem.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"
#include "..//Global/Utility/Manager/FSoundManager.h"

Player::Player(void)
{
	// Do Nothing
}

Player::~Player(void)
{
	// Do Nothing
}

void Player::init(void)
{
	m_pBaseMesh = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Player/WeaponOnly.x",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,
		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pBaseMesh->setPosition(m_stPosition);
	m_pBaseMesh->setTimeScale(1.8f);
	m_pBaseMesh->setRenderEnable(true);

	m_pNormalMesh = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Player/NormalOnly.x",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,
		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pNormalMesh->setPosition(m_stPosition);
	m_pNormalMesh->setTimeScale(1.8f);
	m_pNormalMesh->setRenderEnable(false);

	m_pEquipMesh = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/MonsterHunter/Player/subChar.x",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,
		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pEquipMesh->setPosition(m_stPosition);
	m_pEquipMesh->setTimeScale(1.8f);
	m_pEquipMesh->setRenderEnable(true);

	m_ePlayerState = EPlayerState::IDLE;
	m_oAnimationList = m_pBaseMesh->getAnimationNameList();
	m_oNoAnimationList = m_pNormalMesh->getAnimationNameList();
	m_pNormalMesh->playAnimation(m_oNoAnimationList[0], true);


	// 피격 박스를 설정한다
	m_pPosBox = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	
	m_pHitBox = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	m_pPosBox->addChildObject(m_pHitBox);


	m_pHitBox->setDebugEnable(true);
	m_pHitBox->setScale(D3DXVECTOR3(50.0f, 50.0, 150.0f));
	//m_pHitBox->setPosition(D3DXVECTOR3(30.0, -30.0f, -30.0f));

	// 타격 구를 설정한다
	m_pPosSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	
	m_pAtkSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	m_pPosSphere->addChildObject(m_pAtkSphere);
	m_pAtkSphere->setScale(D3DXVECTOR3(30.0f, 30.0f, 30.0f));

	m_pAtkSphere->setDebugEnable(true);
	m_pAtkSphere->setPosition(D3DXVECTOR3(90.0f, 80.0f, 37.0f));

	// 타격 구를 설정한다
	m_pSPosSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});

	m_pSAtkSphere = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_STATIC_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_STATIC_MESH_RENDER_BUFFER,
		KEY_STATIC_MESH_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_STATIC_MESH_ELEMENT_DESCS
	});
	
	m_pSPosSphere->addChildObject(m_pSAtkSphere);
	m_pSAtkSphere->setScale(D3DXVECTOR3(45.0f, 45.0f, 45.0f));

	m_pSAtkSphere->setDebugEnable(true);
	m_pSAtkSphere->setPosition(D3DXVECTOR3(0.0f, -80.0f, 50.0f));

	m_pFlashBomb = Create<CStaticMesh>
		(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_COLOR_ELEMENT_DESCS
		});
	m_pFlashBomb->setScale(D3DXVECTOR3(0.5f, 0.5f, 0.5f));
	m_pFlashBomb->setAlphaBlendEnable(true);
	m_pFlashBomb->setColor(D3DXCOLOR(1.0f, 1.0f, 0.0f, 0.5f));
	m_pFlashBomb->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_pFlashBomb->setAlphaBlendEnable(true);
	//m_pFlashBomb->setDebugEnable(true);

	m_pShadow = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_SPHERE_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::SPHERE,
		INPUT_COLOR_ELEMENT_DESCS
		});

	m_pShadow->setScale(D3DXVECTOR3(2.5f, 0.0f, 2.5f));
	m_pShadow->setPosition(m_stPosition);
	m_pShadow->setAlphaBlendEnable(true);
	m_pShadow->setColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.4f));

	// 사운드를 한다
	GET_FMOD()->init();
	GET_FMOD()->addSound("WATK", "Resources/Sounds/Effect/Char/MATK.wav");
	GET_FMOD()->addSound("MATK", "Resources/Sounds/Effect/Char/WATK.wav");
	GET_FMOD()->addSound("SATK", "Resources/Sounds/Effect/Char/SATK.wav");
	GET_FMOD()->addSound("SHDATK1", "Resources/Sounds/Effect/Char/SHATK1.wav");
	GET_FMOD()->addSound("SHDATK2", "Resources/Sounds/Effect/Char/SHATK2.wav");
	GET_FMOD()->addSound("SHON", "Resources/Sounds/Effect/Char/SHON.wav");
	GET_FMOD()->addSound("USING", "Resources/Sounds/Effect/Item/사용음.wav");
	GET_FMOD()->addSound("WOFF", "Resources/Sounds/Effect/Char/WOFF.wav");
	GET_FMOD()->addSound("WON", "Resources/Sounds/Effect/Char/WON.wav");
	GET_FMOD()->addSound("BFOOT", "Resources/Sounds/Effect/Char/BFOOT.wav");
	GET_FMOD()->addSound("AFOOT", "Resources/Sounds/Effect/Char/AFOOT.wav");
	GET_FMOD()->addSound("WALK", "Resources/Sounds/Effect/ConWalk.wav");
	GET_FMOD()->addSound("TATK", "Resources/Sounds/Effect/Char/TATK.wav");
	GET_FMOD()->addSound("HIT1", "Resources/Sounds/Effect/Char/HIT1.wav");
	GET_FMOD()->addSound("HIT2", "Resources/Sounds/Effect/Char/HIT2.wav");
	//GET_FMOD()->addSound("투기장", "Resources/Sounds/BGM/투기장.mp3",true,true);
	GET_FMOD()->addSound("JUMP", "Resources/Sounds/Effect/Char/JUMP.wav");
	GET_FMOD()->addSound("고기굽기", "Resources/Sounds/Effect/Item/고기굽기.wav");
	GET_FMOD()->addSound("사용음", "Resources/Sounds/Effect/Item/사용음.wav");
	GET_FMOD()->addSound("테이스티", "Resources/Sounds/Effect/Item/테이스티.wav");
	GET_FMOD()->addSound("FLASH", "Resources/Sounds/Effect/Char/FLASH.wav");
	GET_FMOD()->addSound("셀렉트", "Resources/Sounds/Effect/UI/좌우소리.wav");

	//GET_FMOD()->play("투기장", DEFAULT_BGM_VOLUME);

	m_pPParticle = Create<CParticleSystem>(CParticleSystem::STParameters{
	"Resources/Textures/particle_bl.png",					//이미지파서
	0.4f, 0.3f, 9.0f,										//나온 텍스처 효과시간, 시작 최대 스케일, 끝 최대 스케일
	D3DXCOLOR(0.5f, 0.0f, 0.0f, 0.8f),						//시작 텍스처 색상
	D3DXCOLOR(0.5f, 0.0f, 0.0f, 0.25f),						//끝 텍스처 색상
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),							//FRONT, RIGHT, UP 이동 최대치
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),							//BACK, LEFT, DOWN 이동 최대치
	0.0f, 3, 3											//이동 속도, 최대 파티클, 생성 파티클
		});
	m_pPParticle->setAlphaBlendEnable(true);
	m_pPParticle->setPosition(m_pBaseMesh->getPosition());

	m_eCurrItem = CItem::EITEMTYPE::HPPOTION;

	// 이동속도와 점프속도를 설정한다
	m_fMoveSpeed = 10.0f;
	m_fJumpSpeed = 10.0f;

	m_fMaxHp = 100;
	m_fMaxSp = 100;
	m_fHp = m_fMaxHp;
	m_fSp = m_fMaxSp;

	m_fYOffset = DEFAULT_NOR_HEIGHT;

	m_bIsArmed = false;
	m_bIsStar = false;
}

void Player::update(void)
{
	if (m_bIsEquip)
	{
		m_pEquipMesh->update();
		m_pEquipMesh->setPosition(m_pNormalMesh->getPosition());
	}

	// 섬광탄
	m_pFlashBomb->update();

	// 그림자
	m_pShadow->update();

	
	// 공통조작
	this->ControlRotation();
	this->ControlPublic();
	this->particleManager();
	this->flashBombManager();

	// 체력 스테미너 조작
	this->gaugeManager();

	// 사운드 매니저
	this->soundManager();
	
	// 파티클
	m_pPParticle->update();

	m_pBaseMesh->update();
	m_pNormalMesh->update();
	// 전투 매쉬용
	if (m_bIsArmed)
	{
		m_pBaseMesh->setRenderEnable(true);
		m_pNormalMesh->setRenderEnable(false);
		m_fDbl = m_pBaseMesh->getDblPercent();
		this->CombatAnimationManager(m_pBaseMesh);
		this->ControlManager(m_pBaseMesh);
		this->ControlDBLMotion(m_pBaseMesh);
		this->AnimPosManager(m_pBaseMesh);
		this->setHeightMap(m_pBaseMesh);
		this->setHitBox(m_pBaseMesh);
		this->addShadw(m_pBaseMesh);
		this->HitManager();
	}

	else if(!m_bIsArmed && !m_bIsEquip)
	{
		// 노말 매쉬용
		m_pNormalMesh->setRenderEnable(true);
		m_pBaseMesh->setRenderEnable(false);
		m_fDbl = m_pNormalMesh->getDblPercent();

		this->NoControlManager(m_pNormalMesh);
		this->ControlDBLMotion(m_pNormalMesh);
		this->AnimPosManager(m_pNormalMesh);
		this->NormalAnimationManager(m_pNormalMesh);
		this->setHeightMap(m_pNormalMesh);
		this->setHitBox(m_pNormalMesh);
		this->addShadw(m_pNormalMesh);
		this->HitManager();
	}

	
}

void Player::render(void)
{
	if (m_bIsArmed)
	{
		m_pBaseMesh->render();
	}
	else
	{
		m_pNormalMesh->render();
	}
	
	if(m_bIsEquip)
	{
		m_pEquipMesh->render();
	}

	m_pShadow->render();
	if (m_bIsFly)
	{
	m_pFlashBomb->render();

	}
	m_pPParticle->render();
}

void Player::runLoopAnimation(CSkinnedMesh * a_pMesh, int a_nIndex1, int a_nIndex2)
{
	auto &rAnimList = a_pMesh->getAnimationNameList();
	//처음 실행시
	if (m_nCurrAnimIndex != a_nIndex1 &&
		m_nCurrAnimIndex != a_nIndex2)
	{
		m_bIsSwap = false;
		m_bIsChanged = false;
		m_nCurrAnimIndex = a_nIndex1;
	}
	//이후 루프시
	else
	{
		a_pMesh->playAnimation(rAnimList[m_nCurrAnimIndex], true);
		if (m_fDbl > 0.95f)
		{
			if (m_nCurrAnimIndex == a_nIndex1)
			{
				m_nCurrAnimIndex = a_nIndex2;
			}
			else if (m_nCurrAnimIndex == a_nIndex2)
			{
				m_nCurrAnimIndex = a_nIndex1;
			}
		}
	}

}

void Player::NormalAnimationManager(CSkinnedMesh * a_pMesh)
{
	if (!m_bIsArmed)
	{
		switch (m_ePlayerState)
		{
		case EPlayerState::IDLE: a_pMesh->playAnimation(m_oNoAnimationList[0], true); break;
		case EPlayerState::WALK: a_pMesh->playAnimation(m_oNoAnimationList[1], true); break;
		case EPlayerState::RUN: a_pMesh->playAnimation(m_oNoAnimationList[2], true); break;
		case EPlayerState::PREMEAT: a_pMesh->playAnimation(m_oNoAnimationList[42], true); break;
		case EPlayerState::DOMEAT: a_pMesh->playAnimation(m_oNoAnimationList[45], true); break;
		case EPlayerState::POSTMEAT: a_pMesh->playAnimation(m_oNoAnimationList[43], true); break;
		case EPlayerState::PREEAT: a_pMesh->playAnimation(m_oNoAnimationList[36], true); break;
		case EPlayerState::POSTEAT: a_pMesh->playAnimation(m_oNoAnimationList[37], true); break;
		case EPlayerState::FLASH: a_pMesh->playAnimation(m_oNoAnimationList[33], true); break;
		}
		
	}
}

void Player::CombatAnimationManager(CSkinnedMesh * a_pMesh)
{
	if (m_bIsArmed)
	{
		switch (m_ePlayerState)
		{
		case EPlayerState::ATTACK : this->AttackAnimationManager(a_pMesh); break;
		case EPlayerState::MOVE : a_pMesh->playAnimation(m_oAnimationList[3], true); break;
		case EPlayerState::PREBLOCK: a_pMesh->playAnimation(m_oAnimationList[4], true); break;
		case EPlayerState::BLOCK: a_pMesh->playAnimation(m_oAnimationList[5], true); break;
		case EPlayerState::POSTBLOCK: a_pMesh->playAnimation(m_oAnimationList[6], true); break;
		case EPlayerState::SHIT: a_pMesh->playAnimation(m_oAnimationList[21], false); break;
		case EPlayerState::IDLE: a_pMesh->playAnimation(m_oAnimationList[0], true); break;
		case EPlayerState::SHHIT: a_pMesh->playAnimation(m_oAnimationList[24], false); break;
		case EPlayerState::JUMP :
		{
			if (IS_KEY_DOWN(DIK_A))
			{
				a_pMesh->playAnimation(m_oAnimationList[8], false);
				m_ePlayerState = EPlayerState::JUMPING;
				m_eDirection = EPlayerDirection::LEFT;
			}
			else if (IS_KEY_DOWN(DIK_D))
			{
				a_pMesh->playAnimation(m_oAnimationList[9], false);
				m_ePlayerState = EPlayerState::JUMPING;
				m_eDirection = EPlayerDirection::RIGHT;
			}
			else
			{
				// 기본 점프
			}
		}
		break;
		}


	}
}

void Player::AttackAnimationManager(CSkinnedMesh * a_pMesh)
{
	switch (m_eAttackType)
	{
	case EAttackType::WEEK1: a_pMesh->playAnimation(m_oAnimationList[11], false); break;
	case EAttackType::WEEK2: a_pMesh->playAnimation(m_oAnimationList[12], false); break;
	case EAttackType::WEEK3: a_pMesh->playAnimation(m_oAnimationList[15], false); break;
	case EAttackType::WEEK4: a_pMesh->playAnimation(m_oAnimationList[16], false); break;
	case EAttackType::DASH1: a_pMesh->playAnimation(m_oAnimationList[14], false); break;
	case EAttackType::SHDATK1: a_pMesh->playAnimation(m_oAnimationList[17], false); break;
	case EAttackType::SHDATK2: a_pMesh->playAnimation(m_oAnimationList[18], false); break;
	
	}
}



void Player::AnimPosManager(CSkinnedMesh* a_pMesh)
{
	// 방어 모션
	if (m_ePlayerState == EPlayerState::PREBLOCK)
	{
		a_pMesh->moveByRightDirection(-3.0f * GET_DELTA_TIME(), true);
		m_fYOffset -= 2.0f * GET_DELTA_TIME();
	}
	if (m_ePlayerState == EPlayerState::BLOCK)
	{

	}
	if (m_ePlayerState == EPlayerState::POSTBLOCK)
	{
		if(m_fYOffset <= DEFAULT_HEIGHT)
		m_fYOffset += 2.0f * GET_DELTA_TIME();
		a_pMesh->moveByRightDirection(3.0f * GET_DELTA_TIME(), true);
	}
	// 구르기 모션
	// {
	if (m_ePlayerState == EPlayerState::JUMPING)
	{
		a_pMesh->setTimeScale(3.0f);
		if (m_eDirection == EPlayerDirection::LEFT)
		{
			a_pMesh->moveByForwardDirection(m_fJumpSpeed *1.5f * GET_DELTA_TIME(), true);
		}

		else if (m_eDirection == EPlayerDirection::RIGHT)
		{
			a_pMesh->moveByForwardDirection(-m_fJumpSpeed * 1.5f * GET_DELTA_TIME(), true);
		}
		if (m_fDbl < 0.5)
		{
			m_fYOffset -= 6.0f * GET_DELTA_TIME();
		}
		else
		{
			m_fYOffset += 6.0f * GET_DELTA_TIME();
		}
	}
	// }

	// 3타 회전 공격
	if (m_eAttackType == EAttackType::WEEK3)
	{
		if (m_fDbl < 0.4f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}

	}

	if (m_eAttackType == EAttackType::WEEK4)
	{
		if (m_fDbl < 0.3f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}

	}

	// 점프 공격 모션
	if (m_eAttackType == EAttackType::DASH1)
	{
		if (m_fDbl < 0.15f)
		{
			
			m_fYOffset += 2.0f * GET_DELTA_TIME();
		}
		
		else if(m_fDbl >= 0.15f &&
			m_fDbl <0.3f)
		{
			if (m_fYOffset >= DEFAULT_HEIGHT)
			{
				m_fYOffset -= 2.0f * GET_DELTA_TIME();
			}
		}
	
		if (m_fDbl < 0.4)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * 1.3f * GET_DELTA_TIME(), true);
		}
	
	}

	// 방패 공격 모션
	if (m_eAttackType == EAttackType::SHDATK1)
	{
		if (m_fDbl < 0.2f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	}
	if (m_eAttackType == EAttackType::SHDATK2)
	{
		if (m_fDbl < 0.2f)
		{
			a_pMesh->moveByRightDirection(m_fJumpSpeed * GET_DELTA_TIME(), true);
		}
	}
}

void Player::ControlManager(CSkinnedMesh * a_pMesh)
{
	
	// 움직임을 설정한다
	if (m_bIsCanMove)
	{
		// 이동 
		// {
		if (IS_KEY_DOWN(DIK_W))
		{
			if (m_eAttackType != EAttackType::DASH1 &&
				m_ePlayerState != EPlayerState::LHIT &&
				m_ePlayerState != EPlayerState::SHIT &&
				m_ePlayerState != EPlayerState::DISARM &&
				m_ePlayerState != EPlayerState::ARMING)
			{
			a_pMesh->moveByRightDirection(m_fMoveSpeed * GET_DELTA_TIME()), true;
			m_ePlayerState = EPlayerState::MOVE;

			}
		}
		//추후 작업
		//else if (IS_KEY_DOWN(DIK_S))f
		//{
		//	a_pMesh->moveByRightDirection(-//m_fMoveSpeed*GET_DELTA_TIME()),true;
		//	m_ePlayerState = EPlayerState::MOVE;
		//}

		if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
		{
			if (m_ePlayerState != EPlayerState::DISARM &&
				m_ePlayerState != EPlayerState::ARMING)
			{
				m_ePlayerState = EPlayerState::IDLE;
			}
		}
		// }

	}


	// 구르기
		// {

	if (m_fSp > 20.0f)
	{
		if (IS_KEY_PRESSED(DIK_SPACE) &&
			m_bIsCanJump)
		{
			if (IS_KEY_DOWN(DIK_A) ||
				IS_KEY_DOWN(DIK_D))
			{
				m_ePlayerState = EPlayerState::JUMP;
				m_bIsCanAtk = false;
				m_bIsCanMove = false;
				m_bIsReturn = true;
				m_fSp -= 20.0f;

			}
			
		}
	}
	

	// }

	// 공격 모션
	if (IS_MOUSE_BUTTON_PRESSED(EMouseButton::LEFT) && 
		m_bIsCanAtk)
	{
		m_bIsReturn = false;
		m_bIsCanMove = false;
		m_bIsCanJump = false;
		
		if (m_eAttackType == EAttackType::NONE &&
			m_ePlayerState == EPlayerState::MOVE)
		{
			m_eAttackType = EAttackType::DASH1;
			m_ePlayerState = EPlayerState::ATTACK;
			m_bIsReturn = true;
			return;
		}

		if (m_eAttackType == EAttackType::NONE)
		{
			m_eAttackType = EAttackType::WEEK1;
			m_ePlayerState = EPlayerState::ATTACK;
			m_bIsReturn = true;
			return;
		}

		m_ePlayerState = EPlayerState::ATTACK;
		if (m_fDbl > 0.35f)
		{
			switch (m_eAttackType)
			{
			case EAttackType::WEEK1: m_eAttackType = EAttackType::WEEK2; break;
			case EAttackType::DASH1: m_eAttackType = EAttackType::WEEK2; break;
			case EAttackType::WEEK2: m_eAttackType = EAttackType::WEEK3; break;
			case EAttackType::WEEK3: m_eAttackType = EAttackType::WEEK4; break;
			}
		}

		m_bIsReturn = true;
	}

	// 방어 모션
	if (IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT) &&
		m_ePlayerState != EPlayerState::ATTACK &&
		m_ePlayerState != EPlayerState::SHHIT)
	{
		m_bIsReturn = false;
		m_bIsCanMove = false;
		m_bIsCanJump = false;
		
		if (m_ePlayerState == EPlayerState::IDLE ||
			m_ePlayerState == EPlayerState::MOVE)
		{
			if (m_ePlayerState != EPlayerState::PREBLOCK)
			{
				GET_FMOD()->play("SHON", DEFAULT_EFFECT_VOLUME-0.3f);
				m_ePlayerState = EPlayerState::PREBLOCK;
			}
		}

		if (m_ePlayerState == EPlayerState::PREBLOCK &&
			m_fDbl > 0.5f)
		{
			m_ePlayerState = EPlayerState::BLOCK;
		}

	}

	else if (IS_MOUSE_BUTTON_RELEASED(EMouseButton::RIGHT) &&
		m_ePlayerState != EPlayerState::ATTACK)
	{
		if (m_ePlayerState == EPlayerState::BLOCK)
		{
			m_ePlayerState = EPlayerState::POSTBLOCK;
			m_bIsReturn = true;
		}

		else if (m_ePlayerState == EPlayerState::PREBLOCK ||
			m_ePlayerState == EPlayerState::POSTBLOCK)
		{
			m_bIsReturn = true;
		}
	}

	// 쉴드 공격
	if (IS_MOUSE_BUTTON_PRESSED(EMouseButton::RIGHT)
		&&m_ePlayerState == EPlayerState::ATTACK)
	{
		if (m_fDbl > 0.35f && m_eAttackType == EAttackType::WEEK2)
		{
			m_bIsReturn = false;
			m_bIsCanMove = false;
			m_bIsCanJump = false;
			m_eAttackType = EAttackType::SHDATK1;
		}
		else if (m_fDbl > 0.35f && m_eAttackType == EAttackType::SHDATK1)
		{
			m_bIsReturn = false;
			m_bIsCanMove = false;
			m_bIsCanJump = false;
			m_eAttackType = EAttackType::SHDATK2;
		}
		m_bIsReturn = true;
	}
}

void Player::NoControlManager(CSkinnedMesh* a_pMesh)
{
	
	// 움직임을 설정한다
	if (m_bIsCanMove)
	{
		// 이동 
		// {
		if (IS_KEY_DOWN(DIK_LSHIFT))
		{
			if (IS_KEY_DOWN(DIK_W) &&
				m_ePlayerState != EPlayerState::LHIT &&
				m_ePlayerState != EPlayerState::SHIT &&
				m_ePlayerState != EPlayerState::DISARM &&
				m_ePlayerState != EPlayerState::ARMING )
			{
				a_pMesh->moveByRightDirection(m_fMoveSpeed* GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::RUN;
				m_fSp -= 25.0f * GET_DELTA_TIME();
			}

			if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
			{
				if (m_ePlayerState != EPlayerState::DISARM &&
					m_ePlayerState != EPlayerState::ARMING)
				{
					m_ePlayerState = EPlayerState::IDLE;
				}
		
			}
		}

		else
		{
			if (IS_KEY_DOWN(DIK_W) &&
				m_ePlayerState != EPlayerState::LHIT &&
				m_ePlayerState != EPlayerState::SHIT &&
				m_ePlayerState != EPlayerState::DISARM &&
				m_ePlayerState != EPlayerState::ARMING)
			{
				a_pMesh->moveByRightDirection(m_fMoveSpeed/2.0f * GET_DELTA_TIME()), true;
				m_ePlayerState = EPlayerState::WALK;
			}

			if (IS_KEY_RELEASED(DIK_W) || IS_KEY_RELEASED(DIK_S))
			{
				if (m_ePlayerState != EPlayerState::DISARM &&
					m_ePlayerState != EPlayerState::ARMING)
				{
					m_ePlayerState = EPlayerState::IDLE;
				}
			}
		}
		

		if (IS_KEY_PRESSED(DIK_F))
		{
			if (m_eCurrItem == CItem::EITEMTYPE::RARE)
			{
				m_bIsCanAtk = false;
				m_bIsCanMove = false;
				m_bIsCanJump = false;
				m_ePlayerState = EPlayerState::PREMEAT;
				GET_FMOD()->play("고기굽기", FULL_EFFECT_VOLUME);
			}
			else if (m_eCurrItem == CItem::EITEMTYPE::FLASHBOMB)
			{
				m_bIsCanAtk = false;
				m_bIsCanMove = false;
				m_bIsCanJump = false;
				m_bIsReturn = true;
				m_ePlayerState = EPlayerState::FLASH;
			}
			else
			{
				m_ePlayerState = EPlayerState::PREEAT;
				m_bIsCanAtk = false;
				m_bIsCanMove = false;
				m_bIsCanJump = false;
				m_pNormalMesh->setTimeScale(2.2f);
			}
		}


	}

	if (m_ePlayerState == EPlayerState::PREEAT )
	{
		if (m_fDbl > 0.0f && m_fDbl < 0.1f)
		{
			m_fYOffset -= 1.5f * GET_DELTA_TIME();
		}
	
		if (m_fDbl > 0.9f)
		{
		m_ePlayerState = EPlayerState::POSTEAT;
		}
	}

	if (m_ePlayerState == EPlayerState::POSTEAT)
	{
		m_pNormalMesh->setTimeScale(2.6f);
		if (m_fDbl>0.07f && m_fDbl < 0.30f)
		{
			m_fYOffset -= 1.8f * GET_DELTA_TIME();
		}
		else if (m_fDbl >0.30f && m_fDbl<0.60f)
		{
			if (m_fYOffset < DEFAULT_NOR_HEIGHT)
			{
			m_fYOffset += 1.8f * GET_DELTA_TIME();
			}
		}
		if (m_fDbl > 0.9f)
		{
		m_bIsReturn = true;
		}

		if (m_eCurrItem == CItem::EITEMTYPE::CUREPOTION)
		{
			m_bIsPoison = false;
		}
	}

	if (m_ePlayerState == EPlayerState::PREMEAT)
	{
		if (m_fDbl > 0.9f)
		{
		m_ePlayerState = EPlayerState::DOMEAT;
		}
		else if (m_fDbl > 0.45 &&
			m_fDbl<0.75)
		{
			m_fYOffset -= 2.0f * GET_DELTA_TIME();
		}
	}

	if (m_ePlayerState == EPlayerState::DOMEAT &&
		IS_KEY_PRESSED(DIK_F))
	{
		m_ePlayerState = EPlayerState::POSTMEAT;
		GET_FMOD()->stop("고기굽기");
		GET_FMOD()->play("테이스티");
	}

	if (m_ePlayerState == EPlayerState::POSTMEAT)
	{
		if (m_fDbl > 0.96f)
		{
		m_bIsReturn = true;
		}
		else if (m_fDbl > 0.0f &&
			m_fDbl < 0.1f)
		{
			m_fYOffset += 3.0f * GET_DELTA_TIME();
		}
	}
	
	if (m_ePlayerState == EPlayerState::FLASH)
	{
		// 공사예정
	}
	
}

void Player::ControlRotation(void)
{
	// 캐릭터를 회전한다
	if (IS_KEY_DOWN(DIK_Q))
	{
		if (!m_bIsFly)
		{
		m_pFlashBomb->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
		}
		m_pEquipMesh->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
		m_pBaseMesh->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
		m_pNormalMesh->rotateByUpDirection(-120.0f * GET_DELTA_TIME(), true);
	}
	else if (IS_KEY_DOWN(DIK_E))
	{
		if (!m_bIsFly)
		{
			m_pFlashBomb->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
		}
		m_pEquipMesh->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
		m_pBaseMesh->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
		m_pNormalMesh->rotateByUpDirection(120.0f * GET_DELTA_TIME(), true);
	}
}

void Player::ControlDBLMotion(CSkinnedMesh* a_pMesh)
{
	// 아이들로 리턴
	if (m_bIsReturn)
	{
		if (m_bIsArmed)
		{
			if (m_fYOffset < DEFAULT_HEIGHT)
			{
				m_fYOffset += 1.0f * GET_DELTA_TIME();
			}
		}

		else
		{
			if (m_fYOffset < DEFAULT_NOR_HEIGHT)
			{
				m_fYOffset += 1.0f * GET_DELTA_TIME();
			}
		}
		
		// 빨리 끝내야하는 모션들
		if (m_ePlayerState == EPlayerState::DISARM)
		{
			if (m_fDbl > 0.5f)
			{
				m_bIsCanMove = true;
				m_bIsCanJump = true;
				this->returnIdle(a_pMesh);
				m_fYOffset = DEFAULT_NOR_HEIGHT;
				m_bIsReturn = false;
				return;
			}
		}

		if (m_ePlayerState == EPlayerState::ARMING)
		{
			if (m_fDbl > 0.8f)
			{
				m_bIsCanMove = true;
				m_bIsCanJump = true;
				this->returnIdle(a_pMesh);
				m_fYOffset = DEFAULT_NOR_HEIGHT;
				m_bIsReturn = false;
				return;
			}
		}

		if (m_fDbl > 0.95f)
		{
			this->returnIdle(a_pMesh);
			if (m_bIsArmed)
			{
				m_fYOffset = DEFAULT_HEIGHT;
			}
			else
			{
				m_fYOffset = DEFAULT_NOR_HEIGHT;
			}
			m_bIsReturn = false;

		}
	
		// 점프제어
		else if (m_fDbl > 0.5f)
		{
			m_bIsCanJump = true;
		}
	}

}
void Player::setHeightMap(CSkinnedMesh * a_pMesh)
{
	if (m_ePlayerControl != EPlayerControl::FORCE)
	{
		auto pstPos = a_pMesh->getPosition();
		m_stPosition = pstPos;
		a_pMesh->setPosition(D3DXVECTOR3(pstPos.x,
			m_fHeight + m_fYOffset, pstPos.z));
	}
	
	else
	{
		a_pMesh->setPosition(D3DXVECTOR3(m_stPosition.x, m_fHeight + m_fYOffset,m_stPosition.z));

	}
}

void Player::setHitBox(CSkinnedMesh * a_pMesh)
{
	m_pPosBox->update();
	m_pHitBox->update();
	m_pPosSphere->update();
	m_pAtkSphere->update();
	m_pSPosSphere->update();
	m_pSAtkSphere->update();

	if (m_bIsArmed)
	{
	m_pPosSphere->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_SW"));
	}
	else
	{
		m_pPosSphere->setPosition(D3DXVECTOR3(0.0f, -10.0f, 0.0f));
	}
	
	m_pSPosSphere->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_WP_00"));
	m_pPosBox->setWorldMatrix(a_pMesh->getBoneMatrix("Armature_BD_01"));

}

void Player::addShadw(CSkinnedMesh* a_pMesh)
{
	auto stPosition = a_pMesh->getPosition();
	stPosition.y = m_fHeight;
	m_pShadow->setPosition(stPosition);
}

void Player::HitManager(void)
{
	if (!m_bIsStar)
	{
		if (m_ePlayerState == EPlayerState::LHIT ||
			m_ePlayerState == EPlayerState::SHIT ||
			m_ePlayerState == EPlayerState ::SHHIT)
		{
			m_bIsCanMove = false;
			m_bIsCanJump = false;
			m_bIsCanAtk = false;
			m_bIsReturn = true;
			m_fStarTime = 0.0f;
			m_bIsStar = true;

			if (m_ePlayerState == EPlayerState::LHIT)
			{
				if (m_bIsArmed)
				{
					this->swapMesh();
				}
			}

		}
	}
	else
	{
		m_fStarTime += GET_DELTA_TIME();
		if (m_fStarTime >= DEFAULT_STAR_TIME)
		{
			m_bIsStar = false;
		}
	}

	if (m_ePlayerState == EPlayerState::SHIT)
	{
		if (m_fDbl < 0.3f)
		{
		m_pBaseMesh->moveByRightDirection(-5.0f * GET_DELTA_TIME(),true);
		}
	}

	if (m_ePlayerState == EPlayerState::SHHIT)
	{
		if (m_fDbl < 0.7f)
		{
			m_pBaseMesh->moveByRightDirection(-5.0f * GET_DELTA_TIME(),true);
		}
	}
}

void Player::ControlPublic(void)
{
	// 무기 스왑
	// {
	if (IS_KEY_PRESSED(DIK_R) &&
		m_ePlayerState != EPlayerState::MOVE &&
		m_ePlayerState != EPlayerState::RUN &&
		m_ePlayerState != EPlayerState::WALK)
	{
		m_bIsCanAtk = false;
		m_bIsCanJump = false;
		m_bIsCanMove = false;
		if (m_bIsArmed)
		{
			m_ePlayerState = EPlayerState::DISARM;
			m_bIsArmSwap = true;
		}
		else
		{
			m_ePlayerState = EPlayerState::ARMING;
			m_bIsArmSwap = true;
		}
	}
	// 애니메이션 조작
	if (m_bIsArmSwap)
	{
		if (m_ePlayerState == EPlayerState::DISARM)
		{
			m_pBaseMesh->playAnimation(m_oAnimationList[2], false);
			if (m_fDbl > 0.7f)
			{
				this->swapMesh();
				m_bIsArmSwap = false;
				m_bIsReturn = true;
			}
		}
		else if(m_ePlayerState == EPlayerState::ARMING)
		{
			this->swapMesh();
			m_pBaseMesh->playAnimation(m_oAnimationList[1], false);
			m_pBaseMesh->setTimeScale(1.0f);
			m_bIsArmSwap = false;
			m_bIsReturn = true;
		}
	}

	if (m_ePlayerState == EPlayerState::ARMING)
	{
		if (m_fDbl < 0.5)
		{
			m_pBaseMesh->moveByRightDirection(2.0f * GET_DELTA_TIME(), true);
			if (m_fYOffset > DEFAULT_NOR_HEIGHT)
			{
			m_fYOffset -= 0.6f * GET_DELTA_TIME();
			}
		}
	}

	if (m_ePlayerState == EPlayerState::DISARM)
	{
		if (m_fDbl < 0.5)
		{
			m_pBaseMesh->moveByRightDirection(2.0f * GET_DELTA_TIME(), true);
			if(m_fYOffset < DEFAULT_NOR_HEIGHT)
			{
			m_fYOffset += 0.6f * GET_DELTA_TIME();
			}
		}
	}
	// 탈진
	// {
	if (m_fSp < 10.0f)
	{
		m_bIsCanAtk = false;
		m_bIsCanJump = false;
		m_bIsCanMove = false;
		m_ePlayerState = EPlayerState::DRAINED;
	}

	if (m_ePlayerState == EPlayerState::DRAINED)
	{
		m_pNormalMesh->playAnimation(m_oNoAnimationList[3], false);

		if (m_fDbl > 0.9f)
		{
			m_ePlayerState = EPlayerState::DRAINED2;
		}
		else if (m_fDbl < 0.16f)
		{
			m_pNormalMesh->moveByRightDirection(m_fMoveSpeed / 2.0f * GET_DELTA_TIME());
		}
	}

	if (m_ePlayerState == EPlayerState::DRAINED2)
	{
		m_pNormalMesh->playAnimation(m_oNoAnimationList[4], false);
		if (m_fDbl > 0.9f)
		{
			m_bIsReturn = true;
		}
	}
	// }

	// 위치를 항상 갱신한다
	if (!m_bIsArmed)
	{
		m_pBaseMesh->setPosition(m_pNormalMesh->getPosition());
	}

	// LHIT
	if (m_ePlayerState == EPlayerState::LHIT)
	{
		m_pNormalMesh->playAnimation(m_oNoAnimationList[31], false);
		m_pNormalMesh->setTimeScale(1.5f);
		if (m_fDbl < 0.7f)
		{
			m_pNormalMesh->moveByRightDirection(-m_fJumpSpeed*1.8*GET_DELTA_TIME(), true);
		}

		if (m_fDbl > 0.9f)
		{
			m_ePlayerState = EPlayerState::GETUP;
		}
		if (m_fDbl < 0.2f &&
			m_fDbl > 0.6f)
		{
			m_fYOffset += 2.5f * GET_DELTA_TIME();
		}
		if (m_fDbl > 0.3f && m_fDbl < 0.7f)
		{
			m_fYOffset -= 5.0f * GET_DELTA_TIME();
		}
	
	}

	if (m_ePlayerState == EPlayerState::GETUP)
	{
		m_pNormalMesh->playAnimation(m_oNoAnimationList[32], false);

		if (m_fDbl > 0.8f)
		{
			if (m_fYOffset < DEFAULT_NOR_HEIGHT)
			{
			m_fYOffset += 1.0f * GET_DELTA_TIME();
			}
		}
		if (m_fDbl > 0.9f)
		{
			m_bIsReturn = true;
		}

	}
	
	if (IS_KEY_PRESSED(DIK_RIGHTARROW))
	{
		GET_FMOD()->play("셀렉트", DEFAULT_EFFECT_VOLUME);
		m_pEquipMesh->setPosition(m_pBaseMesh->getPosition());
		if (m_bIsEquip)
		{
			m_pEquipMesh->setRenderEnable(true);
			m_pNormalMesh->setRenderEnable(false);
			m_bIsEquip = false;
		}
		else
		{
			m_bIsEquip = true;
			auto equipAnim = m_pEquipMesh->getAnimationNameList();
			m_pEquipMesh->playAnimation(equipAnim[1], true);
			m_pEquipMesh->setRenderEnable(true);
			m_pNormalMesh->setRenderEnable(false);
		}
	}

}

void Player::swapMesh(void)
{
	// 매쉬를 스왑한다
	if (m_bIsArmed)
	{
		m_bIsArmed = false;
		m_pNormalMesh->setPosition(m_pBaseMesh->getPosition());
	}
	else
	{
		m_bIsArmed = true;
		m_pBaseMesh->setPosition(m_pNormalMesh->getPosition());
	}
}

void Player::gaugeManager(void)
{
	if (m_ePlayerState == EPlayerState::IDLE ||
		m_ePlayerState == EPlayerState::WALK ||
		m_ePlayerState == EPlayerState::MOVE ||
		m_ePlayerState == EPlayerState::USING ||
		m_ePlayerState == EPlayerState::ARMING ||
		m_ePlayerState == EPlayerState::DISARM ||
		m_ePlayerState == EPlayerState::DRAINED ||
		m_ePlayerState == EPlayerState::DRAINED2)
	{
		if (m_fSp < m_fMaxSp)
		{
			m_fSp += (m_fMaxSp * 0.3f) * GET_DELTA_TIME();
		}

		if (m_fSp > m_fMaxSp)
		{
			m_fSp = m_fMaxSp;
		}
	}

	if (m_bIsPoison)
	{
		if (m_fHp > 10.0f)
		{
			m_fHp -= 5.0f * GET_DELTA_TIME();
		}
	}

}

void Player::soundManager(void)
{
	GET_FMOD()->update();

	if (m_ePlayerState == EPlayerState::MOVE ||
		m_ePlayerState == EPlayerState::RUN)
	{
		if (GET_FMOD()->isPlaying("WALK"))
		{
			GET_FMOD()->play("WALK", FULL_EFFECT_VOLUME);
		}
	}
	else
	{
		GET_FMOD()->stop("WALK");
	}

	// 공사중
	if (m_ePlayerState == EPlayerState::WALK)
	{
		auto nDbl = (int)m_fDbl * 1000;
		if (GET_FMOD()->isPlaying("AFOOT") && nDbl % 200 == 0)
		{
			GET_FMOD()->play("AFOOT");
		}
	}
	
	
	if (m_ePlayerState == EPlayerState::ARMING)
	{
		if (GET_FMOD()->isPlaying("WON"))
		{
			GET_FMOD()->play("WON", DEFAULT_EFFECT_VOLUME-0.5f);
		}
	}

	if (m_ePlayerState == EPlayerState::DISARM)
	{
		if (GET_FMOD()->isPlaying("WOFF") && m_fDbl <0.1f)
		{
			GET_FMOD()->play("WOFF", DEFAULT_EFFECT_VOLUME-0.5f);
		}
	}

	if (m_ePlayerState == EPlayerState::POSTEAT)
	{
		if (GET_FMOD()->isPlaying("USING") && m_fDbl > 0.2f && m_fDbl<0.3f)
		{
			GET_FMOD()->play("USING", DEFAULT_BGM_VOLUME);
		}
	}
	if (m_ePlayerState == EPlayerState::ATTACK)
	{
		if (m_eAttackType == EAttackType::WEEK1)
		{
			if (GET_FMOD()->isPlaying("MATK") && m_fDbl < 0.1f)
			{
				GET_FMOD()->play("MATK", DEFAULT_EFFECT_VOLUME);
			}
		}

		if (m_eAttackType == EAttackType::WEEK2)
		{
			if (GET_FMOD()->isPlaying("SATK") && m_fDbl < 0.1f)
			{
				GET_FMOD()->play("SATK", DEFAULT_EFFECT_VOLUME - 0.5f);
			}
		}

		if (m_eAttackType == EAttackType::WEEK3)
		{
			if (GET_FMOD()->isPlaying("TATK") && m_fDbl < 0.01f)
			{
				GET_FMOD()->play("TATK", DEFAULT_EFFECT_VOLUME - 0.5f);
			}
		}

		if (m_eAttackType == EAttackType::WEEK4)
		{
			if (GET_FMOD()->isPlaying("SATK") && m_fDbl < 0.1f)
			{
				GET_FMOD()->play("SATK", DEFAULT_EFFECT_VOLUME - 0.5f);
			}
		}

		if (m_eAttackType == EAttackType::SHDATK1)
		{
			if (GET_FMOD()->isPlaying("SHDATK1") && m_fDbl < 0.1f)
			{
				GET_FMOD()->play("SHDATK1", DEFAULT_EFFECT_VOLUME - 0.5f);
			}
		}

		if (m_eAttackType == EAttackType::SHDATK2)
		{
			if (GET_FMOD()->isPlaying("SHDATK2") && m_fDbl < 0.1f)
			{
				GET_FMOD()->play("SHDATK2", DEFAULT_EFFECT_VOLUME - 0.5f);
			}
		}

		if (m_eAttackType == EAttackType::DASH1)
		{
			if(GET_FMOD()->isPlaying("MATK") && m_fDbl >0.35f &&m_fDbl<0.45f)
			{
				GET_FMOD()->play("MATK", DEFAULT_EFFECT_VOLUME-0.1f);
			}
		}
	}
	else
	{
		GET_FMOD()->stop("SHDATK1");
		GET_FMOD()->stop("SHDATK1");
		GET_FMOD()->stop("WATK");
		GET_FMOD()->stop("SATK");
		GET_FMOD()->stop("MATK");
	}
	
	if (m_ePlayerState == EPlayerState::JUMP && m_fDbl<0.1f)
	{
		if (GET_FMOD()->isPlaying("JUMP"))
		{
		GET_FMOD()->play("JUMP", FULL_EFFECT_VOLUME);
		}
	}

	if (m_ePlayerState == EPlayerState::SHHIT &&
		m_fDbl < 0.05f)
	{
		if (GET_FMOD()->isPlaying("HIT1"))
		{
			GET_FMOD()->play("HIT1", FULL_EFFECT_VOLUME);
		}
	}

	if (m_ePlayerState == EPlayerState::LHIT ||
		m_ePlayerState == EPlayerState::SHIT)
	{
		if (m_fDbl < 0.05f)
		{
			if (GET_FMOD()->isPlaying("HIT2"))
			{
				GET_FMOD()->play("HIT2", FULL_EFFECT_VOLUME);
			}
		}

	}

	
	
}

void Player::flashBombManager(void)
{
	if (m_ePlayerState == EPlayerState::FLASH
		&&m_fDbl > 0.3f && !m_bIsFly)
	{
		m_pFlashBomb->setPosition(D3DXVECTOR3(m_pNormalMesh->getPosition().x,
			m_pNormalMesh->getPosition().y+2.0f,
			m_pNormalMesh->getPosition().z));
		m_bIsFly = true;
	}
	if (m_bIsFly)
	{
		if (m_fFlyTime < 1.3f)
		{
			m_pFlashBomb->moveByUpDirection(4.0f * GET_DELTA_TIME());
			m_pFlashBomb->moveByRightDirection(12.0f * GET_DELTA_TIME());
			m_fFlyTime += GET_DELTA_TIME();
		}
		else
		{
			m_pFlashBomb->moveByUpDirection(4.0f * GET_DELTA_TIME());
			m_pFlashBomb->moveByRightDirection(-12.0f * GET_DELTA_TIME());
			m_fFlyTime += GET_DELTA_TIME();
		}
		
		if (m_fFlyTime >= 1.8f)
		{
			GET_FMOD()->play("FLASH", DEFAULT_EFFECT_VOLUME);
			m_bIsFly = false;
			m_fFlyTime = 0.0f;
			m_pFlashBomb->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		}
	}
}

void Player::particleManager(void)
{
	if (m_ePlayerState == EPlayerState::LHIT ||
		m_ePlayerState == EPlayerState::SHIT)
	{
		if (!m_bIsParticle)
		{
			m_pPParticle->startParticleEmit(0.4f);
			m_bIsParticle = true;
		}
	}
	else
	{
		m_bIsParticle = false;
	}

	if (m_bIsArmed)
	{
		m_pPParticle->setPosition(m_pBaseMesh->getPosition()/2);
	}
	else
	{
		m_pPParticle->setPosition(m_pNormalMesh->getPosition()/2);
	}

}


STObjectBox & Player::getAtkObjectBox(void)
{
	return m_pAtkSphere->getFinalObjectBoxList()[0];
}

STObjectBox& Player::getSAtkObjectBox(void)
{
	return m_pSAtkSphere->getFinalObjectBoxList()[0];
}

STObjectBox& Player::getHitObjectBox(void)
{
	return m_pHitBox->getFinalObjectBoxList()[0];
}


void Player::returnIdle(CSkinnedMesh * a_pMesh)
{
	m_eAttackType = EAttackType::NONE;
	m_ePlayerState = EPlayerState::IDLE;
	m_eDirection = EPlayerDirection::NONE;
	// 루프 모션으로 수정
	//a_pMesh->playAnimation(m_oAnimationList[0], true);
	m_bIsCanMove = true;
	m_bIsCanAtk = true;


	a_pMesh->setTimeScale(DEFAULT_TIME_SCALE);

}
