#pragma once
#include "../Global/Define/KGlobalDefine.h"
#include "../Item/CItem.h"
#define DEFAULT_HEIGHT 1.9f
#define DEFAULT_NOR_HEIGHT 2.3f
#define DEFAULT_TIME_SCALE 1.8f
#define DEFAULT_STAR_TIME 1.5f
#define FULL_EFFECT_VOLUME 1.0f
#define DEFAULT_EFFECT_VOLUME 0.8f
#define DEFAULT_BGM_VOLUME 0.8f
class CSkinnedMesh;
class CStaticMesh;
class CParticleSystem;

class Player 
{
public:					// Enum

	enum class EPlayerControl
	{
		FREE,
		FORCE,
		NONE
	};
	enum class EPlayerState
	{
		WALK,
		RUN,
		MOVE,
		ATTACK,
		PREBLOCK,
		BLOCK,
		POSTBLOCK,
		LHIT,
		SHIT,
		SHHIT,
		USING,
		PREMEAT,
		DOMEAT,
		POSTMEAT,
		PREEAT,
		POSTEAT,
		JUMP,
		JUMPING,
		IDLE,
		FORCE,
		ARMING,
		DISARM,
		DRAINED,
		DRAINED2,
		ROLL,
		GETUP,
		FLASH,
		BYE,
		NONE
	};

	enum class EAttackType
	{
		WEEK1,
		WEEK2,
		WEEK3,
		WEEK4,
		DASH1,
		SHDATK1,
		SHDATK2,
		NONE
	};

	enum class EPlayerDirection
	{
		FRONT,
		BACK,
		LEFT,
		RIGHT,
		NONE
	};

public:					// getter


public:					// 생성자, 소멸자
	Player(void);
	virtual ~Player(void);

public:					// public 함수
	virtual void init(void);
	virtual void update(void);
	virtual void render(void);

	CC_SYNTHESIZE(float, m_fHp, Hp);
	CC_SYNTHESIZE(float, m_fMaxHp, MaxHp);
	CC_SYNTHESIZE(float, m_fSp, Sp);
	CC_SYNTHESIZE(float, m_fMaxSp, MaxSp);
	CC_SYNTHESIZE(D3DXVECTOR3, m_stPosition, Position);
	CC_SYNTHESIZE(EPlayerState, m_ePlayerState, State);
	CC_SYNTHESIZE(EAttackType, m_eAttackType, Attack);
	CC_SYNTHESIZE(EPlayerControl, m_ePlayerControl, PlayerControl);
	CC_SYNTHESIZE(float, m_fHeight, Height);
	CC_SYNTHESIZE(float, m_fDamage, Damage);
	CC_SYNTHESIZE(float, m_fMoveSpeed, MoveSpd);
	CC_SYNTHESIZE(float, m_fJumpSpeed, JumpSpd);
	CC_SYNTHESIZE(bool,m_bIsStar, Star);
	CC_SYNTHESIZE(CItem::EITEMTYPE, m_eCurrItem, CurrentItem);
	CC_SYNTHESIZE(bool, m_bIsPoison, Poison);

	CC_SYNTHESIZE(CStaticMesh*, m_pFlashBomb, FlashBomb);

private:				// private 함수
	
	// 루프애니메이션을 작동한다
	void runLoopAnimation(CSkinnedMesh * a_pMesh,int a_nIndex1, int a_nIndex2);

	// 노말 애니메이션을 관리한다
	void NormalAnimationManager(CSkinnedMesh * a_pMesh);

	// 전투 애니메이션을 관리한다
	void CombatAnimationManager(CSkinnedMesh * a_pMesh);

	// 공격 애니메이션을 관리한다
	void AttackAnimationManager(CSkinnedMesh* a_pMesh);

	// 애니메이션 좌표움직임을 관리한다.
	void AnimPosManager(CSkinnedMesh* a_pMesh);

	// 조작을 관리한다
	void ControlManager(CSkinnedMesh * a_pMesh);

	// 노말조작을 관리한다
	void NoControlManager(CSkinnedMesh* a_pMesh);

	// 회전 제어를 한다
	void ControlRotation(void);

	// 아이들 모션으로 돌아온다
	void returnIdle(CSkinnedMesh* a_pMesh);

	// DBL 모션제어
	void ControlDBLMotion(CSkinnedMesh* a_pMesh);

	// 높이를 설정한다
	void setHeightMap(CSkinnedMesh* a_pMesh);

	// 구와 박스를 셋팅한다
	void setHitBox(CSkinnedMesh* a_pMesh);

	// 그림자를 추가한다
	void addShadw(CSkinnedMesh* a_pMesh);

	// 히트 당했을때를 설정한다
	void HitManager(void);

	// 공동 모션을 제어한다
	void ControlPublic(void);

	// 매쉬를 스왑한다.
	void swapMesh(void);

	// 체력과 스테미너를 관리한다.
	void gaugeManager(void);

	// 사운드를 관리한다
	void soundManager(void);

	// 섬광탄을 관리한다
	void flashBombManager(void);

	// 파티클을 관리한다
	void particleManager(void);
public:			// getter
	
	STObjectBox & getAtkObjectBox(void);
	STObjectBox & getSAtkObjectBox(void);
	STObjectBox & getHitObjectBox(void);
	CSkinnedMesh* m_pBaseMesh = nullptr;

private:				// private 변수

	CSkinnedMesh* m_pNormalMesh = nullptr;
	CSkinnedMesh* m_pEquipMesh = nullptr;

	CStaticMesh* m_pHitBox = nullptr;
	CStaticMesh* m_pAtkSphere = nullptr;
	CStaticMesh* m_pSAtkSphere = nullptr;
	CStaticMesh* m_pPosBox = nullptr;
	CStaticMesh* m_pPosSphere = nullptr;
	CStaticMesh* m_pSPosSphere = nullptr;
	CStaticMesh* m_pShadow = nullptr;

	int m_nAnimIndex = 0;
	int m_nCurrAnimIndex = 0;
	double m_fDbl = 0.0000f;
	float m_fYOffset = DEFAULT_HEIGHT;
	float m_fStarTime = 0.0f;

	bool m_bIsEquip = false;
	bool m_bIsSwap = false;

	bool m_bIsArmSwap = false;
	bool m_bIsChanged = false;
	bool m_bIsArmed;
	bool m_bIsReturn = false;
	bool m_bIsParticle = false;

	bool m_bIsFly = false;
	float m_fFlyTime = 0.0f;
	EPlayerDirection m_eDirection = EPlayerDirection::NONE;


	// 행동제어
	bool m_bIsCanMove = true;
	bool m_bIsCanAtk = true;
	bool m_bIsCanJump = true;

	CParticleSystem* m_pPParticle = nullptr;

	std::vector<std::string> m_oAnimationList;
	std::vector<std::string> m_oNoAnimationList;
};