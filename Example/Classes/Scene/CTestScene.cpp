#include "CTestScene.h"
#include "../Global/Function/GlobalFunction.h"
#include "../Global/Utility/UI/CUIWindow.h"
#include "../Global/Utility/UI/CUIImage.h"
#include "../Global/Utility/Manager/CInputManager.h"
#include "../Global/Utility/Manager/CTimeManager.h"
#include "../Global/Utility/Manager/CBattleManager.h"
#include "../Global/Utility/Manager/CUIManager.h"
#include "../Global/Utility/Manager/CWindowManager.h"
#include "../Global/Utility/Manager/CResourceManager.h"
#include "../Global/Utility/Object/CParticleSystem.h"
#include "../Global/Utility/Object/CSkinnedMesh.h"
#include "../Global/Utility/Object/CStaticMesh.h"
#include "../Global/Utility/Object/CCamera.h"
#include "../Global/Utility/Object/CLight.h"
#include "../Global/Utility/Manager/FSoundManager.h"
#include "../Player/Player.h"
#include "../Monster/Monster.h"
#include "../Map/CMap.h"
#include "../Map/CTown.h"


CTestScene::CTestScene(HINSTANCE a_hInstance, int a_nShowOptions, const SIZE& a_rstWindowSize)
:CDirect3DApplication(a_hInstance,a_nShowOptions,a_rstWindowSize)
{
	// Do Nothing
}

CTestScene::~CTestScene(void)
{
	// Do Nothing
}

void CTestScene::init(void)
{
	CDirect3DApplication::init();
	//GET_FMOD()->init();

	m_ePlayingScene = EPLAYINGSCENE::TAG_ENDING;

	m_pTitleBackImage = Create<CUIImage>("Resources/Textures/UI/TitleBackGround.jpg");
	m_pTitleBackImage->setScale(D3DXVECTOR2(GET_WINDOW_SIZE().cx, GET_WINDOW_SIZE().cy));
	
	m_pTitleImage = Create<CUIImage>("Resources/Textures/UI/1-Trilogo.png");
	m_pTitleImage->setScale(m_pTitleImage->getImageSize());
	m_pTitleImage->setLocalPosition(D3DXVECTOR2(
		(GET_WINDOW_SIZE().cx / 2.0f) - (m_pTitleImage->getScale().x / 2.0f),
		(GET_WINDOW_SIZE().cy / 2.0f) - (m_pTitleImage->getScale().y / 2.0f) + 100.0f));
	
	m_pTitleLogoImage = Create<CUIImage>("Resources/Textures/UI/titlelogo.png");
	m_pTitleLogoImage->setScale(m_pTitleLogoImage->getImageSize() * 0.8f);
	m_pTitleLogoImage->setLocalPosition(D3DXVECTOR2(
		(GET_WINDOW_SIZE().cx / 2.0f) - (m_pTitleLogoImage->getScale().x / 2.0f),
		(GET_WINDOW_SIZE().cy / 2.0f) - (m_pTitleLogoImage->getScale().y / 2.0f) - 300.0f));

	m_pClearLogoImage = Create<CUIImage>("Resources/Textures/UI/clear.png");
	m_pClearLogoImage->setScale(m_pClearLogoImage->getImageSize() * 1.5f);
	m_pClearLogoImage->setLocalPosition(D3DXVECTOR2(
		(GET_WINDOW_SIZE().cx / 2.0f) - (m_pClearLogoImage->getScale().x / 2.0f), GET_WINDOW_SIZE().cy));
		//(GET_WINDOW_SIZE().cy / 2.0f) - (m_pClearLogoImage->getScale().y / 2.0f)));
	
	m_pEndingBackImage = Create<CUIImage>("Resources/Textures/UI/ending.jpg");
	m_pEndingBackImage->setScale(D3DXVECTOR2(GET_WINDOW_SIZE().cx, GET_WINDOW_SIZE().cy));
	
	m_pNowLoadingBackImage = Create<CUIImage>("Resources/Textures/UI/load_back01.png");
	m_pNowLoadingBackImage->setScale(D3DXVECTOR2(GET_WINDOW_SIZE().cx, GET_WINDOW_SIZE().cy));
	
	m_pNowLoadingImage[0] = Create<CUIImage>("Resources/Textures/UI/nowloading_n.png");
	m_pNowLoadingImage[1] = Create<CUIImage>("Resources/Textures/UI/nowloading_o.png");
	m_pNowLoadingImage[2] = Create<CUIImage>("Resources/Textures/UI/nowloading_w.png");
	m_pNowLoadingImage[3] = Create<CUIImage>("Resources/Textures/UI/nowloading_l.png");
	m_pNowLoadingImage[4] = Create<CUIImage>("Resources/Textures/UI/nowloading_o.png");
	m_pNowLoadingImage[5] = Create<CUIImage>("Resources/Textures/UI/nowloading_a.png");
	m_pNowLoadingImage[6] = Create<CUIImage>("Resources/Textures/UI/nowloading_d.png");
	m_pNowLoadingImage[7] = Create<CUIImage>("Resources/Textures/UI/nowloading_i.png");
	m_pNowLoadingImage[8] = Create<CUIImage>("Resources/Textures/UI/nowloading_n.png");
	m_pNowLoadingImage[9] = Create<CUIImage>("Resources/Textures/UI/nowloading_g.png");

	for (int i = 0; i < 3; ++i)
	{
		m_pNowLoadingImage[i]->setScale(m_pNowLoadingImage[i]->getImageSize() * 2.0f);

		m_pNowLoadingImage[i]->setLocalPosition(D3DXVECTOR2(
			130.0f + (i * 120.0f) - (m_pNowLoadingImage[i]->getScale().x / 2.0f),
			150.0f - (m_pNowLoadingImage[i]->getScale().y / 2.0f)));
		
		m_bNowLoading[i] = false;
	}

	for (int i = 3; i < 10; ++i)
	{
		m_pNowLoadingImage[i]->setScale(m_pNowLoadingImage[i]->getImageSize() * 2.0f);

		m_pNowLoadingImage[i]->setLocalPosition(D3DXVECTOR2(
			200.0f + (i * 120.0f) - (m_pNowLoadingImage[i]->getScale().x / 2.0f),
			150.0f - (m_pNowLoadingImage[i]->getScale().y / 2.0f)));
		
		m_bNowLoading[i] = false;
	}
	
	// 맵 생성
	m_pMap = Create<CMap>("BattleMap/Map");
	m_pMap->setScale(D3DXVECTOR3(0.5f, 0.5f, 0.5f));

	// 맵 생성
	m_pVillageMap = Create<CTown>("VillageMap/Town", 1.0f);
	auto &rAnimationNameList = m_pVillageMap->getAnimationNameList();
	
	m_pVillageMap->playAnimation(rAnimationNameList[1], true);

	m_pBoxMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
		});
	m_pBoxMesh->setPosition(D3DXVECTOR3(-8.0f, 2.0f, -12.0f));
	m_pBoxMesh->setScale(D3DXVECTOR3(7.0f, 1.0f, 7.0f));
	m_pBoxMesh->rotateByUpDirection(50.0f);

	// UI 생성
	m_pUIRoot = Create<CUIWindow>();
	m_pUIRoot->setLocalPosition(D3DXVECTOR2(0.0f, 0.0f));

	// 캐릭터 생성
	m_pPlayer = Create<Player>();
	m_pPlayer->init();

	// NPC 생성
	m_pNPC = Create<CSkinnedMesh>(CSkinnedMesh::STParameters{
		"Resources/Meshes/NPC/NPC.X",
		KEY_SKINNED_MESH_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_SKINNED_MESH_RENDER_BUFFER,
		KEY_SKINNED_MESH_INPUT_LAYOUT,

		INPUT_SKINNED_MESH_ELEMENT_DESCS
		});

	m_pNPC->setPosition(D3DXVECTOR3(0.0f, m_pVillageMap->getHeightAtPosition(m_pNPC->getPosition()) + 3.6f, 2.0f));
	m_pNPC->setRotation(D3DXVECTOR3(0.0f, 45.0f, 0.0f));

	m_pNPCMesh = Create<CStaticMesh>(CStaticMesh::STParameters{
		KEY_BOX_MESH,
		KEY_COLOR_SHADER,
		KEY_TRANSFORM_BUFFER,
		KEY_COLOR_RENDER_BUFFER,
		KEY_COLOR_INPUT_LAYOUT,
		EStaticMeshType::BOX,
		INPUT_STATIC_MESH_ELEMENT_DESCS
		});
	m_pNPCMesh->setPosition(D3DXVECTOR3(0.0f, 1.5f, 2.0f));
	m_pNPCMesh->setScale(D3DXVECTOR3(15.0f, 1.5f, 15.0f));

	auto pAnimation = m_pNPC->getAnimationNameList();

	m_pNPC->playAnimation(pAnimation[3], true);

	// 몬스터 생성
	m_pMonster = Create<Monster>();
	m_pMonster->init();

	// 카메라 설정
	GET_CAMERA()->setTargetObject(m_pPlayer->m_pBaseMesh,
		D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(0.0f, 30.0f, -15.0f),
		EFollowType::FREE);
	

	// 배틀매니저 생성
	m_pBTManager = Create<CBattleManager>();
	m_pBTManager->init(m_pPlayer, m_pMonster);

	// UI매니저 생성
	m_pUIManager = Create<CUIManager>(m_pPlayer, m_pUIRoot);

	// 밝기 설정
	GET_LIGHT()->setPosition(D3DXVECTOR3(0.0f, 5.0f, 0.0f));
	GET_LIGHT()->rotateByRightDirection(90.0f, true);

	// 사운드 생성
	GET_FMOD()->addSound("타이틀", "Resources/Sounds/BGM/타이틀.mp3", true, true);
	GET_FMOD()->addSound("투기장", "Resources/Sounds/BGM/투기장.mp3", true, true);
	GET_FMOD()->addSound("집회소", "Resources/Sounds/BGM/집회소.wav", true, true);

	GET_FMOD()->addSound("시작소리", "Resources/Sounds/Effect/UI/시작소리.wav");
	GET_FMOD()->addSound("엔터소리", "Resources/Sounds/Effect/UI/엔터소리.wav");
	GET_FMOD()->addSound("퀘스트시작", "Resources/Sounds/Effect/UI/퀘스트시작.wav");
	GET_FMOD()->addSound("상자", "Resources/Sounds/Effect/UI/open chest.wav");

	//GET_FMOD()->play("투기장");
}

void CTestScene::update(void)
{
	CDirect3DApplication::update();
	//GET_FMOD()->update();
	if (m_bIsChange)
	{
		m_fChangeCount += 1.0f * GET_DELTA_TIME();
		if (m_fChangeCount > 3.0f)
		{
			switch (m_ePlayingScene)
			{
			case EPLAYINGSCENE::TAG_TITLE:
			{
				this->changeScene(EPLAYINGSCENE::TAG_VILLAGE);	
				m_pPlayer->setPlayerControl(Player::EPlayerControl::FORCE);
				m_pPlayer->setPosition(D3DXVECTOR3(21.5f, 10.0f, -10.0f));
			}
			break;
			case EPLAYINGSCENE::TAG_VILLAGE: 
			{
				this->changeScene(EPLAYINGSCENE::TAG_LOADING);
			}
			break;
			}
			m_bIsChange = false;
			m_fChangeCount = 0.0f;
		}
	}

	if (IS_KEY_PRESSED(DIK_F5))
	{
		switch (m_ePlayingScene)
		{
		case CTestScene::EPLAYINGSCENE::TAG_TITLE:
		{
			GET_FMOD()->play("시작소리", 7.0f);
			m_bIsChange = true;
		}
			break;
		}
	}

	if (m_ePlayingScene == EPLAYINGSCENE::TAG_VILLAGE)
	{
		if (IS_KEY_PRESSED(DIK_F8))
		{
			GET_FMOD()->play("집회소");
		}
		if (IS_KEY_PRESSED(DIK_F1))
		{
			m_bIsDebugBox = !m_bIsDebugBox;

			m_pBoxMesh->setDebugEnable(m_bIsDebugBox);
			m_pNPCMesh->setDebugEnable(m_bIsDebugBox);
		}
		if (IS_KEY_DOWN(DIK_W))
		{
			m_pPlayer->setPlayerControl(Player::EPlayerControl::FREE);
		}
		if (IS_KEY_PRESSED(DIK_RETURN))
		{
			if (IsIntersectObjectBox(m_pBoxMesh->getFinalObjectBoxList()[0],
				m_pPlayer->getHitObjectBox()))
			{
				GET_FMOD()->play("엔터소리");
				if (!m_pUIRoot->findChildObjectbyTag(CUIWindow::EMENUTAG::TAG_OPENWINDOW))
				{
					GET_FMOD()->play("상자");
					m_pUIRoot->openItemWindow();
				}
				else
				{
					m_pUIRoot->removeChildObjectbyTag(CUIWindow::EMENUTAG::TAG_OPENWINDOW);
				}
			}

		}


		if (IsIntersectObjectBox(m_pNPCMesh->getFinalObjectBoxList()[0],
			m_pPlayer->getHitObjectBox()))
		{
			if (IS_KEY_PRESSED(DIK_RETURN))
			{
				GET_FMOD()->play("엔터소리");
				GET_FMOD()->play("퀘스트시작");
				m_bIsChange = true;
			}
			if (!m_bIsAnim)
			{
				auto oNpcAnimList = m_pNPC->getAnimationNameList();
				m_pNPC->setTimeScale(1.0f);
				m_pNPC->playAnimation(oNpcAnimList[2], false);
				m_bIsAnim = true;
			}
			else
			{
				auto NpcDBL = m_pNPC->getDblPercent();
				if (NpcDBL > 0.9f)
				{
					auto oNpcAnimList = m_pNPC->getAnimationNameList();
					m_pNPC->playAnimation(oNpcAnimList[3], true);
				}
			}
		}
		else
		{
			auto oNpcAnimList = m_pNPC->getAnimationNameList();
			m_pNPC->playAnimation(oNpcAnimList[1], true);
		}
	}

	switch (m_ePlayingScene)
	{
	case CTestScene::EPLAYINGSCENE::TAG_LOADING:
	{
		static bool bIsLogo[10] = { true };
		
		for(int i = 0; i < 10; ++i)
		{
			if(m_bNowLoading[i])
			{
				if(bIsLogo[i])
				{
					m_pNowLoadingImage[i]->setLocalPosition(m_pNowLoadingImage[i]->getLocalPosition() - D3DXVECTOR2(0.0f, 10.0f * GET_DELTA_TIME()));
				
					if (m_pNowLoadingImage[i]->getLocalPosition().y <
						145.0f - (m_pNowLoadingImage[i]->getScale().y / 2.0f))
					{
						bIsLogo[i] = !bIsLogo[i];
					}
				}
				else
				{
					m_pNowLoadingImage[i]->setLocalPosition(m_pNowLoadingImage[i]->getLocalPosition() + D3DXVECTOR2(0.0f, 10.0f * GET_DELTA_TIME()));

					if (m_pNowLoadingImage[i]->getLocalPosition().y >
						155.0f - (m_pNowLoadingImage[i]->getScale().y / 2.0f))
					{
						bIsLogo[i] = !bIsLogo[i];
					}
				}
			}
			else
			{
				if(m_pNowLoadingImage[i]->getLocalPosition().y + 0.5f < m_pNowLoadingImage[i - 1]->getLocalPosition().y)
				{
					m_bNowLoading[i] = true;
				}
			}
		}

		m_pNowLoadingBackImage->update();

		for(auto image : m_pNowLoadingImage)
		{
			image->update();
		}

		m_fSceneCount += GET_DELTA_TIME();
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_TITLE:
	{
		static bool bIsLogoMove = true;

		if(bIsLogoMove)
		{
			m_pTitleLogoImage->setLocalPosition(m_pTitleLogoImage->getLocalPosition() - D3DXVECTOR2(0.0f, 10.0f * GET_DELTA_TIME()));

			if (m_pTitleLogoImage->getLocalPosition().y <
				(GET_WINDOW_SIZE().cy / 2.0f) - (m_pTitleLogoImage->getScale().y / 2.0f) - 300.0f)
			{
				bIsLogoMove = !bIsLogoMove;
			}
		}
		else
		{
			m_pTitleLogoImage->setLocalPosition(m_pTitleLogoImage->getLocalPosition() + D3DXVECTOR2(0.0f, 10.0f * GET_DELTA_TIME()));

			if (m_pTitleLogoImage->getLocalPosition().y >
				(GET_WINDOW_SIZE().cy / 2.0f) - (m_pTitleLogoImage->getScale().y / 2.0f) - 290.0f)
			{
				bIsLogoMove = !bIsLogoMove;
			}
		}

		m_pTitleBackImage->update();
		m_pTitleImage->update();
		m_pTitleLogoImage->update();
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_VILLAGE:
	{
		m_pPlayer->setHeight(m_pVillageMap->getHeightAtPosition(m_pPlayer->getPosition()));
		m_pNPC->update();
		m_pVillageMap->update();
		m_pUIRoot->update();

		m_pBoxMesh->update();
		m_pNPCMesh->update();

		m_pPlayer->update();
		m_pUIManager->update();

		if (!IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT) && !m_pBTManager->getCollision())
		{
			GET_CAMERA()->setForwardDirection(D3DXVECTOR3(m_pPlayer->m_pBaseMesh->getRightDirection().x, m_pPlayer->m_pBaseMesh->getRightDirection().y - 0.3f, m_pPlayer->m_pBaseMesh->getRightDirection().z));
		}
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_BATTLE:
	{
		m_pUIManager->update();
		m_pMap->update();
		m_pUIRoot->update();
		m_pPlayer->update();
		m_pPlayer->setHeight(m_pMap->getHeightAtPosition(m_pPlayer->getPosition()));
		m_pMonster->update();

		m_pBTManager->update();
		m_pClearLogoImage->update();

		if (!IS_MOUSE_BUTTON_DOWN(EMouseButton::RIGHT) && !m_pBTManager->getCollision())
		{
			GET_CAMERA()->setForwardDirection(D3DXVECTOR3(m_pPlayer->m_pBaseMesh->getRightDirection().x, m_pPlayer->m_pBaseMesh->getRightDirection().y - 0.3f, m_pPlayer->m_pBaseMesh->getRightDirection().z));
		}
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_ENDING:
	{
		m_pEndingBackImage->update();
	}
	break;
	}
}

void CTestScene::render(void)
{
	CDirect3DApplication::render();

	switch (m_ePlayingScene)
	{
	case CTestScene::EPLAYINGSCENE::TAG_LOADING:
	{
		m_pNowLoadingBackImage->render();

		for(auto image : m_pNowLoadingImage)
		{
			image->render();
		}

		if (m_fSceneCount > 10.0f)
		{
			this->changeScene(EPLAYINGSCENE::TAG_BATTLE);
		}
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_TITLE:
	{
		m_pTitleBackImage->render();
		m_pTitleImage->render();
		m_pTitleLogoImage->render();
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_VILLAGE:
	{
		if (m_bIsDebugBox)
		{
			m_pBoxMesh->render();
			m_pNPCMesh->render();
		}

		m_pNPC->render();
		m_pVillageMap->render();
		m_pPlayer->render();
		m_pUIRoot->render();
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_BATTLE:
	{
		m_pMap->render();

		m_pMonster->render();
		m_pPlayer->render();
		m_pBTManager->render();
		m_pUIRoot->render();
	}
	break;
	case CTestScene::EPLAYINGSCENE::TAG_ENDING:
	{
		m_pEndingBackImage->render();
		GET_FMOD()->stop("M클리어");
	}
	break;
	}

	if(m_pMonster->getState() == Monster::EState::DIE &&
		m_ePlayingScene == EPLAYINGSCENE::TAG_BATTLE)
	{
		m_pClearLogoImage->render();

		static float endTime = 0;
		endTime += GET_DELTA_TIME();
		
		if(endTime < 2.0f)
		{
			m_pClearLogoImage->setLocalPosition(D3DXVECTOR2(
				m_pClearLogoImage->getLocalPosition().x,
				m_pClearLogoImage->getLocalPosition().y
				-((GET_WINDOW_SIZE().cy / 2.0f) + (m_pClearLogoImage->getScale().y / 2.0f)) * (GET_DELTA_TIME() / 2.0f)));
		}

		if(endTime >= 30.0f)
		{
			this->changeScene(EPLAYINGSCENE::TAG_ENDING);
		}
	}
}

void CTestScene::changeScene(EPLAYINGSCENE a_ePlayingScene)
{
	m_ePlayingScene = a_ePlayingScene;

	switch (m_ePlayingScene)
	{
	case CTestScene::EPLAYINGSCENE::TAG_VILLAGE:
	{
		GET_FMOD()->stop("타이틀");

	}
		break;
	case CTestScene::EPLAYINGSCENE::TAG_LOADING:
	{
		GET_FMOD()->stop("집회소");
		m_pPlayer->setPlayerControl(Player::EPlayerControl::FORCE);
		m_pPlayer->setPosition(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		m_pPlayer->setPlayerControl(Player::EPlayerControl::FREE);
	}
		break;
	case CTestScene::EPLAYINGSCENE::TAG_BATTLE:
	{
		GET_FMOD()->play("투기장");
	}
		break;
	case CTestScene::EPLAYINGSCENE::TAG_ENDING:
		GET_FMOD()->stop("투기장");
		break;
	}

}