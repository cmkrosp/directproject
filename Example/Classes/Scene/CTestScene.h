#pragma once
#include "../Global/Define/KGlobalDefine.h"
#include "../Global/Utility/Base/CDirect3DApplication.h"

class Player;
class Monster;
class CMap;
class CTown;
class CSkinnedMesh;
class CStaticMesh;
class CUIWindow;
class CBattleManager;
class CUIManager;
class CUIImage;
class CParticleSystem;

class CTestScene : public CDirect3DApplication
{
public:

	enum class EPLAYINGSCENE
	{
		TAG_NONE = 0,
		TAG_TITLE,
		TAG_VILLAGE,
		TAG_LOADING,
		TAG_BATTLE,
		TAG_ENDING,
	};

public:				// 생성자, 소멸자

	//! 생성자
	CTestScene(HINSTANCE a_hInstance, int a_nShowOptions, const SIZE& a_rstWindowSize);

	//! 소멸자
	virtual ~CTestScene(void) override;

protected:			// protected 함수
	virtual void init(void) override;

public:				// public 함수
	virtual void update(void) override;
	virtual void render(void) override;


private:			// private 함수

	void changeScene(EPLAYINGSCENE a_ePlayingScene);

private:			// private 변수

	float m_fSceneCount = 0.0f;
	float m_fChangeCount = 0.0f;
	bool m_bIsDebugBox = false;
	bool m_bIsChange = false;
	bool m_bNowLoading[10] = { true, false, };

	EPLAYINGSCENE m_ePlayingScene = EPLAYINGSCENE::TAG_NONE;

	CUIImage* m_pTitleBackImage = nullptr;
	CUIImage* m_pTitleLogoImage = nullptr;
	CUIImage* m_pTitleImage = nullptr;

	CUIImage* m_pClearLogoImage = nullptr;

	CUIImage* m_pEndingBackImage = nullptr;

	CUIImage* m_pNowLoadingBackImage = nullptr;
	CUIImage* m_pNowLoadingImage[10] = {};

	CBattleManager* m_pBTManager = nullptr;
	CUIManager* m_pUIManager = nullptr;

	float m_fZeroVolume = 0.0f;

	Player* m_pPlayer = nullptr;
	Monster* m_pMonster = nullptr;
	CSkinnedMesh* m_pNPC = nullptr;
	CStaticMesh* m_pNPCMesh = nullptr;
	CMap *m_pMap = nullptr;
	CTown* m_pVillageMap = nullptr;
	CStaticMesh* m_pBoxMesh = nullptr;
	CParticleSystem* m_pParticle = nullptr;
	CUIWindow *m_pUIRoot = nullptr;

	bool m_bIsAnim = false;
};